Es mejor transmitir las ideas que intentar  explicarlas. 
Nuestros pensamientos afectan tanto nuestro interior como nuestro exterior, e influencian  nuestra manera de ser.
Para cultivar la empatía hay que escuchar los puntos de vista de los demás, sobretodo los que son diferentes a los tuyos.
El cuerpo no solo está constituido de piel, huesos, músculos y órganos, sino también de emociones.
Vivir la vida con plenitud es aceptar las dificultades.
El estrés puede ser provocado por una mala imaginación.
Hay que guardar una “actitud de desafío” para construir y dejar esa actitud cuando es para destruir.
La mente puede curar el cuerpo, pero el cuerpo mal regenerado afecta a la mente. Hay que cuidarlos y darles toda nuestra atención.
La noción de felicidad es diferente para cada uno de nosotros. Depende de nosotros identificar su contenido.
Tenemos que aceptar nuestros propios defectos si queremos superarlos.
Una respiración consciente oxigena y alivia nuestro cuerpo, y también nuestra alma.
Aprender a enfrentar cada día las dificultades de un modo sereno y contemplativo nos llevará a la felicidad.
La complementariedad entre dos personas pasa por aceptar las diferencias del otro.
La comunicación positiva y afectiva pasa por la palabra, la mirada y los gestos.
La felicidad esta hecha para ser compartida.
Al comparar, estamos buscando excusas para responsabilizar al otro de nuestras propias debilidades.
Es mejor tener compasión que juzgar.
Observa tus pensamientos, luego identifica tus emociones y podrás gestionar las reacciones que te provocan ambos.
Si conseguimos afrontar y observar una dificultad  con desapego, aprenderemos de ella y creceremos en humildad y paciencia.
Hay que aprender a construir nuestro presente  con, y no contra, nuestro pasado.
Para tener una comunicación fluida y agradable hay que mejorar nuestra forma de expresarnos.
Una prueba de madurez es entender que podemos estar en desacuerdo sin dejar de respetar.
Saber escuchar es tan importante como saber expresar lo que sentimos.
Lo que ha sucedido, siempre está en nosotros. Hay que aceptarlo, comprenderlo con serenidad para enriquecer nuestro presente.
El estrés puede ser provocado muchas veces por nuestros pensamientos negativos.
El objetivo se genera desde el cerebro, mientras que el deseo se construye a través del corazón.
¡Somos más que un cuerpo! El alma es la riqueza que nos llena. Además de nutrir nuestra consciencia, nutre nuestra vida.
Ayudar a los demás alimenta positivamente nuestro ego.
La relajación física es esencial, ya que es imposible dirigir nuestra mente en estado de tension.
La mente bien dirigida afecta positivamente a todo nuestro cuerpo. 
El estrés empieza a ser gestionado a través del pensamiento consciente.
La mente bien dirigida ayuda a sanar al cuerpo. El cuerpo regenerado nos permite desarrollar nuestras capacidades y valores.
Saber perdonar es saber entender a los demás.
La humildad es tanto saber dar como saber recibir.
Si todos tuviéramos las mismas opiniones seríamos robots, la diversidad es una riqueza.
Si siembras buenos pensamientos en tu mente, florecerán emociones positivas.  « Laura ».
Aquel que escucha, ni juzga ni critica.  
Favorecer la autonomía del niño es favorecer su propia imagen positiva.
Nuestro cuerpo es nuestro mejor amigo, hay que tratarlo como tal.
Nuestro cuerpo es nuestro mejor amigo, hay que tratarlo como tal.
Saborea cada momento y cada acción del día.
Vivir cada día como si fuese la primera vez, sin juzgar y sin prejuicios.
Dentro de la normalidad pasamos por todos los estados emocionales, tanto negativos como positivos.
Protege tu cuerpo y tu cuerpo te protegerá.
Para evitar malentendidos el primer paso es estar dispuesto a escuchar.
La serenidad se puede transmitir a través de la palabra, los gestos y la mirada.
Cuando te conformes con ser tu mismo y no otra persona, ganarás el respeto.
¿Qué hay en la admiración? “Reconocimiento y amor”.
Una vida exitosa es una colección de pequeños momentos placenteros.
Una vez concentrada, la mente se libera de toda su confusión.
La clave de una relación sana con los demás es no imponer tu ego ni aplastar el ego de los otros.
Nuestro cuerpo es nuestro mejor amigo, nos lleva durante toda la vida.
Podemos definir la salud cuando los movimientos interiores del cuerpo están en armonía.
La enfermedad más grave es el desprecio de nuestro cuerpo. “Montaigne”.
Hay que reírse antes de ser feliz, de miedo a morirse sin haber reído.
El amor no se divide sino se multiplica. “Camille Lombard”.
Observa, percibe, disfruta de todo lo bueno que tienen las personas que conoces y olvida todo lo que no te gusta tanto de ellas.
Saber perdonar es saber entender a los demás.
Es preferible proponer tus ideas y no imponerlas.
El mundo es de todos, la Tierra no tiene dueño.
No siempre hay que buscar el sentido de las cosas sino vivirlas.
Hay que aspirar al saber y humildad para reconocer lo que no sabes.
Cultivar el sentido del humor cada día es preventivo para la salud.
Cada día es nuevo, hay que aprovechar para desarrollar una visión positiva de nosotros.
Aprender a relativizar un problema ya es gestionar el estrés.
Decir une frase agradable a un ser querido cada día es cultivar el amor.
Compartir confianza y atención nos aportará un sentimiento de bienestar.
Es más interesante concentrarse en la solución y no tanto en el problema.
Si todos tuviéramos las mismas opiniones seríamos robots, la diversidad es una riqueza.
Dentro de una dificultad, siempre hay una oportunidad. “Einstein”
Perdonar es saber escuchar para comprender mejor.
Prefiero perder mi orgullo por la persona que amo, que perder a la persona que amo por mi orgullo. 
Si un hombre tiene hambre no le des un pez, enséñale a pescar."Confucius”
Cada pensamiento agradable dirigido hacia una parte de la conciencia repercute positivamente sobre todo el ser.
La simplicidad nos lleva hacia nuestra sabiduría interior.
El camino de la felicidad está en la armonía con uno mismo.
Cuanto más estemos en contacto con nuestras capacidades y valores, más brindaremos un sentido a nuestra vida. 
El dinero es únicamente una herramienta para vivir mejor. El amor y la amistad, son una necesidad vital que da un sentido a nuestra vida. 
El tiempo es una representación de la mente, solo el presente existe.
Vivir el presente con serenidad te ayudará a construir tu futuro positivamente. « Luc Lombard »
La generosidad es el opuesto a la esterilidad y a la avaricia.
Cada día no es ni mejor ni peor; es únicamente diferente, hay que pararse para descubrirlo sin prejuicio. 
Para vivir el momento presente, hay que dejar los pensamientos y saborear el aquí y ahora.
Cuanto más conozcas tu cuerpo, mejor lo podrás tratar.
¿Qué es el optimismo? Es tener fe en el futuro.
¿Cómo enfrentar un desafío? Dando lo mejor de uno mismo.
Soy y estoy  en mi cuerpo, en mi presente, positivamente.
Para gestionar el estrés, hay que saber dirigir los pensamientos.
Detrás de nuestra mirada se encuentran nuestras ideas.  
Para desarrollar tus ideas hay que creer en ti mismo, cada día.
Para no tener dependencia emocional hay que saber definir sus necesidades.
La curiosidad bien dirigida nos puede llevar a la creatividad.
El poder del optimismo nos da vida, ilusión y activa nuestra imaginación.
Una emoción se percibe, no se piensa.
Cuando somos tolerantes, vivimos las situaciones con flexibilidad y paciencia. 
El no gestionar las emociones negativas o las situaciones difíciles puede tener consecuencias para el cuerpo y la mente. 
Los resultados o las soluciones no dependen del contexto ni del grado de dificultad, sino de nuestras capacidades. 
La vida es como una estrella fugaz, brillante y llena de energía. 
Cuanto más trabajas tus ideas más estimulas tu intuición.