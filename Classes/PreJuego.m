//
//  PreJuego.m
//  iDStress
//
//  Created by Pold on 24/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PreJuego.h"
#import "InfoJuego.h"
#import "Flurry.h"
@implementation PreJuego
@synthesize botonJuego, guia;
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0)
        return;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:buttonIndex forKey:@"gameLevel"];
     
    [[NSNotificationCenter defaultCenter] postNotificationName:@"empezarJuego2" object:@""];
    [Flurry logEvent:@"Juego" withParameters:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Juego 2", [NSString stringWithFormat:@"%i", buttonIndex ], nil] forKeys:[NSArray arrayWithObjects:@"Juego", @"Velocidad", nil]]];
}

- (IBAction) empezarprueba:(id)sender{
	if (((UIButton*)sender).tag == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"empezarJuego" object:@""];
        [Flurry logEvent:@"Juego" withParameters:[NSDictionary dictionaryWithObject:@"Juego 1" forKey:@"Juego"]];
    }
    else {
        [[[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Choose speed",nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"Slow",nil), NSLocalizedString(@"Medium",nil), NSLocalizedString(@"Fast",nil), nil] autorelease] show];
    }
    
}

-(IBAction) info: (id)sender {
	InfoJuego *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[InfoJuego alloc] initWithNibName:@"InfoJuego-iPad" bundle:nil];
    } else {
        info = [[InfoJuego alloc] initWithNibName:@"InfoJuego" bundle:nil];
    }
    [self.navigationController pushViewController:info animated:YES];
	[info release];
}


- (void)viewWillAppear:(BOOL)animated {
    [Flurry logEvent:@"Tab bar" withParameters:[NSDictionary dictionaryWithObject:@"Juego" forKey:@"Tab"]];
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
	
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

- (void)viewDidLoad {
    [super viewDidLoad];
    SET_NAVIGATION_BAR;
	self.title = NSLocalizedString(@"Game",nil);
	
    SET_INFO_BUTTON;
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[botonJuego release];
	[guia release];
    [super dealloc];
}


@end
