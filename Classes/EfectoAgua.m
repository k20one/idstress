//
//  EfectoAgua.m
//  iDStress
//
//  Created by Sophy on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EfectoAgua.h"
#import "SimpleAudioEngine.h"
#import "SessionM.h"

// HelloWorld implementation
@implementation EfectoAgua


+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	EfectoAgua *layer = [EfectoAgua node];
	layer.isTouchEnabled=YES;
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// se inicializan las instancias
-(id) init
{	
	if( (self=[super init] )) 	
	{
		
		
		CCMenuItem *backMenuItem;
		CCDirector *director = [CCDirector sharedDirector];
		if([director enableRetinaDisplay:YES])
		{	

			retina = TRUE;
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png" target:self selector:@selector(backButtonTapped:)];
			rippleImage = [ pgeRippleSprite ripplespriteWithFile:@"fondo_piedras.png" ];
			
			rippleImage.scaleX = 2;
			rippleImage.scaleY = 2;
            
            if (([[UIScreen mainScreen ] bounds].size.height == 568.0f)) {
                rippleImage.scaleY = 2.36;
                rippleImage.scaleX = 2.36;
            }
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			{
                rippleImage.scaleX = 4.80;
                rippleImage.scaleY = 4.88;
			}
		}
		else
		{
			retina  = FALSE;
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png" target:self selector:@selector(backButtonTapped:)];
			rippleImage = [ pgeRippleSprite ripplespriteWithFile:@"fondo_piedras.png" ];
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
			{
				rippleImage.scaleX = 1.22;
				rippleImage.scaleY = 1.09;
			}
		}
		rippleImage.anchorPoint = CGPointZero;
		[self addChild:rippleImage];
		rippleImage.position = ccp(0,0);
		
		backMenuItem.position = ccp(30, 30);
		backMenu = [CCMenu menuWithItems:backMenuItem, nil];
		backMenu.position = CGPointZero;
		[self addChild:backMenu];
		
		// enable touch
        [ [ CCTouchDispatcher sharedDispatcher ] addTargetedDelegate:self priority:0 swallowsTouches:YES ];	
        
        // schedule update
        [ self schedule:@selector( update: ) ];    
	}
	return self;
}

float runtime = 0;

-( void )update:( ccTime )dt 
{
    runtime += dt;    
    [ rippleImage update:dt ];
}

-( BOOL )ccTouchBegan:( UITouch* )touch withEvent:( UIEvent* )event 
{
    runtime = 0.2f;
	NSSet *allTouches = [event allTouches];  // <--this gets all the current touches
	for (UITouch *touch in allTouches) //iterate through all the touches
	{ 
		[ self ccTouchMoved:touch withEvent:event ];		
	}
	
    return( YES );
}

-( void )ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event 
{
    CGPoint pos;    
    if ( runtime >= 0.2f ) 
	{
        runtime -= 0.2f;
		
		NSSet *allTouches = [event allTouches]; 
		for (UITouch *touch in allTouches)
		{
			// get touch position and convert to screen coordinates
			pos = [ touch locationInView: [ touch view ] ];
			pos = [ [ CCDirector sharedDirector ] convertToGL:pos ];
			
			if (!retina)
			{
				pos.x = pos.x / rippleImage.scaleX;
				pos.y = pos.y / rippleImage.scaleY;
			}
            else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                pos.x = pos.x / (rippleImage.scaleX /2);
                pos.y = pos.y / (rippleImage.scaleY /2);
            }
			
			[rippleImage addRipple:pos type:RIPPLE_TYPE_WATER strength:1.6f];  
		}
	}
}

-(void) countDown:(ccTime)delta
{
	if (time != 0)
	{
		time -= 1;
		//[temporizador setString:[NSString stringWithFormat:@"%i", time]];
	}
	else 
	{
		[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	}
}

- (void)backButtonTapped:(id)sender 
{
	[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
	backMenu.visible = YES;
    SMAction(@"Water");
}
// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}
@end
