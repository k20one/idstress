//
//  Test.m
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Test.h"
#import "Info.h"
#import "Flurry.h"

@implementation Test

@synthesize boton1, boton2, boton3, boton4, preguntaTexto, progresoTexto, progresoBarra;
int contador, puntos;
int total = 20;
BOOL insomnio;

-(IBAction) info: (id)sender {
	Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
    [info viewWillAppear:YES];
    info.texto1 =  [[[NSMutableString alloc] initWithString: NSLocalizedString(@"This test will give you an approximate estimate of your stress level.",nil)] autorelease];
	info.texto2 = [[[NSMutableString alloc] initWithString: NSLocalizedString(@"\nThe goal of this test is to evaluate your stress and then suggest exercises appropriate to your level.It is not a certificated test and has 3 possible results: \nAlertness phase \nResistance phase\nExhaustion phase",nil)] autorelease];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    SET_NAVIGATION_BAR;
	SET_INFO_BUTTON;
	
	self.title = NSLocalizedString(@"Test",nil);
	
	//Preparar el test
	puntos = 0;
	contador = 0;
	progresoTexto.text = [NSString stringWithFormat:@"1/%i",total];
	progresoBarra.progress = 0;
	[self siguiente];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	NSLog(@"appear");
    if (self.tabBarController.selectedIndex!=0) {
        [Flurry logEvent:@"Tab bar" withParameters:[NSDictionary dictionaryWithObject:@"Test" forKey:@"Tab"]];
    }
}
/*

- (IBAction) siTouched: (id) sender{
	if (contador==1) {
		puntos +=0;
		insomnio = NO;
	}
	else if (contador==3 ||contador==8|| contador==10 || contador==11) puntos += 3;
	else if (contador==4|| contador==12) puntos += 4;
	else puntos += 2;
	[self siguiente];
}

- (IBAction) noTouched: (id) sender{
	if (contador==1) {
		puntos +=3;
		insomnio = YES;
	}
	else puntos += 0;
	[self siguiente];
}
 */

- (IBAction) respuestaBoton: (id)sender{
	puntos += ((UIButton *)sender).tag;
	[self siguiente];
}
- (void)siguiente{
	contador ++;
	if (contador<=total) {
		NSMutableString *text = [[[NSMutableString alloc] initWithString:@""] autorelease];
		[text appendString:[NSString stringWithFormat:@"%i", contador]];
		[text appendString:[NSString stringWithFormat:@"/%i", total]];
		progresoTexto.text = text;
		[progresoBarra setProgress:(float)contador/total];
		switch (contador) {
			default:
				break;
			case 1:
				preguntaTexto.text = NSLocalizedString(@"Do you feel negative emotions such as anxiety, restlessness, and irritability, or have negative thoughts?",nil);
                [boton4 setTitle:NSLocalizedString(@"A lot",nil) forState:UIControlStateNormal];
				[boton3 setTitle:NSLocalizedString(@"Often",nil) forState:UIControlStateNormal];
				[boton2 setTitle:NSLocalizedString(@"Sometimes",nil) forState:UIControlStateNormal];
				[boton1 setTitle:NSLocalizedString(@"Never",nil) forState:UIControlStateNormal];
				break;
			case 2:
				preguntaTexto.text = NSLocalizedString(@"You feel tense, restless, unable to relax or feel very tired?",nil);

				break;
			case 3:
				preguntaTexto.text = NSLocalizedString(@"Lately do you feel anxious or afraid?",nil);
				break;
			case 4:
				preguntaTexto.text = NSLocalizedString(@"Lately, have you had difficulties to sleep, do you  have nightmares or feel tired when you wake up?",nil);

				break;
			case 5:
				preguntaTexto.text = NSLocalizedString(@"Do you have difficulty to concentrate or memorize?",nil);
				break;
			case 6:
                
				preguntaTexto.text = NSLocalizedString(@"Do you feel depressed, sense a loss of interest or desires, or have trouble making simple decisions?",nil);

				break;
			case 7:
				preguntaTexto.text = NSLocalizedString(@"Do you often have pain or tensions with tendency to clench the teeth or jaw?",nil);

				break;
			case 8:
				preguntaTexto.text = NSLocalizedString(@"Do you experience hot flashes, chills, or hear ringing in your ears?",nil);
				break;
			case 9:
				preguntaTexto.text = NSLocalizedString(@"Lately you've had any cardiovascular symptoms, palpitations, chest pains or dizziness?",nil);
				break;
			case 10:
				preguntaTexto.text = NSLocalizedString(@"Do you have any digestive problems, bloating, constipation, diarrhea or nausea?",nil);
				break;
			case 11:
				preguntaTexto.text = NSLocalizedString(@"Lately do you have any difficulties in your emocional (personal) life or in your relationship?",nil);
				break;
			case 12:
				preguntaTexto.text = NSLocalizedString(@"Are you currently experiencing sexual dysfunction or a lack of sex drive?",nil);
				break;
			case 13:
				preguntaTexto.text = NSLocalizedString(@"Do you have career problems?  ",nil);
				break;
			case 14:
				preguntaTexto.text = NSLocalizedString(@"Do you have financial difficulties?",nil);

				break;
			case 15:
                preguntaTexto.text = NSLocalizedString(@"Do you often  have headaches or migraines?",nil);

				break;
			case 16:
				preguntaTexto.text = NSLocalizedString(@"Have you recently experienced some major changes? E.g. moving, pregnancy, work...",nil);
				break;
			case 17:
				preguntaTexto.text = NSLocalizedString(@"Have you recently lost a loved one? ",nil);
                [boton4 setTitle:NSLocalizedString(@"Yes",nil) forState:UIControlStateNormal];
				[boton3 setTitle:@" " forState:UIControlStateNormal];
				[boton2 setTitle:@" " forState:UIControlStateNormal];
				[boton1 setTitle:NSLocalizedString(@"No",nil) forState:UIControlStateNormal];
				break;
			case 18:
				preguntaTexto.text = NSLocalizedString(@"Do you work in a noisy place?",nil);
                [boton4 setTitle:NSLocalizedString(@"A lot",nil) forState:UIControlStateNormal];
				[boton3 setTitle:NSLocalizedString(@"Often",nil) forState:UIControlStateNormal];
				[boton2 setTitle:NSLocalizedString(@"Sometimes",nil) forState:UIControlStateNormal];
				[boton1 setTitle:NSLocalizedString(@"Never",nil) forState:UIControlStateNormal];
				break;
			case 19:
				preguntaTexto.text = NSLocalizedString(@"Have you been eating much lately?",nil);
                [boton4 setTitle:NSLocalizedString(@"A lot more",nil) forState:UIControlStateNormal];
				[boton3 setTitle:NSLocalizedString(@"Some more",nil) forState:UIControlStateNormal];
				[boton2 setTitle:NSLocalizedString(@"A little more",nil) forState:UIControlStateNormal];
				[boton1 setTitle:NSLocalizedString(@"No",nil) forState:UIControlStateNormal];
				break;
			case 20:
				preguntaTexto.text = NSLocalizedString(@"Do you drivelong distances every day?",nil);

				break;
		}
	}
	else {
        ResultadoTest *resultadoTest;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            resultadoTest = [[ResultadoTest alloc] initWithNibName:@"ResultadoTest-iPad" bundle:nil];
        } else {
            resultadoTest = [[ResultadoTest alloc] initWithNibName:@"ResultadoTest" bundle:nil];
        }
        resultadoTest.puntos = puntos;
		resultadoTest.insomnio = insomnio;
		[self.navigationController pushViewController:resultadoTest animated:YES];
		[resultadoTest release];
		puntos = 0;
		contador = 0;
		[self siguiente];
	}

}

- (void)dealloc {
	[boton1 release];
	[boton2 release];
	[boton3 release];
	[boton4 release];
	[preguntaTexto release]; 
	[progresoTexto release]; 
	[progresoBarra release]; 
    [super dealloc];
	
}


@end
