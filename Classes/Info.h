//
//  Info.h
//  iDStress
//
//  Created by Leopold Riola on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface Info : UIViewController {
	IBOutlet UITextView *infoTexto1;
	IBOutlet UITextView *infoTexto2;
	
	IBOutlet UIImageView *guia;
	NSMutableString *texto1;
	NSMutableString *texto2;
}
@property (nonatomic, retain) IBOutlet UITextView *infoTexto1;
@property (nonatomic, retain) IBOutlet UITextView *infoTexto2;

@property (nonatomic, retain) IBOutlet UIImageView *guia;
@property(nonatomic, retain) NSMutableString *texto1;
@property(nonatomic,retain) NSMutableString *texto2;

@end
