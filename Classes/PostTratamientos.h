//
//  PostTratamientos.h
//  iDStress
//
//  Created by Pold on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tratamientos.h"
#import "FBConnect.h"

@interface PostTratamientos : UIViewController <FBDialogDelegate>{
	IBOutlet UISlider *valoracion;
	IBOutlet UIImageView *guia;
}
@property (nonatomic, retain) IBOutlet UIImageView *guia;
@property (nonatomic, retain) IBOutlet UISlider *valoracion;
- (IBAction) fin: (id)sender;
- (IBAction) compartirFacebook: (id)sender;
- (IBAction) compartirTwitter: (id)sender;
@end
