//
//  UserInfo.h
//  Starbates
//
//  Created by Leopold Riola on 29/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FBConnect.h"

@interface UserInfo : NSObject <FBSessionDelegate>

@property (nonatomic, strong) Facebook *facebook;

+ (UserInfo*)sharedInstance;
- (void) startFbLogin;

@end
