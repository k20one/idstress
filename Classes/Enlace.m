//
//  Enlace.m
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Enlace.h"
#import <MapKit/MKAnnotation.h>
#import "DDAnnotation.h"
#import "Flurry.h"
@implementation Enlace
@synthesize scrollView, mapa;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Enlace"];
    
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	SET_NAVIGATION_BAR;
	scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 1070);
	scrollView.frame = CGRectMake(0, -40, self.view.frame.size.width, self.view.frame.size.height+40);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.view setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-iPad.png"]]];
   } else {
        [self.view setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    }
    
    [self.view addSubview:scrollView];
	self.title = @"Enlace";
	
	MKMapView *mapView = [[[MKMapView alloc] initWithFrame:self.mapa.frame] autorelease];
	mapView.mapType = MKMapTypeStandard;
	
	CLLocationCoordinate2D coord = {latitude: 41.394518748, longitude: 2.1489263333};
	MKCoordinateSpan span = {latitudeDelta: 0.001, longitudeDelta: 0.01};
	MKCoordinateRegion region = {coord, span};
	[mapView setRegion:region];
	
	
	DDAnnotation *annotation = [[[DDAnnotation alloc] initWithCoordinate:coord addressDictionary:nil] autorelease];
	annotation.title = @"Centro Enlace";
	//annotation.subtitle = @"Medicina convencional y alternativa";
	
	[mapView addAnnotation:annotation];
	
	[scrollView addSubview:mapView];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[scrollView release];
	[mapa release];
    [super dealloc];
}


@end
