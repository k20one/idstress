//
//  Intro.m
//  iDStress
//
//  Created by Pold on 15/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Intro.h"
#include <stdlib.h>
#import "General.h"
#import "Flurry.h"
#import "Alerta.h"
#import "SessionM.h"
#import "SMPortalButton.h"

@implementation Intro
@synthesize frase;


- (BOOL)shouldAutorotate
{
    return NO;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Home",nil);
	[self.navigationController setNavigationBarHidden:YES];
    
    
    SET_NAVIGATION_BAR;
    
    SMPortalButton* portalButton= [SMPortalButton buttonWithType:UIButtonTypeCustom];
    [portalButton setFrame:CGRectMake(25, 25, 40, 40)];
    [portalButton.button setTitle:@"" forState:UIControlStateNormal];
    [portalButton.button setImage:[UIImage imageNamed:@"sessionM"] forState:UIControlStateNormal];
    [portalButton.button setImage:[UIImage imageNamed:@"transparent"] forState:UIControlStateDisabled];
    [self.view addSubview:portalButton];

    if(IS_IPHONE_5){
        UIImage *image = [UIImage imageNamed:@"Default568h@2x.png"];
        backgroundImage.image = image;
        backgroundImage.frame = CGRectMake(backgroundImage.frame.origin.x, backgroundImage.frame.origin.y-89, [[UIScreen mainScreen ] bounds].size.width, [[UIScreen mainScreen ] bounds].size.height);
        fraseDelDiaLabel.frame = CGRectMake(fraseDelDiaLabel.frame.origin.x, fraseDelDiaLabel.frame.origin.y+40, fraseDelDiaLabel.frame.size.width, fraseDelDiaLabel.frame.size.height);
        frase.frame = CGRectMake(frase.frame.origin.x, frase.frame.origin.y+40, frase.frame.size.width, frase.frame.size.height);
        
//        UIViewController *crazy = [[UIViewController alloc] initWithNibName:nil bundle:nil];
//        [self presentViewController:crazy animated:NO completion:^{
//            [self dismissViewControllerAnimated:NO completion:nil];
//            [crazy release];
//        }];
        
    }
    
    NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:NSLocalizedString(@"FrasesDelDia",nil) ofType:@"txt"] encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *frases = [[NSMutableArray alloc]initWithArray:[fileText componentsSeparatedByString:@"\n"]];

    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger dia = [components day];
    NSInteger mes = [components month];
    NSLog(@"Frases: %u", frases.count);
    NSLog(@"Frases: %i %i", dia, mes);
    NSLog(@"%@", frases);
    int index = ((dia + ((mes%3)*30))%frases.count);
	frase.text = [frases objectAtIndex:index];
    [frases release];
    
     self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Alerts",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showAlerts)] autorelease];
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareTouched)];
    [self.navigationItem setRightBarButtonItem:shareButton];
    [shareButton release];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"Tab bar" withParameters:[NSDictionary dictionaryWithObject:@"Home" forKey:@"Tab"]];
}
- (void)dealloc {
	[frase release];
    [backgroundImage release];
    [fraseDelDiaLabel release];
    [alerta release];
    [super dealloc];
}
-(void)showAlerts{
    [Flurry logEvent:@"Alerta"];
    if (!alerta)  alerta = [[Alerta alloc] initWithNibName:@"Alerta" bundle:nil];
    [self.navigationController pushViewController:alerta animated:YES];
}

- (void) shareTouched {
    NSArray *activityItems = @[[NSString stringWithFormat:@"%@ #iDStress",frase.text]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems applicationActivities:nil];
    [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
        if (completed) {
            SMAction(@"Quote");
            [Flurry logEvent:@"Share Finished" withParameters:@{@"Activity Type":activityType}];
        }
    }];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
@end
