//
//  EfectoFuego.h
//  iDStress
//
//  Created by Sophy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.

#import "cocos2d.h"
#import <map>

// HelloWorld Layer
@interface EfectoFuego : CCLayer
{
	CCParticleSystem						*emitter;
	CCParticleFire						*hoguera1;
	CCParticleFire						*hoguera2;
	std::map<NSUInteger, CGPoint>	fingers;
	CCMenu				*backMenu;
	CCSprite			*background;
	CGSize size;
}

@property (readwrite,retain) CCParticleSystem *emitter;
@property (readwrite,retain) CCMenu *backMenu;


// returns a Scene that contains the HelloWorld as the only child
+(id) scene;
-(CCParticleSystem*) getFireParticle;
@end
