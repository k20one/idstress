//
//  Test.h
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResultadoTest.h"

@interface Test : UIViewController {
	
	IBOutlet UIButton *boton1;
	IBOutlet UIButton *boton2;
	IBOutlet UIButton *boton3;
	IBOutlet UIButton *boton4;
	IBOutlet UILabel *preguntaTexto;
	IBOutlet UILabel *progresoTexto;
	IBOutlet UIProgressView *progresoBarra;
}

@property (nonatomic, retain) IBOutlet UIButton *boton1;
@property (nonatomic, retain) IBOutlet UIButton *boton2;
@property (nonatomic, retain) IBOutlet UIButton *boton3;
@property (nonatomic, retain) IBOutlet UIButton *boton4;
@property (nonatomic, retain) IBOutlet UILabel *preguntaTexto;
@property (nonatomic, retain) IBOutlet UILabel *progresoTexto;
@property (nonatomic, retain) IBOutlet UIProgressView *progresoBarra;

- (IBAction) respuestaBoton: (id) sender;
- (void)siguiente;

@end
