//
//  SSStoreManager.m
//  StopSmoking
//
//  Created by Thomas Hanks on 2/5/13.
//  Copyright (c) 2013 Demand Media, Inc. All rights reserved.
//

#import "StoreManager.h"
#import <StoreKit/StoreKit.h>
#import "MBProgressHUD.h"

#pragma mark - Private Interface
@interface StoreManager ()<SKStoreProductViewControllerDelegate,SKProductsRequestDelegate,SKPaymentTransactionObserver>
@property (retain) NSArray* myProducts;
@property (retain) NSString* requestId;
@property (retain) MBProgressHUD *progress;
@end

@implementation StoreManager
#pragma mark - Constructors
-(id)init{
    self = [super init];
    if(self == nil)
        return nil;
    self.myProducts = [NSArray array];
    
    return self;
}

-(void)dealloc{
    [super dealloc];
}
#pragma mark - Accessors
SYNTHESIZE_SINGLETON_FOR_CLASS(StoreManager);
@synthesize myProducts = _myProducts;
@synthesize requestId = _requestId;
@synthesize progress = _progress;

#pragma mark - Private Methods

- (void) requestProductData
{
     NSLog(@"Request Product Data");
    SKProductsRequest *request= [[[SKProductsRequest alloc] initWithProductIdentifiers:
                                 [NSSet setWithObjects: iDStress1, iDStress2, iDStress3, iDStress4, iDStress5, iDStress6, nil]] autorelease];
    request.delegate = self;
    [request start];
}


//
// saves a record of the transaction by storing the receipt to disk
//
- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Recording Transaction");
    //  if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseProUpgradeProductId])
    //  {
    //    // save the transaction receipt to disk
    //    [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"proUpgradeTransactionReceipt" ];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    //  }
}

//
// enable pro features
//
- (void)provideContent:(NSString *)productId
{
    NSLog(@"Providing Content");
    //  if ([productId isEqualToString:kInAppPurchaseProUpgradeProductId])
    //  {
    //    // enable the pro features
    //    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isProUpgradePurchased" ];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    //  }
}

//
// removes the transaction from the queue and posts a notification with the transaction result
//
- (void)finishTransaction:(SKPaymentTransaction *)transaction wasSuccessful:(BOOL)wasSuccessful
{
    NSLog(@"Finished transaction");
    // remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    if (wasSuccessful){
        NSLog(@"Finish transaction successful");
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:YES forKey:@"Purchased"]; //To remove ads or whatever
        //Remove Ads
        if ([transaction.payment.productIdentifier isEqualToString:iDStress1]) {
            [defaults setBool:YES forKey:iDStress1];
        }
        else if ([transaction.payment.productIdentifier isEqualToString:iDStress2]) {
            [defaults setBool:YES forKey:iDStress2];
        }
        else if ([transaction.payment.productIdentifier isEqualToString:iDStress3]) {
            [defaults setBool:YES forKey:iDStress3];
        }
        else if ([transaction.payment.productIdentifier isEqualToString:iDStress4]) {
            [defaults setBool:YES forKey:iDStress4];
        }
        else if ([transaction.payment.productIdentifier isEqualToString:iDStress5]) {
            [defaults setBool:YES forKey:iDStress5];
        }
        else if ([transaction.payment.productIdentifier isEqualToString:iDStress6]) {
            [defaults setBool:YES forKey:iDStress6];
        }
        [defaults synchronize];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TransactionSuccessful" object:nil];
    }
    else
    {
        NSLog(@"Finish transaction failed");
    }
}

//
// called when the transaction was successful
//
- (void)completeTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Complete transaction");
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has been restored and and successfully completed
//
- (void)restoreTransaction:(SKPaymentTransaction *)transaction
{
    NSLog(@"Restore transaction");
    [self recordTransaction:transaction.originalTransaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [self finishTransaction:transaction wasSuccessful:YES];
}

//
// called when a transaction has failed
//
- (void)failedTransaction:(SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled){
        // error!
        NSLog(@"Transaction error description: %@ \nCode: %i", transaction.error.localizedDescription, transaction.error.code);
        [self finishTransaction:transaction wasSuccessful:NO];
    }
    else{
        // this is fine, the user just cancelled, so don’t notify
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

-(void)restoreAllPurchases;
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)purchaseItem:(SKProduct*)product;
{
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (void)purchaseItemWithIdentifier:(NSString*)identifier
{
    
    if (!self.myProducts || [self.myProducts count]<=0) {
        //We are not ready.
        NSLog(@"Products not ready");
        self.requestId = identifier;
        [self requestProductData];
    }
    else {
        NSLog(@"Fetching product");
        SKProduct *requestedProduct = nil;
        for (SKProduct *product in self.myProducts) {
            if ([product.productIdentifier isEqualToString:identifier]) {
                requestedProduct = product;
                continue;
            }
        }
        if (!requestedProduct) {
            NSLog(@"Product Not Found: %@", identifier);
            return;
        }
        [self purchaseItem:requestedProduct];
        self.requestId = nil;
    }
}




#pragma mark - SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    for (SKProduct *product in response.products) {
        NSLog(@"Product %@", product.productIdentifier);
    }
    
    // Save a reference to the products list.
    self.myProducts = [response.products retain];
    if (self.requestId) {
        NSLog(@"Requested id %@", self.requestId);
        [self purchaseItemWithIdentifier:self.requestId];
    }
}

#pragma mark - SKPaymentTransactionObserver
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_3_0);
{
    NSLog(@"Updated transactions");
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                NSLog(@"Purchased");
                [[SKPaymentQueue defaultQueue] startDownloads:transaction.downloads];
                //[self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                NSLog(@"Failed");
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                NSLog(@"Restored");
                [[SKPaymentQueue defaultQueue] startDownloads:transaction.downloads];
                //[self restoreTransaction:transaction];
                break;
            default:
                break;
        }
    }
}

#pragma mark - Downloads
-(void) download:(SKPaymentTransaction *)transaction
{
    NSAssert(transaction.transactionState == SKPaymentTransactionStatePurchased ||
             transaction.transactionState == SKPaymentTransactionStateRestored, @"The payment transaction must be completed");
    
    if ([transaction.downloads count])
    {
        [[SKPaymentQueue defaultQueue] startDownloads:transaction.downloads];
    }
}

-(void) paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads
{
    for (SKDownload *download in downloads)
    {
        switch (download.downloadState)
        {
            case SKDownloadStateActive:
            {
#ifdef DEBUG
                NSLog(@"%f", download.progress);
                NSLog(@"%f remaining", download.timeRemaining);
#endif
                
                if (download.progress == 0.0)
                {
                    
                }
                
                if (!_progress) {
                    _progress = [[MBProgressHUD alloc] initWithView:[[UIApplication sharedApplication] keyWindow]];
                    UIView *window= [[UIApplication sharedApplication] keyWindow];
                    [window addSubview:_progress];
                    
                    [_progress show:YES];
                    [_progress setDimBackground:YES];
                    [_progress setLabelText:NSLocalizedString(@"Downloading", nil)];
                    [_progress setMode:MBProgressHUDModeAnnularDeterminate];
                }
                
               [_progress setProgress:download.progress];
                
                break;
            }
                
            case SKDownloadStateCancelled: { break; }
            case SKDownloadStateFailed:
            {
                [_progress setDimBackground:NO];
                [_progress hide:YES];
                [_progress release];
                _progress = nil;
               NSLog(@"Fail! Implement me!");
                break;
            }
                
            case SKDownloadStateFinished:
            {          
                [_progress setDimBackground:NO];
                [_progress hide:YES];
                [_progress release];
                _progress = nil;
                
                [self processDownload:download];
                [self finishTransaction:download.transaction wasSuccessful:YES];
                [[[UIApplication sharedApplication] keyWindow].rootViewController dismissViewControllerAnimated:NO completion:nil];
                break;
            }
                
            case SKDownloadStatePaused:
            {
#ifdef DEBUG
                NSLog(@"SKDownloadStatePaused");
#endif
                break;
            }
                
            case SKDownloadStateWaiting:
            {
#ifdef DEBUG
                NSLog(@"SKDownloadStateWaiting");
#endif
                break;
            }
        }
    }
}

- (void) processDownload:(SKDownload*)download;
{
    // convert url to string, suitable for NSFileManager
    NSString *path = [download.contentURL path];
    
    // files are in Contents directory
    path = [path stringByAppendingPathComponent:@"Contents"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *files = [fileManager contentsOfDirectoryAtPath:path error:&error];
    
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    directory = [directory stringByAppendingPathComponent:@"Downloads"];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    directory = [directory stringByAppendingPathComponent:[prefs stringForKey:@"Tratamiento"]];
    
    if ([fileManager fileExistsAtPath:directory] == NO) {
        
        NSError *error;
        if ([fileManager createDirectoryAtPath:directory withIntermediateDirectories:YES attributes:nil error:&error] == NO) {
            NSLog(@"Error: Unable to create directory: %@", error);
        }
        
        NSURL *url = [NSURL fileURLWithPath:directory];
        // exclude downloads from iCloud backup
        if ([url setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error] == NO) {
            NSLog(@"Error: Unable to exclude directory from backup: %@", error);
        }
    }
    
    
    for (NSString *file in files) {
        NSString *fullPathSrc = [path stringByAppendingPathComponent:file];
        NSString *fullPathDst = [directory stringByAppendingPathComponent:file];
        
        // not allowed to overwrite files - remove destination file
        [fileManager removeItemAtPath:fullPathDst error:NULL];
        
        if ([fileManager moveItemAtPath:fullPathSrc toPath:fullPathDst error:&error] == NO) {
            NSLog(@"Error: unable to move item: %@", error);
        }
    }
    
    // NOT SHOWN: use download.contentIdentifier to tell your model that we've been downloaded
}

@end
