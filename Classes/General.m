//
//  General.m
//  iDStress
//
//  Created by Pold on 24/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "General.h"
#import "Intro.h"
#import "Flurry.h"

@implementation General
@synthesize rootController;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    //    intro.rootController = rootController;
    //	[rootController setSelectedViewController:nil];
    rootController.view.frame = self.view.bounds;
	[self.view addSubview:rootController.view];
    
    if ([self.rootController.tabBar respondsToSelector:@selector(setTranslucent:)]) {
        self.rootController.tabBar.tintColor = [UIColor colorWithRed:45.0/255.0 green:144.0/255.0 blue:24.0/255.0 alpha:1.0];
        self.rootController.tabBar.barTintColor = [UIColor colorWithRed:222/255.0 green:231/255.0 blue:228/255.0 alpha:1.0];
        self.rootController.tabBar.translucent = NO;
    }
    
     [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //    [intro release];
	for (int i=0; i<[rootController.viewControllers count]; i++) {
		[(UIViewController *)[rootController.viewControllers objectAtIndex:i] viewWillAppear:YES];
	}
    [Flurry logAllPageViews:rootController];
}


 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
  

- (BOOL)shouldAutorotate
{
    return YES;
}


- (void)dealloc {
	[rootController release];
    [super dealloc];
}


@end
