/*
 This work is licensed under the Creative Commons Attribution-Share Alike 3.0 United States License. 
 To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/us/ or 
 send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.
 
 Jed Laudenslayer
 http://kwigbo.com
 
 */

#import "MessageNode.h"

@implementation MessageNode

int const MISS_MESSAGE = 0;
int const PERFECT_MESSAGE = 1;
int const CORRECT_MESSAGE = 2;

@synthesize miss, perfect, correct;

- (void) dealloc
{
	[correct release];
	[miss release];
	[perfect release];
	[super dealloc];
}

-(id) init
{
	self = [super init];
	
	if (self)
	{
		CCSprite *m = [[CCSprite alloc] initWithFile:@"miss.png"];
		self.miss = m;
		[m release];
		
		miss.anchorPoint = ccp(0, 0);
		
		[self addChild:miss];
		
		CCSprite *p = [[CCSprite alloc] initWithFile:@"perfect.png"];
		self.perfect = p;
		[p release];
		
		[self addChild:perfect];
		
		perfect.anchorPoint = ccp(0, 0);
		
		CCSprite *c = [[CCSprite alloc] initWithFile:@"correct.png"];
		self.correct = c;
		[c release];
		
		[self addChild:correct];
		
		correct.anchorPoint = ccp(0, 0);
		
		miss.position = ccp(0, -20);
		perfect.position = ccp(0, -20);
		
		[correct runAction:[CCFadeOut actionWithDuration:.01]];
		[perfect runAction:[CCFadeOut actionWithDuration:.01]];
		[miss runAction:[CCFadeOut actionWithDuration:.01]];
	}
	
	return self;
}

- (void) showMessage:(int) message
{
	CCSprite *sprite=miss;
	
	if(message == MISS_MESSAGE)
	{
		sprite = miss;
		missVisible = YES;
	}else if(message == CORRECT_MESSAGE)
	{
		sprite = correct;
		correctVisible = YES;
	}else if (message == PERFECT_MESSAGE)
		sprite = perfect;
	
	[sprite runAction:[CCFadeTo actionWithDuration:.01 opacity:0]];
		
	[sprite runAction:[CCSequence actions:
				[CCFadeTo actionWithDuration:.1 opacity:250],
				[CCDelayTime actionWithDuration:.5], 
				[CCFadeTo actionWithDuration:.1 opacity:0], nil]];
}

@end
