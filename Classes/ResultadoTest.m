//
//  ResultadoTest.m
//  iDStress
//
//  Created by Pold on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ResultadoTest.h"
#import "PreGestoRelax.h"
#import "PreConcentracion.h"
#import "PrePanicButton.h"
#import "PreInsomnia.h"
#import "PreRelajacionBase.h"
#import "PreRespiracion.h"
#import "Info.h"
#import "Flurry.h"
#import "SessionM.h"

@implementation ResultadoTest
@synthesize evalTexto1,evalTexto2,evalTexto3,evalTexto4, puntos, tratamiento1, tratamiento2, insomnio, guia, scrollView;

-(IBAction) info: (id)sender {
    Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
    info.texto1 =  [[[NSMutableString alloc] initWithString: NSLocalizedString(@"This test will give you an approximate estimate of your stress level.",nil)] autorelease];
	info.texto2 = [[[NSMutableString alloc] initWithString: NSLocalizedString(@"The goal of this test is to evaluate your stress and then suggest exercises appropriate to your level.It is not a certificated test and has 3 possible results: \nAlertness phase \nResistance phase\nExhaustion phase",nil)] autorelease];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    SMAction(@"Test");
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.view setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-iPad.png"]]];
    } else {
        [self.view setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];
    }
	self.title = NSLocalizedString(@"Results",nil);
	SET_INFO_BUTTON;
	evalTexto1.text = NSLocalizedString(@"Your stress level is in the",nil);
	
	if (puntos<20) {
        [Flurry logEvent:@"Test" withParameters:[NSDictionary dictionaryWithObject:@"Bajo" forKey:@"Fase"]];
		evalTexto2.text = NSLocalizedString(@"Alertness phase",nil);
		evalTexto3.text = NSLocalizedString(@"It means you are managing stress well",nil);
		evalTexto4.text = NSLocalizedString(@"You should learn to prioritize what’s urgent over what’s important before tension weakens you. Keep relaxing with iDStress to maintain your inner balance.",nil);
		[tratamiento1 setTitle:NSLocalizedString(@"Basic Relaxation",nil) forState:UIControlStateNormal];
		[tratamiento2 setTitle:NSLocalizedString(@"Breathing",nil) forState:UIControlStateNormal];
	}
	else if (puntos>=20&&puntos<40){
        [Flurry logEvent:@"Test" withParameters:[NSDictionary dictionaryWithObject:@"Medio" forKey:@"Fase"]];
		evalTexto2.text = NSLocalizedString(@"Resistance phase",nil);
		evalTexto3.text = NSLocalizedString(@"Your energy is running low. You are in a stress phase.",nil);
		[tratamiento1 setTitle:NSLocalizedString(@"Touch Relax",nil) forState:UIControlStateNormal];
		[tratamiento2 setTitle:NSLocalizedString(@"Concentration",nil) forState:UIControlStateNormal];
		
		evalTexto4.text = NSLocalizedString(@"Keep your sleeping and nutrition habits. Find a healthy distraction. You are very likely to feel stress. Try to relax before approaching a problem. We recommend you to keep using iDStress.",nil);
		
	}
	else if (puntos>=40){
        [Flurry logEvent:@"Test" withParameters:[NSDictionary dictionaryWithObject:@"Alto" forKey:@"Fase"]];
		evalTexto2.text = NSLocalizedString(@"Exhaustion phase",nil);
		evalTexto3.text = NSLocalizedString(@"CAUTION! You are in a chronic or severe stress phase.",nil);
		[tratamiento1 setTitle:NSLocalizedString(@"Panic Button",nil) forState:UIControlStateNormal];
		[tratamiento2 setTitle:NSLocalizedString(@"Insomnia",nil) forState:UIControlStateNormal];
		
		evalTexto4.text = NSLocalizedString(@"Try to get some help, specially if you have: sleeping and eating disturbances, unsatisfaction feelings, irascibility... You should try to relax in a natural way, avoiding self-medication of psychotropic. We recommend you to keep using iDStress.",nil);
	}
	scrollView.frame = self.view.frame;
	scrollView.contentSize = CGSizeMake(self.view.frame.size.width, 670);
	[self.view addSubview:scrollView];
	
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-o.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-o.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-o.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-o.png";
			break;
		default:
			imagen = @"Avatar_Fuego-o.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-o.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-o.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-o.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-o.png";
			break;
		default:
			imagen = @"Avatar_Fuego-o.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}

- (IBAction) tratamientoBoton: (id)sender{
	if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Panic Button",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Panic Button" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PrePanicButton *recomendacion =[[PrePanicButton alloc] initWithNibName:@"PrePanicButton-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PrePanicButton *recomendacion =[[PrePanicButton alloc] initWithNibName:@"PrePanicButton" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
	else if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Touch Relax",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Gesto Relax" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PreGestoRelax *recomendacion =[[PreGestoRelax alloc] initWithNibName:@"PreGestoRelax-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PreGestoRelax *recomendacion =[[PreGestoRelax alloc] initWithNibName:@"PreGestoRelax" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
	else if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Insomnia",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Insomnio" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PreInsomnia *recomendacion =[[PreInsomnia alloc] initWithNibName:@"PreInsomnia-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PreInsomnia *recomendacion =[[PreInsomnia alloc] initWithNibName:@"PreInsomnia" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
	else if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Basic Relaxation",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Relajacion Base" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PreRelajacionBase *recomendacion =[[PreRelajacionBase alloc] initWithNibName:@"PreRelajacionBase-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PreRelajacionBase *recomendacion =[[PreRelajacionBase alloc] initWithNibName:@"PreRelajacionBase" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
	else if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Concentration",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Concentracion" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PreConcentracion *recomendacion =[[PreConcentracion alloc] initWithNibName:@"PreConcentracion-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PreConcentracion *recomendacion =[[PreConcentracion alloc] initWithNibName:@"PreConcentracion" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
    else if ([[sender currentTitle] isEqualToString:NSLocalizedString(@"Breathing",nil)]){
        [Flurry logEvent:@"Sugerencia test" withParameters:[NSDictionary dictionaryWithObject:@"Respiracion" forKey:@"Tratamiento"]];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            PreRespiracion *recomendacion =[[PreRespiracion alloc] initWithNibName:@"PreRespiracion-iPad" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        } else {
            PreRespiracion *recomendacion =[[PreRespiracion alloc] initWithNibName:@"PreRespiracion" bundle:nil];
            [self.navigationController pushViewController:recomendacion animated:YES];
            [recomendacion release];
        }
	}
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
