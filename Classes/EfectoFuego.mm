//
//  EfectoFuego.m
//  iDStress
//
//  Created by Sophy Swidarowicz on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SessionM.h"
#import "SimpleAudioEngine.h"
#import "EfectoFuego.h"
#include <iostream>
#include <algorithm>


#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
#define PARTICLE_FIRE_NAME @"fire.pvr"
#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)
#define PARTICLE_FIRE_NAME @"fire.png"
#endif

#define FOGATA_WIDTH 100
#define DOWN_SPEED 50

@implementation EfectoFuego
@synthesize emitter, backMenu;

-(BOOL) ccTouchBegan:(UITouch*)t withEvent:(UIEvent*)event
{
	
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
	{
		CGPoint location = [touch locationInView: [touch view]];
		CGPoint convertedLocation = [[CCDirector sharedDirector] convertToGL:location];
		emitter.position = convertedLocation;
		NSUInteger n = [touch hash];
		fingers[ n ] = convertedLocation;
		
	}
	
	return YES;
}
- (void)ccTouchMoved:(UITouch*)t withEvent:(UIEvent *)event
{
	NSSet *allTouches = [event allTouches]; 
	int x_min = (size.width - FOGATA_WIDTH) / 2;
	int y_min = size.height/6;
	int y_max = y_min + hoguera1.speed;
	
	for (UITouch *touch in allTouches)
	{
		CGPoint location = [touch locationInView: [touch view]];
		CGPoint convertedLocation = [[CCDirector sharedDirector] convertToGL:location];
		emitter.position = convertedLocation;
		NSUInteger n = [touch hash];		
		

		
		if ( fingers.find(n) != fingers.end() )
		{
			CGPoint old = fingers[n];

			// Si esta entre las x
			if ( (old.x > x_min && old.x < x_min + FOGATA_WIDTH) ||
				(convertedLocation.x > x_min && convertedLocation.x < x_min + FOGATA_WIDTH)
				)
			{
				// Si esta entre las y
				if( (old.y > y_min && old.y < y_max) ||
				   (convertedLocation.y > y_min && convertedLocation.y < y_max)
				   )
				{
					float up = convertedLocation.y - old.y;
					if (up > 0)
					{
						float max_speed = std::min( 120.0f , hoguera1.speed + up);
						hoguera1.speed = max_speed;
						hoguera2.speed = max_speed;
						hoguera1.life = hoguera2.life = 3 + 3 * ( max_speed / 120 );
					}
				}
			}
			fingers[n] = convertedLocation;
		}

	}
}

- (void)ccTouchEnded:(UITouch*)t withEvent:(UIEvent *)event
{
	/*
	hoguera1.life = 3;
	hoguera1.speed = 60;
	hoguera2.life = 3;
	hoguera2.speed = 60;
	 */
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
		fingers.erase( [touch hash] );
}

- (void)ccTouchCancelled:(UITouch *)t withEvent:(UIEvent *)event
{
	[self ccTouchEnded: t withEvent:event ];
}
//#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	EfectoFuego *layer = [EfectoFuego node];
	layer.isTouchEnabled=YES;
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{		
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) {
		
		CCDirector *director = [CCDirector sharedDirector];
		self.isTouchEnabled = YES;
		if( ! [director enableRetinaDisplay:YES] )
		{
			
			CCMenuItem *backMenuItem = [CCMenuItemImage 
										itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png" 
										target:self selector:@selector(backButtonTapped:)];
			backMenuItem.position = ccp(30, 30);
			backMenu = [CCMenu menuWithItems:backMenuItem, nil];
			backMenu.position = CGPointZero;
			
			
		}
		else 
		{
			CCMenuItem *backMenuItem = [CCMenuItemImage 
										itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png" 
										target:self selector:@selector(backButtonTapped:)];
			backMenuItem.position = ccp(30, 30);
			backMenu = [CCMenu menuWithItems:backMenuItem, nil];
			backMenu.position = CGPointZero;
		}
		self.isAccelerometerEnabled = YES;
		[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
		
		[self addChild:backMenu];
		backMenu.visible = YES;
		
		emitter = [self getFireParticle];
		[self addChild: emitter z:10];
		size = [[CCDirector sharedDirector] winSize];
		
		hoguera1 = [[[CCParticleFire alloc]initWithTotalParticles:500] autorelease];
		hoguera1.emissionRate = 500.0f / 6.0f;
		CGPoint var;
		var.x = FOGATA_WIDTH;
		var.y = 10;
		hoguera1.posVar = var;
		[self addChild:hoguera1 z:10];
		hoguera1.position = ccp( size.width /2 , size.height/6 );
		
		hoguera2 = [[[CCParticleFire alloc]initWithTotalParticles:500] autorelease];
		hoguera2.emissionRate = 500.0f / 6.0f;
		var.x = 50;
		var.y = 10;
		hoguera2.posVar = var;
		[self addChild: hoguera2 z:10];
		hoguera2.position = ccp( size.width /2 , size.height/6 - 25 );		
		
		emitter.texture = [[CCTextureCache sharedTextureCache] addImage: PARTICLE_FIRE_NAME];	
		emitter.position = ccp( size.width /2 , size.height/3 );
		
		[ [ CCTouchDispatcher sharedDispatcher ] addTargetedDelegate:self priority:0 swallowsTouches:YES ];	
		
		 [ self schedule:@selector( update: ) ];   
	}
	return self;
}

//Coloca las partículas en coordenadas 
-(void) setEmitterPosition
{
	if( CGPointEqualToPoint( emitter.sourcePosition, CGPointZero ) ) 
		emitter.position = ccp(200, 70);
}

//genera el botón para regresar a la escena anterior//salida
- (void)backButtonTapped:(id)sender {
	CCDirector *director = [CCDirector sharedDirector];
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
    SMAction(@"Fire");
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// [self removeChild:emitter cleanup:true];
	// [emitter release];

	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}

//
// Recibiendo llamada, pausa la aplicación
-(void) applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

// llamada rechazada
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	[[CCDirector sharedDirector] startAnimation];
}

// aplicación será killed
- (void)applicationWillTerminate:(UIApplication *)application
{	
	CC_DIRECTOR_END();
}

// purgar memoria 
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

-(CCParticleSystem*) getFireParticle
{
	CCParticleSystem* p = [CCParticleFire node];
	CGPoint var;
	var.x = 1;
	var.y = 0;
	p.posVar = var;
	return p;
}

-( void )update:( ccTime )dt 
{
	float max_speed = std::max( 40.0f , hoguera1.speed - DOWN_SPEED * dt);
	hoguera1.speed = max_speed;
	hoguera2.speed = max_speed;
	hoguera1.life = hoguera2.life = 3 + 3 * ( max_speed / 120 );
}

@end



