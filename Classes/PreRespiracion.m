//
//  PreRespiracion.m
//  iD-Stress
//
//  Created by Pold on 18/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PreRespiracion.h"
#import "PostTratamientos.h"
#import "Info.h"
#import <MediaPlayer/MediaPlayer.h>
#import "iD_StressAppDelegate.h"
#import "Flurry.h"
#import "StoreManager.h"

@interface MyMovieViewController : MPMoviePlayerViewController
@end

@implementation MyMovieViewController
- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}



@end

@implementation PreRespiracion
@synthesize empezar, guia, animacion;

-(IBAction) info: (id)sender {
	Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
    info.texto1 =  [[[NSMutableString alloc] initWithString: NSLocalizedString(@"\n This video will teach you how to breathe correctly.",nil)] autorelease];
	info.texto2 = [[[NSMutableString alloc] initWithString: NSLocalizedString(@"\n Proper breathing improves your general state and helps manage stress. \n\n WARNING: Pregnant women should not practice the breathing pause.",nil)] autorelease];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}

- (IBAction) empezar: (id)sender{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if ([prefs boolForKey:iDStress6]) {
        
        NSDictionary *dict = [[[NSDictionary alloc] initWithObjectsAndKeys:@"Respiracion",@"Tratamiento", nil] autorelease];
        [Flurry logEvent:@"Tratamiento Reproducido" withParameters:dict];
        ((iD_StressAppDelegate *)[[UIApplication sharedApplication] delegate]).videoPlaying = YES;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        NSString *directory = [paths objectAtIndex:0];
        directory = [directory stringByAppendingPathComponent:@"Downloads"];
        directory = [directory stringByAppendingPathComponent:@"Respiracion"];
        NSString *path = [NSString stringWithFormat:NSLocalizedString(@"%@/en.lproj/%@",nil),
                directory, @"Lombard.m4v"];
        NSURL *movieURL = [NSURL fileURLWithPath:path];
        
        if ([movieURL checkResourceIsReachableAndReturnError:nil] == NO) {
            [prefs setBool:NO forKey:iDStress6];
            [self viewWillAppear:NO];
            return;
        }
        
        MyMovieViewController *moviePlayer = [[[MyMovieViewController alloc] initWithContentURL:movieURL] autorelease];
        [moviePlayer.moviePlayer prepareToPlay];
        [moviePlayer.moviePlayer play];
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            moviePlayer.moviePlayer.scalingMode= MPMovieScalingModeAspectFit;
        }
        //[[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight];
        //[[UIApplication sharedApplication] setStatusBarHidden:YES];
        [self  presentMoviePlayerViewControllerAnimated:moviePlayer];
        [self setWantsFullScreenLayout:YES];
    }
    else {
        [prefs setObject:@"Respiracion" forKey:@"Tratamiento"];
        [[StoreManager sharedStoreManager] purchaseItemWithIdentifier:iDStress6];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchased) name:@"TransactionSuccessful" object:nil];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TransactionSuccessful" object:nil];
}

- (void) purchased {
    [self viewWillAppear:NO];
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:nil];
    
    ((iD_StressAppDelegate *)[[UIApplication sharedApplication] delegate]).videoPlaying = NO;
    PostTratamientos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostTratamientos alloc] initWithNibName:@"PostTratamientos-iPad" bundle:nil];
    } else {
        post =[[PostTratamientos alloc] initWithNibName:@"PostTratamientos" bundle:nil];
    }
    [self.navigationController pushViewController:post animated:YES];
    [post release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Breathing",nil);
    
	SET_INFO_BUTTON;
	
	int numberOfFrames = 30;
	NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames*2+2];
	
	
	animacion.image = [UIImage imageNamed:@"a5-1.png"];
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.Enlace.iDStress", 0);
    dispatch_async(backgroundQueue, ^{
        for (int i=1; i<=numberOfFrames; i++){
            [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a5-%i.png", i]]];
        }
        [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a5-%i.png", numberOfFrames]]];
        [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a5-%i.png", numberOfFrames]]];
        for (int i=0; i<numberOfFrames; i++){
            [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a5-%i.png", numberOfFrames-i]]];
        }
        animacion.animationImages = imagesArray;
        animacion.animationDuration = 6;
        dispatch_async(dispatch_get_main_queue(), ^{
            [animacion startAnimating];
        });
    });
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress6]) {
        [self.empezar setTitle:NSLocalizedString(@"Purchase", nil) forState:UIControlStateNormal];
    }
    else [self.empezar setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
    
    [Flurry logEvent:@"Tratamiento visto" withParameters:[NSDictionary dictionaryWithObject:@"Respiracion" forKey:@"Tratamiento"]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
   // [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
   // [[UIApplication sharedApplication] setStatusBarHidden:YES];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}
- (BOOL)shouldAutorotate
{
    return YES;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[animacion release];
	[guia release];
	[empezar release];
	[super dealloc];
}


@end
