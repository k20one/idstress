//
//  iD_StressAppDelegate.h
//  iD-Stress
//
//  Created by Leopold Riola on 10/08/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "General.h"
@class RootViewController;

@interface iD_StressAppDelegate : NSObject <UIApplicationDelegate> {
	//UIWindow			*window;
	RootViewController	*viewController;
	General				*general;
	NSNotificationCenter *notifyCenter;
	UIWindow *window;
}

@property (nonatomic, retain) NSNotificationCenter *notifyCenter;
@property (nonatomic, retain) General *general;
@property (nonatomic, retain) UIWindow *window;
@property (nonatomic) BOOL *videoPlaying;

- (void) showUIViewController:(UIViewController *) controller;

- (void) hideUIViewController:(UIViewController *) controller;

@end

