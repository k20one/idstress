//
//  PostTratamientos.m
//  iDStress
//
//  Created by Pold on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PostTratamientos.h"
#import "MyWorld.h"
#import "Appirater.h"
#import "Flurry.h"
#import "UserInfo.h"
#import <Twitter/Twitter.h>
#import "SessionM.h"

@implementation PostTratamientos
@synthesize valoracion, guia;


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	//[self.navigationItem setHidesBackButton:YES];
	self.title = NSLocalizedString(@"Evaluation",nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Home",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showHome)];
    [Appirater userDidSignificantEvent:YES];
    SMAction(@"Tratam");
}

-(void)showHome{   
    [self.navigationController popToRootViewControllerAnimated:YES];
}
// Facebook - http://www.facebook.com/developers
// If SHKFacebookUseSessionProxy is enabled then SHKFacebookSecret is ignored and should be left blank

- (void) postOnFacebook
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"134061643340779", @"app_id",
                                   @"iDStress", @"name",
                                   NSLocalizedString(@"I just relaxed through iD-Stress",nil), @"message",
                                   nil];
    
    [[UserInfo sharedInstance].facebook dialog:@"feed" andParams:params andDelegate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FBLogin" object:nil];
}

- (void)compartirFacebook:(id)sender{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postOnFacebook) name:@"FBLogin" object:nil];
    [[UserInfo sharedInstance] startFbLogin];
    [Flurry logEvent:@"Compartir" withParameters:@{@"Ventana": @"Post Tratamientos", @"Plataforma":@"Facebook"}];
    SMAction(@"Facebook");
}

- (void)compartirTwitter:(id)sender{
    TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
    
    // Set the initial tweet text. See the framework for additional properties that can be set.
    [tweetViewController setInitialText:NSLocalizedString(@"I just relaxed through iD-Stress",nil)];
    
    // Create the completion handler block.
    [tweetViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
        [self dismissModalViewControllerAnimated:YES];
        [tweetViewController release];
    }];
    
    // Present the tweet composition view controller modally.
    [self presentModalViewController:tweetViewController animated:YES];
    [Flurry logEvent:@"Compartir" withParameters:@{@"Ventana": @"Post Tratamientos", @"Plataforma":@"Twitter"}];
    SMAction(@"Twitter");
}

- (IBAction) fin: (id)sender{
    [Flurry logEvent:@"Valorado" withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:valoracion.value] forKey:@"valoración"]];
    
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSInteger myInt = [prefs integerForKey:@"nivelFinal"];
	myInt = myInt + ((int)valoracion.value);
	[prefs setInteger:myInt forKey:@"nivelFinal"];
	[((UIButton *)sender) setEnabled:NO];
    
    MyWorld *evol;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        evol = [[MyWorld alloc] initWithNibName:@"MyWorld-iPad" bundle:nil];
    } else {
        evol = [[MyWorld alloc] initWithNibName:@"MyWorld" bundle:nil];
    }
    [self.navigationController pushViewController:evol animated:YES];
	[evol release];
}
 

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[valoracion release];
	[guia release];
    [super dealloc];
}


@end
