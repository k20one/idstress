//
//  InicioMenu.m
//  iDStress
//
//  Created by Leopold Riola on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InicioMenu.h"
#import "Tratamientos.h"
#import "Elementos.h"
#import "Test.h"
#import "PrePanicButton.h"
#import "Intro.h"
#import "Flurry.h"
#import "Alerta.h"

@implementation InicioMenu
@synthesize boton1, boton2, boton3, boton4, intro ;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        intro = [[Intro alloc] initWithNibName:@"Intro-iPad" bundle:nil];
    else intro = [[Intro alloc] initWithNibName:@"Intro" bundle:nil];
    [self presentModalViewController:intro animated:NO];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Quote",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showIntro)] autorelease];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Alerts",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showAlerts)] autorelease];
    
	UIImage* myImage = [UIImage imageNamed:@"logo-barra.png"];
	UIImageView* myImageView = [[[UIImageView alloc] initWithImage:myImage] autorelease];
	self.title= NSLocalizedString(@"Home",nil);
	self.navigationController.navigationItem.titleView =myImageView;
    
    SET_NAVIGATION_BAR;
    [Flurry logAllPageViews:self.navigationController];
}
-(void) viewWillAppear:(BOOL)animated{
    [Flurry logEvent:@"Tab bar" withParameters:[NSDictionary dictionaryWithObject:@"Home" forKey:@"Tab"]];
	[self.navigationController setNavigationBarHidden:NO];
}

-(void)showIntro{
    [Flurry logEvent:@"Frase del dia"];
    [self presentModalViewController:intro animated:YES];
}

-(void)showAlerts{
    [Flurry logEvent:@"Alerta"];
    Alerta *alerta = [[Alerta alloc] initWithNibName:@"Alerta" bundle:nil];
    [self.navigationController pushViewController:alerta animated:YES];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (IBAction) menu1: (id) sender{
    [Flurry logEvent:@"Menu" withParameters:[NSDictionary dictionaryWithObject:@"Test" forKey:@"Apartado"]];
    Test *info;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        info = [[Test alloc] initWithNibName:@"Test-iPad" bundle:nil];
    else
        info = [[Test alloc] initWithNibName:@"Test" bundle:nil];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}
- (IBAction) menu3: (id) sender{
    [Flurry logEvent:@"Menu" withParameters:[NSDictionary dictionaryWithObject:@"Tratamientos" forKey:@"Apartado"]];
    Tratamientos *info;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        info = [[Tratamientos alloc] initWithNibName:@"Tratamientos-iPad" bundle:nil];
    else
        info = [[Tratamientos alloc] initWithNibName:@"Tratamientos" bundle:nil];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}
- (IBAction) menu2: (id) sender{
    [Flurry logEvent:@"Menu" withParameters:[NSDictionary dictionaryWithObject:@"Elementos" forKey:@"Apartado"]];
    Elementos *info;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        info = [[Elementos alloc] initWithNibName:@"Elementos-iPad" bundle:nil];
    else
        info = [[Elementos alloc] initWithNibName:@"Elementos" bundle:nil];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}
- (IBAction) menu4: (id) sender{
    [Flurry logEvent:@"Menu" withParameters:[NSDictionary dictionaryWithObject:@"Panic Button" forKey:@"Apartado"]];
    PrePanicButton *info;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        info = [[PrePanicButton alloc] initWithNibName:@"PrePanicButton-iPad" bundle:nil];
    else
        info = [[PrePanicButton alloc] initWithNibName:@"PrePanicButton" bundle:nil];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[boton1 release];
	[boton2 release];
	[boton3 release];
	[boton4 release];
    [super dealloc];
}


@end

