//
//  Intro.h
//  iDStress
//
//  Created by Pold on 15/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Alerta;
#define IS_IPHONE_5 ([[UIScreen mainScreen ] bounds].size.height >= 568.0f)
@interface Intro : UIViewController {
	IBOutlet UILabel *frase;
    IBOutlet UILabel *fraseDelDiaLabel;
    IBOutlet UIImageView *backgroundImage;
    Alerta *alerta;
}

@property (nonatomic, retain) IBOutlet UILabel *frase;
@property (nonatomic, retain) IBOutlet UIButton *fondo;
@end
