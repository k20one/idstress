//
//  pruebagame.m
//  iDStress
//
//  Created by SOFIA SWIDAROWICZ on 01/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "drawSpace.h"
#import "cpMouse.h"
#import "SimpleAudioEngine.h"
#import "pruebagame.h"
#import "Flurry.h"
#import "Score.h"
#import "SessionM.h"

@implementation pruebagame
@synthesize backMenu;
NSMutableArray* arregloBloques;


//#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

+(id) scene
{
	CCScene *scene = [CCScene node];
	pruebagame *layer = [pruebagame node];
	layer.isTouchEnabled=YES;
	[scene addChild: layer];
	return scene;
}
//crea el espacio que utiliza chipmunk coloco los comentarios en ingles como indica el tutorial
- (void)createSpace 
{
    space = cpSpaceNew(); //create a new object for the Chipmunk virtual space, and stores it.
    space->gravity = ccp(0, -750);
    cpSpaceResizeStaticHash(space, 400, 1000);
	//cpSpaceResizeStaticHash(space, 400, 200); //create the Chipmunk static and active hashes
    //size of a grid cell (second parameter), and the number of grid cells (third parameter)
	//cpSpaceResizeActiveHash(space, 200, 200); // This is just an optimization Chipmunk uses to speed up collision detection.
    cpSpaceResizeActiveHash(space, 200, 1000);
	
	//It basically divides up the Chipmunk space into a grid. Then if two objects are in different grid cells, 
	//Chipmunk knows instantly they don’t collide, and doesn’t have to do any further math.
	
}
- (void)createGround 
{    
    // 1
	//We’re going to make the ground as a line from the lower left of the screen 
	//to the lower right of the screen, so this gets the coordinates of these for future reference.
    CCDirector *director = [CCDirector sharedDirector];
	if( ! [director enableRetinaDisplay:YES] ){
		
		CGSize winSize = [CCDirector sharedDirector].winSize; 
		CGPoint lowerLeft = ccp(0, 0);
		CGPoint lowerRight = ccp(winSize.width, 0);
		CGPoint topLeft = ccp(0,winSize.height);
		CGPoint topRight = ccp(winSize.width,winSize.height); 
		// 2
		//Creates a new Chipmunk body – a static body, because it never moves-. 
		//Usually you have to add the body to the scene, but you don’t have to 
		//for static bodies. In fact, if you do add it, it will cause dynamic bodies
		//to fall through the ground! I’m not actually sure why this is, so if anyone knows please chime in. 
		cpBody *groundBody = cpBodyNewStatic(); 
		cpBody *roofBody = cpBodyNewStatic();
		
		// 3 Creates a new segment shape and associates it to the body just created.
		float radius = 6.0;
		float radiusRoof = 0;
		cpShape *groundShape = cpSegmentShapeNew(groundBody, lowerLeft, lowerRight, radius);
		cpShape *roofShape = cpSegmentShapeNew(roofBody, topLeft, topRight, radiusRoof);
		
		
		// 4 Sets the elasticity to be somewhat bouncy, and the friction to be not very slippery.
		groundShape->e = 0.5; // elasticity
		groundShape->u = 1.0; // friction 
		roofShape->e = 0.5;
		roofShape->u = 0.5;
		
		// 5 Adds the shape to the Chipmunk space.
		cpSpaceAddShape(space, groundShape);  
		cpSpaceAddShape(space, roofShape);
	}else {
		CGSize winSizeHD = [CCDirector sharedDirector].winSize; 
		CGPoint lowerLeftHD = ccp(0, 0);
		CGPoint lowerRightHD = ccp(winSizeHD.width, 0);
		CGPoint topLeftHD = ccp(0,winSizeHD.height);
		CGPoint topRightHD = ccp(960,640); 
		
		// 2
		//Creates a new Chipmunk body – a static body, because it never moves-. 
		//Usually you have to add the body to the scene, but you don’t have to 
		//for static bodies. In fact, if you do add it, it will cause dynamic bodies
		//to fall through the ground! I’m not actually sure why this is, so if anyone knows please chime in. 
		cpBody *groundBody = cpBodyNewStatic(); 
		cpBody *roofBody = cpBodyNewStatic();
		
		// 3 Creates a new segment shape and associates it to the body just created.
		float radius = 6.0;
		float radiusRoof = 0;
		cpShape *groundShape = cpSegmentShapeNew(groundBody, lowerLeftHD, lowerRightHD, radius);
		cpShape *roofShape = cpSegmentShapeNew(roofBody, topLeftHD, topRightHD, radiusRoof);
		
		
		// 4 Sets the elasticity to be somewhat bouncy, and the friction to be not very slippery.
		groundShape->e = 0.5; // elasticity
		groundShape->u = 1.0; // friction 
		roofShape->e = 0.5;
		roofShape->u = 0.5;
		
		// 5 Adds the shape to the Chipmunk space.
		cpSpaceAddShape(space, groundShape);  
		cpSpaceAddShape(space, roofShape);
		
	}
}

- (void) makeStaticBox{
	
	CGSize s = [CCDirector sharedDirector].winSize; 
	
	int x = 4;
	int y = 4;
	int dmargin = x*2;
	int width = s.width - dmargin;
	int height = s.height - dmargin;
	cpShape * shape;
	cpBody *groundBody = cpBodyNewStatic(); 
	shape = cpSegmentShapeNew(groundBody, cpv(x,y), cpv(x+width, y), 0.0f);
	shape->e = 0.3; // elasticity 
	shape->u = 0.2; //friction
	cpSpaceAddStaticShape(space, shape);
	
	shape = cpSegmentShapeNew(groundBody, cpv(x+width, y), cpv(x+width, y+height ), 0.0f);
	shape->e = 0.1; shape->u = 0.1;
	cpSpaceAddStaticShape(space, shape);
	
	shape = cpSegmentShapeNew(groundBody, cpv(x+width, y+height), cpv(x, y+height ), 0.0f);
	shape->e = 0.1; shape->u = 0.1; 
	cpSpaceAddStaticShape(space, shape);
	
	shape = cpSegmentShapeNew(groundBody, cpv(x, y+height ), cpv(x, y), 0.0f);
	shape->e = 0.1; shape->u = 0.1;
	cpSpaceAddStaticShape(space, shape);
}

- (void)createBoxAtLocation:(CGPoint)location {
	
    //float boxSize = 60.0; //cuadrado
	float boxSize = 30.0;
    //float mass = 0.4;
	float mass = 0.8;
	
	//cuadrado
    cpBody *body = cpBodyNew(mass,  cpMomentForBox(mass, boxSize, boxSize));
	
    body->p = location;
    cpSpaceAddBody(space, body);
	//cuadrado
    cpShape *shape = cpBoxShapeNew(body, boxSize, boxSize);
	
    //cuadrado
	shape->e = 1.0; 
    shape->u = 1.0;
	cpSpaceAddShape(space, shape);
	
}

- (void)  makeCircle:(CGPoint)location {
	int num = 60;
	int  radius = 10;
	float mass = 0.8; 
	cpVect verts[] = {
		cpv(-radius, -radius),
		cpv(-radius, radius),
		cpv( radius, radius),
		cpv( radius, -radius),
	};
	// all physics stuff needs a body
	cpBody *body = cpBodyNew(mass, cpMomentForPoly(mass, num, verts, cpvzero));
	
	cpSpaceAddBody(space, body);
	// and a shape to represent its collision box
	cpShape* shape = cpCircleShapeNew(body, radius, cpvzero);
	shape->e = 0.1; 
	shape->u = 0.5;
	cpSpaceAddShape(space, shape);
	
}


/*- (void)draw {
 
 drawSpaceOptions options = {
 0, // drawHash
 0, // drawBBs,
 1, // drawShapes
 0.0, // collisionPointSize
 0.0, // bodyPointSize,
 2.0 // lineThickness
 };
 
 drawSpace(space, &options);
 
 }*/
-(void) boton
{
	CCMenuItem *backMenuItem = [CCMenuItemImage 
								itemFromNormalImage:@"btn-back.png" selectedImage:@"btn-back.png" 
								target:self selector:@selector(backButtonTapped:)];
	backMenuItem.position = ccp(50, 50);
	backMenu = [CCMenu menuWithItems:backMenuItem, nil];
	backMenu.position = CGPointZero;		
	backMenu.visible = YES;
	
}

-(void) crearLabel
{
	CGSize winSize = [CCDirector sharedDirector].winSize;
	CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"Hello, Chipmunk!" fntFile:@"Arial.fnt"];
	label.position = ccp(winSize.width/2, winSize.height/2);
}

- (void)sleep {
    [self stopAllActions];
    //[self runAction:[CCAnimate actionWithAnimation:sleepAnimation restoreOriginalFrame:NO]];
}
- (void)wake {    
    //[self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"cat_awake.png"]];    
}
- (void)restartTapped:(id)sender {
    [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:[pruebagame scene]]];
    
}

// inicializar la instancia
-(id) init
{	
    [Flurry logEvent:@"Empezar juego"];
    int width = [CCDirector sharedDirector].winSize.width;
    int height = [CCDirector sharedDirector].winSize.height;
	if( (self=[super init] )) {
		
		[self createSpace];
		//[self createGround];
	    [self makeStaticBox];
		
		mouse = cpMouseNew(space);
		self.isTouchEnabled = YES;
		
		//[[SimpleAudioEngine sharedEngine] preloadEffect:@"poof.wav"];
		
		//Botón de pausa
		pauseScreenUp=FALSE;
		
		//Arreglo con los bloques de colores, permitirá eliminar los bloques amarillos en un tiempo determinado
		arregloBloques = [[NSMutableArray alloc] init];
		
		
		CCDirector *director = [CCDirector sharedDirector];
		CCMenuItem *backMenuItem;
		CCMenuItem *pauseMenuItem;
		CCMenuItem *restartMenuItem;
        CCLayerGradient *back = [CCLayerGradient layerWithColor:ccc4(56, 140, 124, 255) fadingTo:ccc4(181, 243, 184, 255) ];
        
        [self addChild: back z: -10];
		if( ! [director enableRetinaDisplay:YES] )
		{	
			//No soporta retina
			retina = FALSE;
		    
			
			
			[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites_regular.plist"];
			batchNode = [CCSpriteBatchNode batchNodeWithFile:@"sprites_regular.png"];
			[self addChild:batchNode];
			//int width = [CCDirector sharedDirector].winSize.width;
            int height = [CCDirector sharedDirector].winSize.height;
			//Boton de salir
			backMenuItem = [CCMenuItemImage 
										itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png" 
										target:self selector:@selector(backButtonTapped:)];
			backMenuItem.position = ccp(30, height-20);
			
			
			
			pauseMenuItem = [CCMenuItemImage
										 itemFromNormalImage:@"pausa_regular.png" selectedImage:@"pausa_regular.png"
										 target:self selector:@selector(PauseButtonTapped:)];
			pauseMenuItem.position = ccp(70, height-20);
			
			
			//Botón de reiniciar partida
			restartMenuItem = [CCMenuItemImage 
										   itemFromNormalImage:@"reiniciar_regular.png" selectedImage:@"reiniciar_regular.png" 
										   target:self selector:@selector(restartTapped:)];
			restartMenuItem.position = ccp(110, height-20);
			
		}
		else
		{
			//Soporta retina
			retina = TRUE;
			//Instancia los sprites de High-Res
			
			
			[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"sprites_hd.plist"];
			batchNode = [CCSpriteBatchNode batchNodeWithFile:@"sprites_hd.png"];
			[self addChild:batchNode];
			
			//Boton de salir
			backMenuItem = [CCMenuItemImage 
										itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png" 
										target:self selector:@selector(backButtonTapped:)];
			backMenuItem.position = ccp(30, 300);
			
			pauseMenuItem = [CCMenuItemImage
										 itemFromNormalImage:@"pausa_hd.png" selectedImage:@"pausa_hd.png"
										 target:self selector:@selector(PauseButtonTapped:)];
			pauseMenuItem.position = ccp(70, 300);
			
			
			//Botón de reiniciar partida
			restartMenuItem = [CCMenuItemImage 
										   itemFromNormalImage:@"reiniciar_hd.png" selectedImage:@"reiniciar_hd.png" 
										   target:self selector:@selector(restartTapped:)];
			restartMenuItem.position = ccp(110, 300);
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                backMenuItem.position = ccp(30, 730);
                pauseMenuItem.position = ccp(70, 730);
                restartMenuItem.position = ccp(110, 730);
            }
		}
		
		ingameMenu = [CCMenu menuWithItems: backMenuItem, pauseMenuItem,restartMenuItem,nil];
		ingameMenu.position = CGPointZero;
		[self addChild:ingameMenu];
		
		
		//conteo de cajas aleatorias
		[self scheduleUpdate];
		[self schedule:@selector(timerUpdate:) interval:0.7];
		
		//Tiempo regresivo.
		score = 0;
		time = 60;
		[self schedule:@selector(countDown:) interval:1];
		
		
		//Crea y añade el label score como hijo
		labelPoint = [CCLabelTTF labelWithString:NSLocalizedString(@"Score:",nil) fontName:@"Helvetica" fontSize:20];
		labelPoint.position = ccp(width-90, height - 50); 
		[self addChild:labelPoint z:1];
		
		scoreLabel = [CCLabelTTF labelWithString:@"0" fontName:@"Helvetica" fontSize:24];
		scoreLabel.position = ccp(width-40, height-50); 
		[self addChild:scoreLabel z:1];
		
		//crea el label de tiempo o temporizador
		
		labelTime = [CCLabelTTF labelWithString:NSLocalizedString(@"Time:",nil) fontName:@"Helvetica" fontSize:20];
		labelTime.position = ccp(width-90,height-20);
		[self addChild:labelTime z:1];
		
		//DBLCDTempBlack font
		temporizador = [CCLabelTTF labelWithString:NSLocalizedString(@"Go!",nil) fontName:@"Helvetica" fontSize:24];
		temporizador.position = ccp(width-40,height-20);
		[self addChild:temporizador z:1];
		
		self.isAccelerometerEnabled = YES;
		[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
		
		
	}
	return self;
}
#define kFilterFactor 0.05
- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration{
	static float prevX=0, prevY=0;
	//float accelX = acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
	//float accelY = acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
	float accelX = acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
	float accelY = acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
	prevX = accelX;
	prevY = accelY;
	cpVect v = cpv(-(accelY), accelX);
	while (v.x < 0.1 && v.y < 0.1) 
	{
		v.x += 0.01;
		v.y += 0.01;
	}
	space->gravity = cpvmult(v, 2000);
}
- (void)update:(ccTime)dt 
{
    cpSpaceStep(space, dt);
	for (CPSprite *sprite in batchNode.children) {
		[sprite update];
	}
}


//Pausa la escena 
-(void)PauseButtonTapped:(id)sender
{
	if(pauseScreenUp == FALSE)
	{
		pauseScreenUp = TRUE;
		
		
		//[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
		[[CCDirector sharedDirector] pause];
		
		//Ajusta la capa que se sobrepondrá sobre la capa del juego al tamaño de la ventana
		CGSize s = [[CCDirector sharedDirector] winSize];
		pauseLayer = [CCLayerColor layerWithColor: ccc4(150, 150, 150, 125) width: s.width height: s.height];
		pauseLayer.position = CGPointZero;
		[self addChild: pauseLayer z:8];
		
		if (retina||(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
		{
            int width = [CCDirector sharedDirector].winSize.width;
            int height = [CCDirector sharedDirector].winSize.height;
			//Muestra la imagen blanca que sirve de fondo a los botones del menú
			/*
			pauseScreen =[CCSprite spriteWithFile:@"pausafondoHD.png"];
			pauseScreen.position= ccp(250,150);
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) pauseScreen.position = ccp(width/2,height/2);
			[self addChild:pauseScreen z:8];
			*/
			
			//Continua el Juego
			CCMenuItem *ResumeMenuItem = [CCMenuItemImage
										  itemFromNormalImage:@"continuarHD.png" selectedImage:@"continuarHD.png"
										  target:self selector:@selector(ResumeButtonTapped:)];
			ResumeMenuItem.position = ccp(250, 140);
			
			//Sale del juego
			CCMenuItem *QuitMenuItem = [CCMenuItemImage
										itemFromNormalImage:@"salirHD.png" selectedImage:@"salirHD.png"
										target:self selector:@selector(backButtonTapped:)];
			QuitMenuItem.position = ccp(250, 80);			
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                ResumeMenuItem.position = ccp(width/2,height/2+10);
                QuitMenuItem.position = ccp(width/2,height/2-110);
               
            }
			pauseScreenMenu = [CCMenu menuWithItems:ResumeMenuItem,QuitMenuItem, nil];
			pauseScreenMenu.position = ccp(0,0);
			[self addChild:pauseScreenMenu z:10];
			
		}else {
			//Muestra la imagen blanca que sirve de fondo a los botones del menú
			/*
			pauseScreen =[CCSprite spriteWithFile:@"pausafondoRegular.png"];
			pauseScreen.position= ccp(250,150);
			[self addChild:pauseScreen z:8];
			*/
			//Continua el Juego
			CCMenuItem *ResumeMenuItem = [CCMenuItemImage
										  itemFromNormalImage:@"continuarRegular.png" selectedImage:@"continuarRegular.png"
										  target:self selector:@selector(ResumeButtonTapped:)];
			ResumeMenuItem.position = ccp(250, 160);
			
			
			//Sale del juego
			CCMenuItem *QuitMenuItem = [CCMenuItemImage
										itemFromNormalImage:@"salirRegular.png" selectedImage:@"salirRegular.png"
										target:self selector:@selector(backButtonTapped:)];
			QuitMenuItem.position = ccp(250, 100);
			
			pauseScreenMenu = [CCMenu menuWithItems:ResumeMenuItem,QuitMenuItem, nil];
			pauseScreenMenu.position = ccp(0,0);
			[self addChild:pauseScreenMenu z:10];
		}
		
	}
}
//Fin del Juego
-(void)endGame
{
	
	if (gameOver) return;
	gameOver = TRUE;
    
	if(pauseScreenUp == FALSE)
	{
		pauseScreenUp = TRUE;
		
		
		//[[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
		[[CCDirector sharedDirector] pause];
		
		//Ajusta la capa que se sobrepondrá sobre la capa del juego al tamaño de la ventana
		CGSize s = [[CCDirector sharedDirector] winSize];
		pauseLayer = [CCLayerColor layerWithColor: ccc4(150, 150, 150, 125) width: s.width height: s.height];
		pauseLayer.position = CGPointZero;
		[self addChild: pauseLayer z:8];
		
		BOOL gana = FALSE;
		
		
		if (score >= 50)
			gana = TRUE;
		
		[self stopAllActions];
		[self unschedule:@selector(timerUpdate:)];		

		int width = [CCDirector sharedDirector].winSize.width;
		int height = [CCDirector sharedDirector].winSize.height;
		
		NSString* text;
		if (gana)
			text = NSLocalizedString(@"You win!",nil);
		else
			text = NSLocalizedString(@"Try again!",nil);
		
		CCLabelTTF* t1 = [CCLabelTTF labelWithString:text fontName:@"Helvetica" fontSize:25];
		t1.anchorPoint = ccp(0.5,0.5);
		t1.position = ccp( width / 2.0f, height * 0.75f );
		if (gana)
			[t1 runAction:  [CCRepeatForever actionWithAction:  [CCSequence actions:[CCScaleTo actionWithDuration:0.25 scale:1.25],[CCScaleTo actionWithDuration:0.25 scale:1],nil ] ]];
		[self addChild: t1 z:10];
		
        if (retina||(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
		{
			//Botón de reiniciar partida
			CCMenuItem *reiniciaMenuItem = [CCMenuItemImage 
											itemFromNormalImage:@"comenzarHD.png" selectedImage:@"comenzarHD.png" 
											target:self selector:@selector(RestartButtonTapped:)];
			reiniciaMenuItem.position = ccp(250, 140);
			
			
			//Sale del juego
			CCMenuItem *QuitMenuItem = [CCMenuItemImage
										itemFromNormalImage:@"salirHD.png" selectedImage:@"salirHD.png"
										target:self selector:@selector(backButtonTapped:)];
			QuitMenuItem.position = ccp(250, 80);
			
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                reiniciaMenuItem.position = ccp(width/2,height/2+10);
                QuitMenuItem.position = ccp(width/2,height/2-110);
            }

			pauseScreenMenu = [CCMenu menuWithItems:reiniciaMenuItem,QuitMenuItem, nil];
			pauseScreenMenu.position = ccp(0,0);
			[self addChild:pauseScreenMenu z:10];			
			
		}else {
			/*
			if (gana == TRUE)
			{
				//Muestra la imagen blanca que sirve de fondo a los botones del menú
				pauseScreen =[[CCSprite spriteWithFile:@"HasGanadoRegular.png"] retain];
			}else {
				pauseScreen =[[CCSprite spriteWithFile:@"HasPerdidoRegular.png"] retain];
			}
			pauseScreen.position= ccp(250,150);
            [self addChild:pauseScreen z:8];
			*/
			//Botón de reiniciar partida
			CCMenuItem *reiniciaMenuItem = [CCMenuItemImage 
											itemFromNormalImage:@"comenzarRegular.png" selectedImage:@"comenzarRegular.png" 
											target:self selector:@selector(RestartButtonTapped:)];
			reiniciaMenuItem.position = ccp(250, 180);
			
			
			//Sale del juego
			CCMenuItem *QuitMenuItem = [CCMenuItemImage
										itemFromNormalImage:@"salirRegular.png" selectedImage:@"salirRegular.png"
										target:self selector:@selector(backButtonTapped:)];
			QuitMenuItem.position = ccp(250, 100);
            
			pauseScreenMenu = [CCMenu menuWithItems:reiniciaMenuItem,QuitMenuItem, nil];
			pauseScreenMenu.position = ccp(0,0);
			[self addChild:pauseScreenMenu z:10];
		}
	}	   
}

//Regresar a la escena anterior o salir del juego
- (void)backButtonTapped:(id)sender {
	// Modo landscape
	CCDirector *director = [CCDirector sharedDirector];
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	//[[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
	glColor4ub(255,255,255,255);
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
    SMAction(@"Game1");
	
}
//Reinicia la escena luego de que el juego ha terminado
-(void)RestartButtonTapped:(id)sender{
    [Flurry logEvent:@"Reempezar juego" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:@"Juego 1", @"Juego", nil ]];
	// [self removeChild:pauseScreen cleanup:YES];
	[self removeChild:pauseScreenMenu cleanup:YES];
	[self removeChild:pauseLayer cleanup:YES];
	[[CCDirector sharedDirector] resume];
	//[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"tramp.wav"];
	pauseScreenUp=FALSE;
	[[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:[pruebagame scene]]];
}
//Reinicia la escena luego de ser pausada
-(void)ResumeButtonTapped:(id)sender{
	// [self removeChild:pauseScreen cleanup:YES];
	[self removeChild:pauseScreenMenu cleanup:YES];
	[self removeChild:pauseLayer cleanup:YES];
	//[[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"tramp.wav"];
	[[CCDirector sharedDirector] resume];
	pauseScreenUp=FALSE;
}


-(void) countDown:(ccTime)delta
{
	if (!pauseScreenUp){
		if (time != 0)
		{
			time -= 1;
			[temporizador setString:[NSString stringWithFormat:@"%i", time]];
		}else {
			[self endGame];
		}
	}
	
}
- (void)addPoints
{
    score = score + 1;
    [scoreLabel setString:[NSString stringWithFormat:@"%i", score]];
}
- (void)subPoints
{
    score = score - 1;
    [scoreLabel setString:[NSString stringWithFormat:@"%i", score]];
}
- (void)addFivePoints
{
    score = score + 2;
    [scoreLabel setString:[NSString stringWithFormat:@"%i", score]];
}
- (void)subAllPoints
{
    score = score -2;
    [scoreLabel setString:[NSString stringWithFormat:@"%i", score]];
}

-(void) timerUpdate:(ccTime)delta
{
	if (!pauseScreenUp)
	{
		int width = [CCDirector sharedDirector].winSize.width;
        //posición random de las cajas en pantalla coordenadas Y
		int max = 310;
		int min = 240;
		double scale = (double) (max - min) / RAND_MAX;
		int val = min + floor(rand() * scale);
		
		//random coordenadas X
		int max2 = width-50;
		int min2 = 50;
		double scale2 = (double) (max2 - min2) / RAND_MAX;
		int val2 = min2 + floor(rand() * scale2);
		
		
		
		//Si el tiempo es par arroja un bloque rojo sino uno verde	
		if (time%2 == 0)
		{
			Bloque_verde *block2a = [[[Bloque_verde alloc] initWithSpace:space location:ccp(val2, val)] autorelease];
            block2a.tag = 60-(int)time;
			[batchNode addChild:block2a];
			[arregloBloques addObject:block2a];

			if (score >= 20)
			{  
				Bloque_rojo *block1b = [[[Bloque_rojo alloc] initWithSpace:space location:ccp(val2, val)] autorelease];
				[batchNode addChild:block1b];
				
			}
			
		} else
		{
			if (score >= 36)
			{  Bloque_azul *block3c = [[[Bloque_azul alloc] initWithSpace:space location:ccp(val2, val)] autorelease];
				[batchNode addChild:block3c];
			}
			
			Bloque_amarillo *block4d = [[[Bloque_amarillo alloc] initWithSpace:space location:ccp(val2, val)] autorelease];
			
			[batchNode addChild:block4d];
						NSLog(@"BLOQUE AMARILLO");
		}
		
		
		
		if ([arregloBloques count] > 9) {
			for (int i=0; i < [arregloBloques count]; i++) {
				if (((Bloque_verde *)[arregloBloques objectAtIndex:i]).tag + 10 < 60-(int)time) {
					if ([[arregloBloques objectAtIndex:i] destroy]==1){
						NSLog(@"PLOOF");
						break;
					};
					//[arregloBloques removeObjectAtIndex:i];
					NSLog(@"%i",((Bloque_amarillo *)[arregloBloques objectAtIndex:i]).tag);
				}
				
			}
			
		}
		//show debug
		//NSLog(@"here is my integer: %i", val);
		//NSLog(@"This is my float: %f \n\nAnd here is my integer: %i \n\nAnd finally my string: %@", randomNumber);
	}
}



- (void)registerWithTouchDispatcher {
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    //cpMouseGrab(mouse, touchLocation, false);
	cpShape *shape = cpSpacePointQueryFirst(space, touchLocation, GRABABLE_MASK_BIT, 0);
	
	if (shape) 
	{
		CPSprite *sprite = (CPSprite *) shape->data;
		
		
		NSString *className = NSStringFromClass([sprite class]);
		//NSLog(@"%@", className);
		
		NSString *br = @"Bloque_rojo";
		NSString *bv = @"Bloque_verde";
		NSString *bam = @"Bloque_amarillo";
		NSString *baz = @"Bloque_azul";
		//NSLog(@"%@", br);
		
		
		if([className isEqualToString:br] )
		{
		    //NSLog(@"BLOQUE ROJO FUNCIONA!!!!!!!!!!!");
			[[Score alloc] initPuntuation: +2 location: sprite.position layer: self];
			if ([sprite destroy]==1)
			    [self addFivePoints];
			// [[SimpleAudioEngine sharedEngine] playEffect:@"poof.wav"];
		}
		if ([className isEqualToString:bv]) {
			//NSLog(@"BLOQUE VERDE FUNCIONA!!!!!!!!!!!");
			[[Score alloc] initPuntuation: -1 location: sprite.position layer: self];
			if ([sprite destroy]==1)
			{
				[self subPoints];
			}
			// [[SimpleAudioEngine sharedEngine] playEffect:@"poof.wav"];
		}
		if ([className isEqualToString:bam]) {
			//NSLog(@"BLOQUE AMARILLO FUNCIONA!!!!!!!!!!!");
			[[Score alloc] initPuntuation: +1 location: sprite.position layer: self];
			if ([sprite destroy]==1)
			{ 
				[self addPoints];
				/*
				for (int i=0; i < [arregloBloques count]; i++) {
					if (((Bloque_amarillo *)[arregloBloques objectAtIndex:i]).tag == sprite.tag) {
						//[arregloBloques removeObjectAtIndex:i];
						break;
						NSLog(@"boom sprite");
					}
				}
				 */
			}
			
			
		}
		if ([className isEqualToString:baz]) {
			//NSLog(@"BLOQUE AZUL FUNCIONA!!!!!!!!!!!");
			[[Score alloc] initPuntuation: -2 location: sprite.position layer: self];
			if ([sprite destroy]==1)
				[self subAllPoints];
		    // [[SimpleAudioEngine sharedEngine] playEffect:@"poof.wav"];
		}
		
	}
    return YES;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
    cpMouseMove(mouse, touchLocation);
}

- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
	cpMouseRelease(mouse);
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event {
    cpMouseRelease(mouse);    
}


//  Pausa la aplicación
-(void) applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

// cuando se activa la pantalla
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
	pauseScreenUp = TRUE;
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	[[CCDirector sharedDirector] startAnimation];
}

// aplicación será killed
- (void)applicationWillTerminate:(UIApplication *)application
{	
	//CC_DIRECTOR_END();
	CCDirector *director = [CCDirector sharedDirector];
	// Sets landscape mode
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	
	[[director openGLView] removeFromSuperview];
	
	[director end];	
}

// purgar memoria 
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
	//[[TextureMgr sharedTextureMgr] removeAllTextures];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}
- (void) dealloc
{
	cpMouseFree(mouse); 
	cpSpaceFree(space);	
	//[[CCDirector sharedDirector] end]; //agregue 19 de mayo
	[super dealloc];
}

@end



