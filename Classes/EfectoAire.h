//
//  EfectoAire.h
//  iDStress
//
//  Created by Sophy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#include <vector>
#define IS_IPHONE_5 ([[UIScreen mainScreen ] bounds].size.height == 568.0f)
// HelloWorld Layer
struct diente 
{
	float		weight;
	CCSprite*	sprite;
	bool		flying;
	float		inertia;
	float		time;
	float		angle;
	float		speedX, speedY;
	
	diente()
	{
		weight = 0;
		sprite = nil;
		flying = false;
		inertia = 0;
		angle = 0;
		speedX = 10;
		speedY = 10;
		time = 0;
	}
	
};

@interface EfectoAire : CCLayer
{
    CCLabelTTF          *labelSopla;
	CCMenu				*backMenu;
	AVAudioRecorder		*recorder;
	NSTimer				*levelTimer;
	double				lowPassResults;
	AVAudioPlayer        *theAudio;
	
	
	
	CCLayerGradient*		back;
	CCSprite*				tallo;

	std::vector<diente>	dientes;
}

@property (readwrite,retain) CCParticleSystem *emitter;
@property (readwrite,retain) CCMenu *backMenu;

- (void)levelTimerCallback:(NSTimer *)timer;

// returns a Scene that contains the HelloWorld as the only child
+(id) scene;

@end
