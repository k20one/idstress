//
//  Info.m
//  iDStress
//
//  Created by Leopold Riola on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Info.h"
#import "Flurry.h"

@implementation Info
@synthesize infoTexto1, infoTexto2, guia, texto1, texto2;

- (void)viewWillAppear:(BOOL)animated {
    NSArray *views = self.navigationController.viewControllers;
    NSString *classsName = NSStringFromClass([((UIViewController*)[views objectAtIndex:(views.count-2)]) class]);
    if (classsName) {
        NSLog(@"%@", classsName);
        [Flurry logEvent:@"Info" withParameters:@{@"Origin": classsName}];
    }
    
    
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
	
	[infoTexto1 setText:texto1];
	[infoTexto2 setText:texto2];
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
	
	[infoTexto1 setText:texto1];
	[infoTexto2 setText:texto2];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[infoTexto1 release];
	[infoTexto2 release];
	[guia release];
	[texto1 release];
	[texto2 release];
    [super dealloc];
}


@end
