//
//  PrePanicButton.m
//  iD-Stress
//
//  Created by Pold on 18/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PrePanicButton.h"
#import "Player.h"
#import "Info.h"
#import "Flurry.h"
@implementation PrePanicButton
@synthesize empezar, guia, animacion;

-(IBAction) info: (id)sender {
    Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
	info.texto1 =  [[[NSMutableString alloc] initWithString: NSLocalizedString(@"This is a concentrated relaxation exercise. Restore your inner balance in just a few minutes.",nil)] autorelease];
	info.texto2 = [[[NSMutableString alloc] initWithString: NSLocalizedString(@"\n It prepares you when you are anticipating an extremely tense situation or helps recover positive emotions after a conflict. \n\n Due to its short duration, find a quiet place where you can listen to the exercise uninterrupted.",nil)] autorelease];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}

- (IBAction) empezar: (id)sender{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setObject:@"PanicButton" forKey:@"Tratamiento"];
	
    Player* player;
    player = [[Player alloc] initWithNibName:@"Player" bundle:nil];
    [self.navigationController pushViewController:player animated:NO];
    [player release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Panic Button",nil);
	
    SET_INFO_BUTTON;

	int numberOfFrames = 76;
	NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
	
	animacion.image = [UIImage imageNamed:@"a1-1.png"];
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.Enlace.iDStress", 0);
    dispatch_async(backgroundQueue, ^{
        for (int i=1; i<=numberOfFrames-10; i++){
            [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a1-%i.png", i]]];
        }
        for (int i=1; i<=10; i++){
            [imagesArray addObject:[UIImage imageNamed:@"a1-66.png"]];
        }
        animacion.animationImages = imagesArray;
        animacion.animationDuration = 8;
        dispatch_async(dispatch_get_main_queue(), ^{
            [animacion startAnimating];
        });
    });
}

- (void)viewWillAppear:(BOOL)animated {
    [Flurry logEvent:@"Tratamiento visto" withParameters:[NSDictionary dictionaryWithObject:@"Panic Button" forKey:@"Tratamiento"]];
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[animacion release];
	[guia release];
	[empezar release];
	[super dealloc];
}



@end
