//
//  pruebagame.h
//  iDStress
//
//  Created by SOFIA SWIDAROWICZ on 01/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "SpaceManager.h"
#import "cpCCSprite.h"
#import "cpShapeNode.h"
#import "cpMouse.h"
#import "drawSpace.h" 
#import "Bloque_rojo.h"
#import "Bloque_verde.h"
#import "Bloque_azul.h"
#import "Bloque_amarillo.h"

#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

#define GRABABLE_MASK_BIT (1<<31)
#define NOT_GRABABLE_MASK (~GRABABLE_MASK_BIT)
#pragma mark InicioJuego Class


@interface pruebagame : CCLayer
{
	
	CCMenu				*ingameMenu;
	CCSprite			*background;

	cpSpace				*space;
	cpMouse             *mouse;
	CCSpriteBatchNode   *batchNode;

	int score;
	int time;
    CCLabelTTF          *scoreLabel;
	CCLabelTTF          *temporizador;
	CCLabelTTF          *labelTime;
	CCLabelTTF          *labelPoint;
	CCLabelTTF          *labelInfo;
	BOOL gameOver;
	BOOL retina;
	int  caja_roja;
	
	//pausa objetos
	CCSprite            *movingSpring;
	bool                 pauseScreenUp;
	CCLayer             *pauseLayer;
	CCMenu              *pauseScreenMenu;
	
}

@property (readwrite,retain) CCMenu *backMenu;
//- (void)levelTimerCallback:(NSTimer *)timer;

- (void)sleep;
- (void)wake;

+(id) scene;

@end