//
//  iD_StressAppDelegate.m
//  iD-Stress
//
//  Created by Leopold Riola on 10/08/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "cocos2d.h"

#import "SessionM.h"
#import "iD_StressAppDelegate.h"
#import "GameScene.h"
#import "cocos2d.h"
#import "CCTransition.h"
#import "EfectoAgua.h"
#import "EfectoFuego.h"
#import "EfectoTierra.h"
#import "EfectoAire.h"
#import "pruebagame.h"
#import "Game.h"
#import "Intro.h"
#import "Appirater.h"
#import "Flurry.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "UserInfo.h"

@implementation UINavigationBar (CustomImage)
- (void)drawRect:(CGRect)rect
{
	UIImage *image = [UIImage imageNamed: @"barra.png"];
	[image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	self.tintColor =[UIColor colorWithRed:45.0/255.0 green:144.0/255.0 blue:24.0/255.0 alpha:1.0];
}
@end

@implementation iD_StressAppDelegate

@synthesize notifyCenter, general, window;

- (void)dealloc
{
	[general release];
	[super dealloc];
}
- (void)applicationDidFinishLaunching:(UIApplication *)application
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"Purchased"]) {
        SMStart(@"0b22adce484417380406b204353384daa7421f7a");
    }
    
    [Flurry startSession:@"54J4HDP97TDHVW48N3N2"];
	self.notifyCenter = [NSNotificationCenter defaultCenter];
	[notifyCenter addObserver:self selector:@selector(trackNotifications:) name:nil object:nil];
    // Set AudioSession
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    
    /* Pick any one of them */
    // 1. Overriding the output audio route
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
    
    // 2. Changing the default output audio route
    //UInt32 doChangeDefaultRoute = 1;
    //AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    [window setUserInteractionEnabled:YES];
    [window setMultipleTouchEnabled:YES];
	
	CCDirector *director = [CCDirector sharedDirector];
    // Create an EAGLView with a RGB8 color buffer, and a depth buffer of 24-bits
	EAGLView *glView = [EAGLView viewWithFrame:[window frame]
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0						// GL_DEPTH_COMPONENT16_OES
						];
	
	[glView setMultipleTouchEnabled:YES]; 
	
	// attach the openglView to the director
	[director setOpenGLView:glView];
	
	// Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
	if( ! [director enableRetinaDisplay:YES] )
		CCLOG(@"Retina Display Not supported");
	
	// make the OpenGLView a child of the main window
	[window addSubview:glView];
	
	// make main window visible
	[window makeKeyAndVisible];	
    GameScene *gs = [GameScene node];
	
    [[CCDirector sharedDirector] runWithScene:gs];
	General *principal;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        principal = [[General alloc] initWithNibName:@"General-iPad" bundle:nil];
    } else {
        principal = [[General alloc] initWithNibName:@"General" bundle:nil];
    }

	self.general = principal;
	[principal release];
	[self showUIViewController:general];
    [Appirater appLaunched:YES];
    
    [self setUpNotifications];
}

- (void)setUpNotifications {
    UIApplication* app = [UIApplication sharedApplication];
    NSArray*    oldNotifications = [app scheduledLocalNotifications];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:@"Initialized"]&&![userDefaults boolForKey:@"AlreadyLaunched"]) {
        [userDefaults setBool:YES forKey:@"Initialized"];
        [userDefaults setBool:YES forKey:@"FraseAlarma"];
    }
    UILocalNotification* notifyAlarm = [[[UILocalNotification alloc]
                                         init] autorelease];
    if (notifyAlarm && [oldNotifications count] == 0 && [userDefaults boolForKey:@"FraseAlarma"])
    {
        NSDate *alertTime = [[NSDate date]
                             dateByAddingTimeInterval:24 * 60 * 60 - 600];
        
        NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:NSLocalizedString(@"FrasesDelDia",nil) ofType:@"txt"] encoding:NSUTF8StringEncoding error:nil];
        NSMutableArray *frases = [[NSMutableArray alloc]initWithArray:[fileText componentsSeparatedByString:@"\n"]];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:alertTime];
        NSInteger dia = [components day];
        NSInteger mes = [components month];
        int index = ((dia + ((mes%3)*30))%frases.count);
        
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.alertAction = @"iDStress";
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        notifyAlarm.alertBody = [frases objectAtIndex:index];
        [app scheduleLocalNotification:notifyAlarm];
        
    }
    else if ([oldNotifications count] >= 2) [app cancelAllLocalNotifications];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	[[CCDirector sharedDirector] startAnimation];
      [Appirater appEnteredForeground:YES];
    [self setUpNotifications];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
}

-(void) applicationWillTerminate: (UIApplication*) application
{
    [[CCDirector sharedDirector] end];
}

- (void) trackNotifications: (NSNotification *) notification
{
	id nname = [notification name];
	
	if([nname isEqual:@"inicio"])
	{
		[self showUIViewController:general];
		[[CCDirector sharedDirector] popScene];
	}
	else if([nname isEqual:@"empezarEfectoAgua"])
	{
        
		[self hideUIViewController:general];
		[[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0.0f scene:[EfectoAgua scene]]];
	}
	else if([nname isEqual:@"empezarEfectoFuego"])
	{
		[self hideUIViewController:general];
		[[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0.0f scene:[EfectoFuego scene]]];
	}
	else if([nname isEqual:@"empezarEfectoAire"])
	{
		[self hideUIViewController:general];
		[[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0.0f scene:[EfectoAire scene]]];
		
	}
	
	else if([nname isEqual:@"empezarEfectoTierra"])
	{
		[self hideUIViewController:general];
		
		// Obtain the shared director in order to...
		CCDirector *director = [CCDirector sharedDirector];
		
		// Sets landscape mode
		//[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
		
		// Turn on display FPS
		[director setDisplayFPS:NO];
		
		// Turn on multiple touches
		EAGLView *view = [director openGLView];
		[view setMultipleTouchEnabled:YES];
		
		// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
		// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
		// You can change anytime.
		[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];	
		cpInitChipmunk();
		[[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0 scene:[EfectoTierra scene]]];
		
	}
	else if([nname isEqual:@"empezarJuego"])
	{
        
		[self hideUIViewController:general];
/**/
		// Obtain the shared director in order to...
		CCDirector *director = [CCDirector sharedDirector];
		
		// Sets landscape mode
		[director setDeviceOrientation:kCCDeviceOrientationLandscapeLeft];
		//CCDeviceOrientationPortrait
		// Turn on display FPS
		[director setDisplayFPS:NO];
		
		// Turn on multiple touches
		EAGLView *view = [director openGLView];
		[view setMultipleTouchEnabled:YES];
		
		// Default texture format for PNG/BMP/TIFF/JPEG/GIF images
		// It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
		// You can change anytime.
		[CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];	
		cpInitChipmunk();
		[[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0.0f scene:[pruebagame scene]]];
	}
    else if ([nname isEqual:@"empezarJuego2"]){
        
        
        [self hideUIViewController:general];
         CCDirector *director = [CCDirector sharedDirector];
         EAGLView *view = [director openGLView];
         [view setMultipleTouchEnabled:YES];
         [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];	
         [[CCDirector sharedDirector] pushScene: [CCTransitionMoveInB transitionWithDuration:0.0f scene:[Game scene]]];	
    }
	
}
- (NSUInteger) application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if (self.videoPlaying){
        NSLog(@"Video");
        return UIInterfaceOrientationMaskPortrait;
    }
    else return    UIInterfaceOrientationMaskPortrait;
}

-(void)animDone:(NSString*) animationID finished:(BOOL) finished context:(void*) context
{	
	[[CCDirector sharedDirector] resume];
}

- (void) showUIViewController:(UIViewController *) controller
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
	[[CCDirector sharedDirector] pause];
    if (([[UIScreen mainScreen ] bounds].size.height == 568.0f))
        window.rootViewController = controller;
    else
        [[[CCDirector sharedDirector] openGLView] addSubview:controller.view];

}

- (void) hideUIViewController:(UIViewController *) controller
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:.5];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animDone:finished:context:)];
	
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:[[CCDirector sharedDirector] openGLView] cache:YES];
    if (([[UIScreen mainScreen ] bounds].size.height == 568.0f))
        window.rootViewController = nil;
    else
        [controller.view removeFromSuperview];
	
	[UIView commitAnimations];
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    if ([UserInfo sharedInstance].facebook) {
        return [[UserInfo sharedInstance].facebook handleOpenURL:url];
    }
    else return true;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    if ([UserInfo sharedInstance].facebook) {
        return [[UserInfo sharedInstance].facebook handleOpenURL:url];
    }
    else return true;
}

@end
