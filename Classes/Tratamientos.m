//
//  Tratamientos.m
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Tratamientos.h"
#import "PreGestoRelax.h"
#import "PreConcentracion.h"
#import "PreEmbarazadas.h"
#import "PreRespiracion.h"
#import "PrePanicButton.h"
#import "PreInsomnia.h"
#import "PreRelajacionBase.h"
#import "CustomCell.h"
#import "Flurry.h"
#import "Enlace.h"
#import "StoreManager.h"

@implementation Tratamientos
#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    [[StoreManager sharedStoreManager] requestProductData];
    
	self.title = NSLocalizedString(@"Treatments",nil);
    
    SET_INFO_BUTTON;
	
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Restore",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(restorePressed)] autorelease];
    
	UIView *backgroundView = [[UIView alloc] initWithFrame: self.tableView.frame];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-iPad.png"]];
    } else {
        backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    }
	self.tableView.backgroundView = backgroundView;
	self.tableView.separatorColor = [UIColor whiteColor];
	[backgroundView release];
    
    SET_NAVIGATION_BAR;
}

- (void) restorePressed {
    [[StoreManager sharedStoreManager] restoreAllPurchases];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchased) name:@"TransactionSuccessful" object:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TransactionSuccessful" object:nil];
}

- (void) purchased {
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

-(IBAction) info: (id)sender {
	Enlace *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Enlace alloc] initWithNibName:@"Enlace-iPad" bundle:nil];
    } else {
        info = [[Enlace alloc] initWithNibName:@"Enlace" bundle:nil];
    }
    [self.navigationController pushViewController:info animated:YES];
	[info release];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
	
}


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (([[UIScreen mainScreen ] bounds].size.height >= 568.0f)) {
        return 60;
    }
    else return 51;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CustomCell";
    CustomCell *cell = (CustomCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CustomCell" owner:self options:nil];
		
		for (id currentObject in topLevelObjects){
			if ([currentObject isKindOfClass:[UITableViewCell class]]){
				cell =  (CustomCell *) currentObject;
				break;
			}
		}
	}
	cell.selectionStyle =  UITableViewCellSelectionStyleGray;
    UIColor *unavailableColor = [UIColor colorWithRed:222/255.0 green:231/255.0 blue:228/255.0 alpha:0.3];
    //[UIColor colorWithRed:192/255.0 green:201/255.0 blue:198/255.0 alpha:1.0];

	// Configure the cell...
	switch (indexPath.row) {
		case 0:
			cell.nombre.text = NSLocalizedString(@"Basic Relax",nil);
			cell.descripcion.text = NSLocalizedString(@"Rest your body and mind",nil);
			cell.duracion.text =NSLocalizedString( @"16'44''",@"Basic Relax");
			cell.icon.image = [UIImage imageNamed:@"pbbl-base.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress1])
                cell.backgroundColor = unavailableColor;
			break;
		case 1:
			cell.nombre.text = NSLocalizedString(@"Concentration",nil);
			cell.descripcion.text = NSLocalizedString(@"Increase your attention span",nil);
			cell.duracion.text = NSLocalizedString(@"18'33''",@"Concentracion");
			cell.icon.image = [UIImage imageNamed:@"pbbl-concetra.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress2])
                cell.backgroundColor = unavailableColor;
			break;
		case 2:
			cell.nombre.text = NSLocalizedString(@"Pregnancy",nil);
			cell.descripcion.text = NSLocalizedString(@"Enjoy a peaceful pregnancy",nil);
			cell.duracion.text = NSLocalizedString(@"16'22''",@"Embarazadas");
			cell.icon.image = [UIImage imageNamed:@"pbbl-parto.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress4])
                cell.backgroundColor = unavailableColor;
			break;
		case 3:
			cell.nombre.text = NSLocalizedString(@"Insomnia",nil);
			cell.descripcion.text = NSLocalizedString(@"Allow your body to rest",nil);
			cell.duracion.text = NSLocalizedString(@"12'58''",@"Insomnia");
			cell.icon.image = [UIImage imageNamed:@"pbbl-insomnia.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress3])
                cell.backgroundColor = unavailableColor;
			break;
		case 4:
			cell.nombre.text = NSLocalizedString(@"Breathing",nil);
			cell.descripcion.text = NSLocalizedString(@"Oxygenate your body and mind",nil);
			cell.duracion.text = NSLocalizedString(@"11'10''",@"Respiracion");
			cell.icon.image = [UIImage imageNamed:@"pbbl-respira.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress6])
                cell.backgroundColor = unavailableColor;
			break;
		case 5:
			cell.nombre.text = NSLocalizedString(@"Touch Relax",nil);
			cell.descripcion.text = NSLocalizedString(@"Disconnect from stress through touch",nil);
			cell.duracion.text = NSLocalizedString(@"13'34''",@"Gesto Relax");
			cell.icon.image = [UIImage imageNamed:@"pbbl-relax.png"];
            if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress5])
                cell.backgroundColor = unavailableColor;
			break;
		case 6:
			cell.nombre.text = NSLocalizedString(@"Panic Button",nil);
			cell.descripcion.text = NSLocalizedString(@"Restore your peace in 8 minutes",nil);
			cell.duracion.text = NSLocalizedString(@"8'00''",@"Panic button");
			cell.icon.image = [UIImage imageNamed:@"pbbl-panic.png"];
			break;
		default:
			break;
	}
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */

	[self.tableView cellForRowAtIndexPath:indexPath].selected = NO;
	NSInteger row = [indexPath row];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        switch (row) {
            case 0:{
                PreRelajacionBase *preRelajacionBase =[[PreRelajacionBase alloc] initWithNibName:@"PreRelajacionBase-iPad" bundle:nil];
                [preRelajacionBase viewWillAppear:YES];
                [self.navigationController pushViewController:preRelajacionBase animated:YES];
                [preRelajacionBase release];
                break;
            }
            case 1:{
                PreConcentracion *preConcentracion =[[PreConcentracion alloc] initWithNibName:@"PreConcentracion-iPad" bundle:nil];
                [preConcentracion viewWillAppear:YES];
                [self.navigationController pushViewController:preConcentracion animated:YES];
                [preConcentracion release];
                break;
            }
            case 2:{
                PreEmbarazadas *preEmbarazadas =[[PreEmbarazadas alloc] initWithNibName:@"PreEmbarazadas-iPad" bundle:nil];
                [preEmbarazadas viewWillAppear:YES];
                [self.navigationController pushViewController:preEmbarazadas animated:YES];
                [preEmbarazadas release];
                break;
            }
            case 3:{
                PreInsomnia *preInsomnia =[[PreInsomnia alloc] initWithNibName:@"PreInsomnia-iPad" bundle:nil];
                [preInsomnia viewWillAppear:YES];
                [self.navigationController pushViewController:preInsomnia animated:YES];
                [preInsomnia release];
                break;
            }
            case 4:{
                PreRespiracion *preRespiracion =[[PreRespiracion alloc] initWithNibName:@"PreRespiracion-iPad" bundle:nil];
                [preRespiracion viewWillAppear:YES];
                [self.navigationController pushViewController:preRespiracion animated:YES];
                [preRespiracion release];
                break;
            }
            case 5:{
                PreGestoRelax *preGestoRelax =[[PreGestoRelax alloc] initWithNibName:@"PreGestoRelax-iPad" bundle:nil];
                [preGestoRelax viewWillAppear:YES];
                [self.navigationController pushViewController:preGestoRelax animated:YES];
                [preGestoRelax release];
                break;
            }
            case 6:{
                PrePanicButton *prePanicButton = [[PrePanicButton alloc] initWithNibName:@"PrePanicButton-iPad" bundle:nil];
                [prePanicButton viewWillAppear:YES];
                [self.navigationController pushViewController:prePanicButton animated:YES];
                [prePanicButton release];
                break;
            }
            default:
                break;
        }
    } 
    else {
        switch (row) {
            case 0:{
                PreRelajacionBase *preRelajacionBase =[[PreRelajacionBase alloc] initWithNibName:@"PreRelajacionBase" bundle:nil];
                [preRelajacionBase viewWillAppear:YES];
                [self.navigationController pushViewController:preRelajacionBase animated:YES];
                [preRelajacionBase release];
                break;
            }
            case 1:{
                PreConcentracion *preConcentracion =[[PreConcentracion alloc] initWithNibName:@"PreConcentracion" bundle:nil];
                [preConcentracion viewWillAppear:YES];
                [self.navigationController pushViewController:preConcentracion animated:YES];
                [preConcentracion release];
                break;
            }
            case 2:{
                PreEmbarazadas *preEmbarazadas =[[PreEmbarazadas alloc] initWithNibName:@"PreEmbarazadas" bundle:nil];
                [preEmbarazadas viewWillAppear:YES];
                [self.navigationController pushViewController:preEmbarazadas animated:YES];
                [preEmbarazadas release];
                break;
            }
            case 3:{
                PreInsomnia *preInsomnia =[[PreInsomnia alloc] initWithNibName:@"PreInsomnia" bundle:nil];
                [preInsomnia viewWillAppear:YES];
                [self.navigationController pushViewController:preInsomnia animated:YES];
                [preInsomnia release];
                break;
            }
            case 4:{
                PreRespiracion *preRespiracion =[[PreRespiracion alloc] initWithNibName:@"PreRespiracion" bundle:nil];
                [preRespiracion viewWillAppear:YES];
                [self.navigationController pushViewController:preRespiracion animated:YES];
                [preRespiracion release];
                break;
            }
            case 5:{
                PreGestoRelax *preGestoRelax =[[PreGestoRelax alloc] initWithNibName:@"PreGestoRelax" bundle:nil];
                [preGestoRelax viewWillAppear:YES];
                [self.navigationController pushViewController:preGestoRelax animated:YES];
                [preGestoRelax release];
                break;
            }
            case 6:{
                PrePanicButton *prePanicButton = [[PrePanicButton alloc] initWithNibName:@"PrePanicButton" bundle:nil];
                [prePanicButton viewWillAppear:YES];
                [self.navigationController pushViewController:prePanicButton animated:YES];
                [prePanicButton release];
                break;
            }
            default:
                break;
        }
    }

}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc {
	[super dealloc];
}


@end

