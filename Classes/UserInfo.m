//
//  UserInfo.m
//  Starbates
//
//  Created by Leopold Riola on 29/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserInfo.h"
@implementation UserInfo 

@synthesize facebook;

static UserInfo* sharedInstance;

+(UserInfo*)sharedInstance{
    if (!sharedInstance)
        sharedInstance = [[UserInfo alloc] init];
    return sharedInstance;
}

NSString *kAppId;
#define SHKFacebookKey				@"36e7347dc5545f9f8849a83e14312a98"
#define SHKFacebookSecret			@"c1cff3d1d171ef05aeedd649e6f96837"

- (void) startFbLogin
{
    kAppId = @"134061643340779";
    facebook = [[Facebook alloc] initWithAppId:kAppId andDelegate:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FBLogin" object:nil];
    }
    if (![facebook isSessionValid]) {
        [facebook authorize:nil];
    }
}
#pragma mark - FBSessionDelegate

- (void)fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FBLogin" object:facebook];
}

- (void)fbDidNotLogin:(BOOL)cancelled{}

- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt{}

- (void)fbDidLogout{}

- (void)fbSessionInvalidated{}


@end
