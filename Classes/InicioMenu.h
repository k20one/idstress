//
//  InicioMenu.h
//  iDStress
//
//  Created by Leopold Riola on 28/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Intro.h"

@interface InicioMenu : UIViewController {
	IBOutlet UIButton *boton1;
	IBOutlet UIButton *boton2;
	IBOutlet UIButton *boton3;
	IBOutlet UIButton *boton4;
}

@property (nonatomic, retain) IBOutlet UIButton *boton1;
@property (nonatomic, retain) IBOutlet UIButton *boton2;
@property (nonatomic, retain) IBOutlet UIButton *boton3;
@property (nonatomic, retain) IBOutlet UIButton *boton4;
@property (nonatomic, retain) Intro *intro;
- (IBAction) menu1: (id) sender;
- (IBAction) menu2: (id) sender;
- (IBAction) menu3: (id) sender;
- (IBAction) menu4: (id) sender;

@end
