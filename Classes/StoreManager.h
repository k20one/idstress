//
//  SSStoreManager.h
//  StopSmoking
//
//  Created by Thomas Hanks on 2/5/13.
//  Copyright (c) 2013 Demand Media, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SynthesizeSingleton.h"
#define iDStress1 @"com.Enlace.BasicRelax"
#define iDStress2 @"com.Enlace.Concentration"
#define iDStress3 @"com.Enlace.Insomnia"
#define iDStress4 @"com.Enlace.Pregnancy"
#define iDStress5 @"com.Enlace.TouchRelax"
#define iDStress6 @"com.Enlace.Breathe"
@interface StoreManager : NSObject
SYNTHESIZE_SINGLETON_METHOD_FOR_CLASS(StoreManager);
- (void) requestProductData;
- (void) purchaseItemWithIdentifier:(NSString*)identifier;
- (void) restoreAllPurchases;
@end
