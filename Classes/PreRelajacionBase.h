//
//  PreRelajacionBase.h
//  iDStress
//
//  Created by Leopold Riola on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PreRelajacionBase : UIViewController {

	IBOutlet UIButton *empezar;
	IBOutlet UIImageView *guia;
	IBOutlet UIImageView *animacion;
}

@property (nonatomic, retain) IBOutlet UIImageView *animacion;
@property (nonatomic, retain) IBOutlet UIImageView *guia;
@property (nonatomic, retain) UIButton *empezar;
- (IBAction) empezar: (id)sender;

@end
