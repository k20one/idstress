//
//  Enlace.h
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface Enlace : UIViewController {
	IBOutlet UIScrollView* scrollView;
	IBOutlet UIImageView *mapa;
}
@property (nonatomic, retain) UIScrollView* scrollView;
@property (nonatomic, retain) UIImageView *mapa;
@end
