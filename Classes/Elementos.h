//
//  Elementos.h
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostElementos.h"

@interface Elementos : UIViewController {
	IBOutlet UIButton *botonAgua;
	IBOutlet UIButton *botonFuego;
	IBOutlet UIButton *botonTierra;
	IBOutlet UIButton *botonAire;
}
@property (nonatomic, retain) UIButton *botonAgua;
@property (nonatomic, retain) UIButton *botonFuego;
@property (nonatomic, retain) UIButton *botonTierra;
@property (nonatomic, retain) UIButton *botonAire;

- (IBAction) agua: (id)sender;
- (IBAction) fuego: (id)sender;
- (IBAction) tierra: (id)sender;
- (IBAction) aire: (id)sender;


@end
