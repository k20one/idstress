//
//  ResultadoTest.h
//  iDStress
//
//  Created by Pold on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tratamientos.h"
@interface ResultadoTest : UIViewController {
	IBOutlet UITextView *evalTexto1;
	IBOutlet UILabel *evalTexto2;
	IBOutlet UITextView *evalTexto3;
	IBOutlet UITextView *evalTexto4;
	int puntos;
	BOOL insomnio;
	IBOutlet UIButton *tratamiento1;
	IBOutlet UIButton *tratamiento2;
	IBOutlet UIImageView *guia;
	IBOutlet UIScrollView* scrollView;
}
@property (nonatomic, retain) UIScrollView* scrollView;
@property (nonatomic, retain) IBOutlet UIImageView *guia;	
@property (nonatomic, retain) IBOutlet UITextView *evalTexto1;
@property (nonatomic, retain) IBOutlet UILabel *evalTexto2;
@property (nonatomic, retain) IBOutlet UITextView *evalTexto3;
@property (nonatomic, retain) IBOutlet UITextView *evalTexto4;
@property(nonatomic, assign) int puntos;
@property(nonatomic, assign) BOOL insomnio;
@property (nonatomic, retain) IBOutlet UIButton *tratamiento1;
@property (nonatomic, retain) IBOutlet UIButton *tratamiento2;

- (IBAction) tratamientoBoton: (id)sender;
@end
