//
//  PreJuego.h
//  iDStress
//
//  Created by Pold on 24/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PreJuego : UIViewController <UIAlertViewDelegate>{
	IBOutlet UIButton *botonJuego;
	IBOutlet UIImageView *guia;
}
@property (nonatomic, retain) IBOutlet UIImageView *guia;

@property (nonatomic, retain) UIButton *botonJuego;
- (IBAction) empezarprueba: (id)sender;

@end
