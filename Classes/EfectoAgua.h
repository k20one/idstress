//
//  EfectoAgua.h
//  iDStress
//
//  Created by Sophy on 04/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "pgeRippleSprite.h"

// HelloWorld Layer
@interface EfectoAgua : CCLayer // <CCTargetedTouchDelegate>
{
	CCMenu				*backMenu;
	pgeRippleSprite		*rippleImage;
	BOOL                retina;
    int                 time;
}

// returns a Scene that contains the HelloWorld as the only child
+(id) scene;
@end
