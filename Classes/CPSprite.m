//
//  CPSprite.m
//  iDStress
//
//  Created by SOFIA SWIDAROWICZ on 01/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CPSprite.h"
#import "pruebagame.h"
#import "SimpleAudioEngine.h"


@implementation CPSprite
@synthesize body;

- (void)update 
{    
    self.position = body->p;
    self.rotation = CC_RADIANS_TO_DEGREES(-1 * body->a);
}

- (void)createBodyAtLocation:(CGPoint)location 
{
    float mass = 1.0;
    // body = cpBodyNew(mass, cpMomentForBox(mass, self.contentSize.width, self.contentSize.height));
	body = cpBodyNew(mass, cpMomentForCircle(mass, 0, self.contentSize.width / 2.0f, cpvzero));
    body->p = location;
    body->data = self;
    cpSpaceAddBody(space, body);
	
    // shape = cpBoxShapeNew(body, self.contentSize.width, self.contentSize.height);
	shape = cpCircleShapeNew(body, self.contentSize.width/2.0f, cpvzero);
    shape->e = 0.0; 
    shape->u = 1.0;
    shape->data = self;
    cpSpaceAddShape(space, shape);	
}

- (id)initWithSpace:(cpSpace *)theSpace location:(CGPoint)location spriteFrameName:(NSString *)spriteFrameName {
	
    if ((self = [super initWithSpriteFrameName:spriteFrameName])) 
	{
        space = theSpace;
        [self createBodyAtLocation:location];
        canBeDestroyed = YES;
		
    }
    return self;
	
}


- (int)destroy 
{
	
    if (!canBeDestroyed) return 0;
    cpSpaceRemoveBody(space, body);
    cpSpaceRemoveShape(space, shape);
	
	[self removeFromParentAndCleanup:YES];
	canBeDestroyed = NO;
	return 1;
	
}

@end