//
//  HelloWorldLayer.m
//  Work
//
//  Created by Miguel on 30/04/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


// Import the interfaces
#import "Game.h"
#import "SimpleAudioEngine.h"
#import "Flurry.h"
#import "Score.h"
#import "SessionM.h"

#define PAUSE_BTN			@"Icon-Small.png"
#define PAUSE_BTN_PRESSED	@"Icon-Small-50.png"

#define PARTICLE_RAIN_NAME  @"Image/fire.png"

#define GOOD_PROBABILITY	65
#define PLAYER_SPEED		1300

// HelloWorldLayer implementation
@implementation Game

+(id) scene
{
	CCScene *scene = [CCScene node];
	Game *layer = [Game node];
	layer.isTouchEnabled=YES;
	[scene addChild: layer];
	return scene;
	
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init])) 
	{
		
		wins = [[CCDirector sharedDirector] winSize];
		state = START;
		
		back = [CCLayerGradient layerWithColor:ccc4(56, 140, 124, 255) fadingTo:ccc4(181, 243, 184, 255) ];
		
		[self addChild: back z: -10];
		
		dropsLayer = [[CCLayer alloc] init];
		[self addChild:dropsLayer];
		
		gameLayer = [[CCLayer alloc] init];
		[self addChild: gameLayer];
		
		gameTime = 60;
		puntuation = 0;
		
		// HUD Texts
		m_pPuntuationLabel = [CCLabelTTF labelWithString:NSLocalizedString(@"Points: 00",nil) fontName:@"Helvetica" fontSize:20];
		m_pPuntuationLabel.anchorPoint = ccp(1,1);
		m_pPuntuationLabel.position = ccp( wins.width - 10, wins.height - 10);
		[gameLayer addChild:m_pPuntuationLabel];

		m_pTimeTextLabel = [CCLabelTTF labelWithString:NSLocalizedString(@"Time: 60.00",nil) fontName:@"Helvetica" fontSize:20];
		m_pTimeTextLabel.anchorPoint = ccp(1,1);
		m_pTimeTextLabel.position = ccp( wins.width - 10, wins.height - 10 - 20 );
		[gameLayer addChild:m_pTimeTextLabel];
		
		m_bPaused = false;
		[self createInGameMenu];
		[self createPauseMenu];
		m_pPauseLayer.visible = false;
		
		state = GAME;
		
		// [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"Sounds/playMusic.wav"];
		
		
		dropInterval = START_INTERVAL;
		
		NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
		
		int level = [prefs integerForKey:@"gameLevel"];
		if ( level == 3 )
			dropSpeed = START_SPEED_LEVEL_3;
		else if ( level == 2 )
			dropSpeed = START_SPEED_LEVEL_2;
		else 
			dropSpeed = START_SPEED_LEVEL_1;
		
		NSString *imagen;
		
		goodType = [prefs integerForKey:@"elemento"];
		// Set the player
		switch (goodType) {
			case 1:
				imagen = @"Avatar_Agua-e.png";
				break;
			case 2:
				imagen = @"Avatar_Fuego-e.png";
				break;
			case 3:
				imagen = @"Avatar_Tierra-e.png";
				break;
			case 4:
				imagen = @"Avatar_Aire-e.png";
				break;
			default:
				goodType = 1;
				imagen = @"Avatar_Agua-e.png";
				break;
		}
		m_pPlayer = [CCSprite spriteWithFile:imagen];
		m_pPlayer.anchorPoint = ccp(0.5, 0);
		m_pPlayer.position = ccp( wins.width / 2.0f, 0 );
		if (!retina)
		{
			m_pPlayer.scaleX = 0.45f;
			m_pPlayer.scaleY = 0.45f;
		}
		[dropsLayer addChild:m_pPlayer];
		
		int index = 0;
		for (int i = 1; i<5; i++) 
		{
			if ( i != goodType )
			{
				badTypes[index] = i;
				index++;
			}
		}
		
		// Set update callback
		[self scheduleUpdate];
		
		self.isAccelerometerEnabled = YES;
		[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
		[UIAccelerometer sharedAccelerometer].delegate = self;

	}
	return self;
}


-(void) update:(ccTime)deltaTime
{
	switch (state) {
		case START:
		{
		}
			break;
		case GAME:
		{
			if (!m_bPaused)
			{
				gameTime -= deltaTime;
				if (gameTime > 0)
				{
					[self movePlayer: deltaTime];
					[self updateDropsSettings: deltaTime];
					[self updateDropsAndCheckCollisions:deltaTime];
				}else{
					if (gameTime < 0) gameTime = 0;
					// Show end menu
					[self createEndGame];
					state = FINISHED;
				}
				
				[m_pTimeTextLabel setString:[NSString stringWithFormat:NSLocalizedString(@"Time : %.1f",nil), gameTime] ];
			}
			else 
			{
				
			}

			
		}
			break;
		case FINISHED:
		{
		}
			break;
		default:
			break;
	}
}

- (void) movePlayer:(ccTime)deltaTime
{
	int x = m_pPlayer.position.x;
	x = accelVector.x * PLAYER_SPEED * deltaTime + x;
	float tam = m_pPlayer.scaleX * m_pPlayer.contentSize.width / 2.0f;
	x = clampf(x, tam, wins.width - tam);
	
	m_pPlayer.position = ccp(x, 0);
	
}

- (void) updateDropsSettings:(ccTime)deltaTime
{
	// DROPS
	changeIntervalTime += deltaTime;
	if (changeIntervalTime >= CHANGE_INTERVAL)
	{
		changeIntervalTime -= CHANGE_INTERVAL;
		dropInterval -= MINUS_INTERVAL;
		if (dropInterval < MINIMUN_INTERVAL) 
		{
			dropInterval = MINIMUN_INTERVAL;
		}
	}
	
	changeSpeedTime += deltaTime;
	if (changeSpeedTime >= CHANGE_SPEED)
	{
		changeSpeedTime -= CHANGE_SPEED;
		dropSpeed += PLUS_SPEED;
		if( dropSpeed > MAX_SPEED )
		{
			dropSpeed = MAX_SPEED;
		}
	}
	
	currentInterval += deltaTime;
	if (currentInterval >= dropInterval) 
	{
		currentInterval -= dropInterval;
		
		bool good = false;
		int type = 1;
		if (rand()%100 <  GOOD_PROBABILITY)
		{
			good = true;
			type = goodType;
		}
		else
		{
			type = badTypes[ rand()%3 ];
		}
		
		// Create Drop
		int x =  rand()%(int)wins.width;
		Drop* d = new Drop( x , wins.height, dropsLayer, type, good, retina);
		d->speed = dropSpeed;
		drops.push_back( d );
	}
}

- (void) updateDropsAndCheckCollisions:(ccTime)deltaTime
{
	for (int i = drops.size()-1 ; i >= 0; i--) 
	{
		Drop* d = drops[i];
		if (d->state == Drop::DYING) 
		{
			drops.erase( drops.begin() + i );	
			delete d;
		}
		else 
		{
			d->update(deltaTime);
			if ([self isCollisionBetweenSpriteA: m_pPlayer spriteB: d->sprite pixelPerfect: true])
			{
				// NSLog(@"Collision");
				if (d->good)
				{
					puntuation ++;
					[[Score alloc] initPuntuation: 1 location: d->sprite.position layer: dropsLayer];
				}
				else
				{
					puntuation --;
					[[Score alloc] initPuntuation:-1 location: d->sprite.position layer: dropsLayer];
				}
				[m_pPuntuationLabel setString: [NSString stringWithFormat: NSLocalizedString(@"Points: %2d",nil), puntuation]];
				
				d->die();
			}
		}
	}	
}



// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method

	
	
	// don't forget to call "super dealloc"
	[super dealloc];
}


- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{	
	static float prevX=0, prevY=0;
	
#define kFilterFactor 0.05f
	
	float accelX = (float) acceleration.x * kFilterFactor + (1- kFilterFactor)*prevX;
	float accelY = (float) acceleration.y * kFilterFactor + (1- kFilterFactor)*prevY;
	
	prevX = accelX;
	prevY = accelY;
	
	accelVector = ccp( accelX, accelY);
	
}



//Buttons Callbacks
-(void) backButtonTapped: (id) sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
    SMAction(@"Game2");
}

-(void) PauseButtonTapped:(id) sender
{
	m_bPaused = true;
	[m_pInGameMenu setIsTouchEnabled:false];
	m_pPauseLayer.visible = true;
}

-(void) ResumeButtonTapped:(id) sender
{
	m_bPaused = false;
	[m_pInGameMenu setIsTouchEnabled:true];
	m_pPauseLayer.visible = false; 
}

-(void) restartTapped:(id)sender
{
	[Flurry logEvent:@"Reempezar juego"];
	[[CCDirector sharedDirector] resume];
	[[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:[Game scene]]];
}



-(void) createInGameMenu
{
	CCDirector *director = [CCDirector sharedDirector];
	CCMenuItem *backMenuItem;
	CCMenuItem *pauseMenuItem;
	CCMenuItem *restartMenuItem;
	if( ! [director enableRetinaDisplay:YES] )
	{
		//No soporta retina
		retina = FALSE;
		
		//Boton de salir
		backMenuItem = [CCMenuItemImage
						itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png"
						target:self selector:@selector(backButtonTapped:)];
		
		
		
		pauseMenuItem = [CCMenuItemImage
						 itemFromNormalImage:@"pausa_regular.png" selectedImage:@"pausa_regular.png"
						 target:self selector:@selector(PauseButtonTapped:)];
		
		
		//Bot贸n de reiniciar partida
		restartMenuItem = [CCMenuItemImage
						   itemFromNormalImage:@"reiniciar_regular.png" selectedImage:@"reiniciar_regular.png"
						   target:self selector:@selector(restartTapped:)];
		
	}
	else
	{
		//Soporta retina
		retina = TRUE;
		
		//Boton de salir
		backMenuItem = [CCMenuItemImage
						itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png"
						target:self selector:@selector(backButtonTapped:)];
		
		pauseMenuItem = [CCMenuItemImage
						 itemFromNormalImage:@"pausa_hd.png" selectedImage:@"pausa_hd.png"
						 target:self selector:@selector(PauseButtonTapped:)];
		
		
		//Bot贸n de reiniciar partida
		restartMenuItem = [CCMenuItemImage
						   itemFromNormalImage:@"reiniciar_hd.png" selectedImage:@"reiniciar_hd.png"
						   target:self selector:@selector(restartTapped:)];
	}
	
	m_pInGameMenu = [CCMenu menuWithItems: backMenuItem, pauseMenuItem,restartMenuItem,nil];
	[m_pInGameMenu alignItemsHorizontally];
	float xpos = backMenuItem.contentSize.width + pauseMenuItem.contentSize.width / 2.0f + 10;
	m_pInGameMenu.position = ccp( xpos , wins.height - backMenuItem.contentSize.height / 2.0f - 10);
	[self addChild:m_pInGameMenu];
	
}






-(BOOL) isCollisionBetweenSpriteA:(CCSprite*)spr1 spriteB:(CCSprite*)spr2 pixelPerfect:(BOOL)pp
{
	BOOL isCollision = NO;
	
	CGRect intersection = CGRectIntersection([spr1 boundingBox], [spr2 boundingBox]);
	
	// Look for simple bounding box collision
	if (!CGRectIsEmpty(intersection))
	{
		// If we're not checking for pixel perfect collisions, return true
		if (!pp) {return YES;}
		
		CGPoint spr1OldPosition = spr1.position;
		CGPoint spr2OldPosition = spr2.position;
		
		spr1.position = CGPointMake(spr1.position.x - intersection.origin.x, spr1.position.y - intersection.origin.y);
		spr2.position = CGPointMake(spr2.position.x - intersection.origin.x, spr2.position.y - intersection.origin.y);
		
		intersection = CGRectIntersection([spr1 boundingBox], [spr2 boundingBox]);
		
		// Assuming that the spritebatchnode of both sprites is the same, I just use one. If each sprite has a different sprite batch node as parent you should modify the code to get the spriteBatchNode for each sprite and visit them.
		// CCSpriteBatchNode* _sbnMain =(CCSpriteBatchNode*) spr1.parent;
		
		//NOTE: We are assuming that the spritebatchnode is always at 0,0
		
		// Get intersection info
		unsigned int x = (intersection.origin.x)* CC_CONTENT_SCALE_FACTOR();
		unsigned int y = (intersection.origin.y)* CC_CONTENT_SCALE_FACTOR();
		unsigned int w = intersection.size.width* CC_CONTENT_SCALE_FACTOR();
		unsigned int h = intersection.size.height* CC_CONTENT_SCALE_FACTOR();
		unsigned int numPixels = w * h;// * CC_CONTENT_SCALE_FACTOR();
		
		// create render texture and make it visible for testing purposes
		int renderWidth = w+1;
		int renderHeight = h+1;
		
		if(renderWidth<16)
		{
			renderWidth = 16;
		}
		
		if(renderHeight < 16)
		{
			renderHeight = 16;
		}
		
		CCRenderTexture* _rt = [[CCRenderTexture alloc] initWithWidth:renderWidth height:renderHeight pixelFormat:kCCTexture2DPixelFormat_RGBA8888];
		
		//rt is always going to be at 0,0 - can't change it.
		_rt.position = CGPointMake(0, 0);
		// [[RIGameScene sharedGameScene]addChild:_rt];
		_rt.visible = YES;
		
		//NSLog(@"\nintersection = (%u,%u,%u,%u), area = %u",x,y,w,h,numPixels);
		
		// Draw into the RenderTexture
		[_rt beginWithClear:0 g:0 b:0 a:0];
		
		// Render both sprites: first one in RED and second one in GREEN
		glColorMask(1, 0, 0, 1);
		// [_sbnMain visitSprite:spr1];
		[spr1 visit];
		glColorMask(0, 1, 0, 1);
		// [_sbnMain visitSprite:spr2];
		[spr2 visit];
		glColorMask(1, 1, 1, 1);
		
		// Get color values of intersection area
		ccColor4B *buffer = (ccColor4B*)malloc( sizeof(ccColor4B) * numPixels );
		glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		
		[_rt end];
		
		// Read buffer
		unsigned int step = 1;
		for(unsigned int i=0; i<numPixels; i+=step)
		{
			ccColor4B color = buffer[i];
			
			if (color.r > 0 && color.g > 0)
			{
				isCollision = YES;
				break;
			}
		}
		
		// Free buffer memory
		free(buffer);
		
		spr1.position = spr1OldPosition;
		spr2.position = spr2OldPosition;
		
		[_rt release];
		// [[RIGameScene sharedGameScene]removeChild:_rt cleanup:YES];
	}
	return isCollision;
}

-(void) createPauseMenu
{
	//Ajusta la capa que se sobrepondr谩 sobre la capa del juego al tama帽o de la ventana
	m_pPauseLayer = [CCLayerColor layerWithColor: ccc4(150, 150, 150, 125) width: wins.width height: wins.height];
	m_pPauseLayer.position = CGPointZero;
	[self addChild: m_pPauseLayer z:8];
	
	CCMenuItem *ResumeMenuItem;
	CCMenuItem *QuitMenuItem;
	
	if (retina||(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad))
	{
		//Continua el Juego
		ResumeMenuItem = [CCMenuItemImage
                          itemFromNormalImage:@"continuarHD.png" selectedImage:@"continuarHD.png"
                          target:self selector:@selector(ResumeButtonTapped:)];
		
		//Sale del juego
		QuitMenuItem = [CCMenuItemImage
                        itemFromNormalImage:@"salirHD.png" selectedImage:@"salirHD.png"
                        target:self selector:@selector(backButtonTapped:)];
		
		
	}
	else
	{
		//Continua el Juego
		ResumeMenuItem = [CCMenuItemImage
                          itemFromNormalImage:@"continuarRegular.png" selectedImage:@"continuarRegular.png"
                          target:self selector:@selector(ResumeButtonTapped:)];
		
		//Sale del juego
		QuitMenuItem = [CCMenuItemImage
                        itemFromNormalImage:@"salirRegular.png" selectedImage:@"salirRegular.png"
                        target:self selector:@selector(backButtonTapped:)];
		
	}
	
	ResumeMenuItem.anchorPoint = ccp(0.5, 0.5);
	ResumeMenuItem.position = ccp( wins.width / 2.0f, wins.height * 0.45 );
	
	QuitMenuItem.anchorPoint = ccp(0.5, 0.5);
	QuitMenuItem.position = ccp( wins.width / 2.0f, wins.height * 0.55 );
	
	m_pPauseMenu = [CCMenu menuWithItems:ResumeMenuItem,QuitMenuItem, nil];
	m_pPauseMenu.position = ccp(0,0);
	[m_pPauseLayer addChild:m_pPauseMenu z:10];
	
}


-(void) createEndGame
{
	//Ajusta la capa que se sobrepondrá sobre la capa del juego al tamaño de la ventana
	m_pEndLayer = [CCLayerColor layerWithColor: ccc4(150, 150, 150, 125) width: wins.width height: wins.height];
	m_pEndLayer.position = CGPointZero;
	[self addChild: m_pEndLayer z:8];
	
	CCMenuItem *RetryMenuItem;
	CCMenuItem *QuitMenuItem;
	
	if (retina)
	{
		//Continua el Juego
		RetryMenuItem = [CCMenuItemImage
						  itemFromNormalImage:@"comenzarHD.png" selectedImage:@"comenzarHD.png"
						  target:self selector:@selector(restartTapped:)];
		
		//Sale del juego
		QuitMenuItem = [CCMenuItemImage
						itemFromNormalImage:@"salirHD.png" selectedImage:@"salirHD.png"
						target:self selector:@selector(backButtonTapped:)];
		
		
	}
	else 
	{
		//Continua el Juego
		RetryMenuItem = [CCMenuItemImage
						  itemFromNormalImage:@"comenzarRegular.png" selectedImage:@"comenzarRegular.png"
						  target:self selector:@selector(restartTapped:)];
		
		//Sale del juego
		QuitMenuItem = [CCMenuItemImage
						itemFromNormalImage:@"salirRegular.png" selectedImage:@"salirRegular.png"
						target:self selector:@selector(backButtonTapped:)];
		
	}
	
	RetryMenuItem.anchorPoint = ccp(0.5, 0.5);
	RetryMenuItem.position = ccp( wins.width / 2.0f, wins.height * 0.35 );
	
	QuitMenuItem.anchorPoint = ccp(0.5, 0.5);
	QuitMenuItem.position = ccp( wins.width / 2.0f, wins.height * 0.45 );
	
	m_pEndMenu = [CCMenu menuWithItems:RetryMenuItem,QuitMenuItem, nil];
	m_pEndMenu.position = ccp(0,0);
	[m_pEndLayer addChild:m_pEndMenu z:10];
	
	// Puntuation
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString* text;
	bool newRecord = false;
	if ([prefs objectForKey:@"Game2_Record"] == nil)
	{
		text = [NSString stringWithFormat:NSLocalizedString(@"New Record!",nil)];
		[prefs setInteger:puntuation forKey:@"Game2_Record"];
		newRecord = true;
	}else{
		int oldRecord = (int)[prefs integerForKey:@"Game2_Record"]; 
		if (oldRecord < puntuation)
		{
			text = [NSString stringWithFormat:NSLocalizedString(@"New Record!",nil)];
			[prefs setInteger:puntuation forKey:@"Game2_Record"];
			newRecord = true;
		}else{
			text = [NSString stringWithFormat:NSLocalizedString(@"Score",nil)];
		}
	}
	CCLabelTTF* t1 = [CCLabelTTF labelWithString:text fontName:@"Helvetica" fontSize:25];
	t1.anchorPoint = ccp(0.5,0.5);
	t1.position = ccp( wins.width / 2.0f, wins.height * 0.625f );
	if (newRecord)
	{
		[t1 runAction:  [CCRepeatForever actionWithAction:  [CCSequence actions:[CCScaleTo actionWithDuration:0.25 scale:1.25],[CCScaleTo actionWithDuration:0.25 scale:1],nil ] ]];
	}
	
	CCLabelTTF* t2 = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", puntuation] fontName:@"Helvetica" fontSize:25];
	t2.anchorPoint = ccp(0.5,0.5);
	t2.position = ccp( wins.width / 2.0f, wins.height * 0.55f );
	[m_pEndLayer addChild:t1];
	[m_pEndLayer addChild:t2];
}


@end
