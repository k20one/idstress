/*
 *  Drop.h
 *  Work
 *
 *  Created by Miguel on 30/04/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#import "cocos2d.h"

@interface Score : NSObject
{
	CCLabelTTF* text;
	CCLayer* layer;
}
- (id)initPuntuation:(int)puntuation location:(CGPoint)location layer:(CCLayer *)layer;
- (void) end;
@end;