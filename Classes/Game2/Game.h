//
//  Game.h
//  Work
//
//  Created by Miguel on 30/04/12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#define START_INTERVAL	1.5f
#define START_SPEED_LEVEL_1		(wins.height / 3.5f)
#define START_SPEED_LEVEL_2		(wins.height / 2.5f)
#define START_SPEED_LEVEL_3		(wins.height / 1.5f)

#define CHANGE_INTERVAL	15.0f	// 60 s
#define CHANGE_SPEED	10.0f	

#define MINUS_INTERVAL	0.1f
#define	PLUS_SPEED		(wins.height / 30.0f)

#define MINIMUN_INTERVAL	0.1f
#define MAX_SPEED			(wins.height * 2.0f)


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#include <vector>
// Importing Chipmunk headers
#import "chipmunk.h"

#import "Drop.h"

typedef enum 
{
	START,
	GAME,
	FINISHED
}GameState;

// Game
@interface Game : CCLayer
{
	bool m_bPaused;
	CCMenu*	m_pInGameMenu;
	CCLayer* m_pPauseLayer;
	CCMenu*	m_pPauseMenu;
	CCLayer* m_pEndLayer;
	CCMenu* m_pEndMenu;
	
	GameState state;
	
	CCLayer*			dropsLayer;
	std::vector<Drop*>	drops;
	
	float			changeIntervalTime;
	float			dropInterval;
	
	float			changeSpeedTime;
	float			dropSpeed;
	
	float			currentInterval;
	
	CCLayer*		gameLayer;
	CCSprite*		player;
	
	CCLabelTTF*		m_pPuntuationLabel;
	CCLabelTTF*		m_pTimeTextLabel;
	float			gameTime;
	int				puntuation;
	
	CGSize			wins;
	
	CCParticleRain*	rainParticle;
	BOOL			retina;
	
	int				goodType;
	int				badTypes[3];
	CCSprite*		m_pPlayer;
	
	CCLayerGradient*	back;
	
	CGPoint				accelVector;
	
}

+(id) scene;

- (void) movePlayer:(ccTime) deltaTime;
- (void) updateDropsSettings:(ccTime)deltaTime;
- (void) updateDropsAndCheckCollisions:(ccTime)deltaTime;

// Buttons Callbacks
-(void) backButtonTapped: (id) sender;
-(void) PauseButtonTapped:(id) sender;
-(void) restartTapped:(id)sender;
-(void) ResumeButtonTapped: (id)sender;


-(void) createInGameMenu;
-(void) createPauseMenu;
-(void) createEndGame;

-(BOOL) isCollisionBetweenSpriteA:(CCSprite*)spr1 spriteB:(CCSprite*)spr2 pixelPerfect:(BOOL)pp;

@end
