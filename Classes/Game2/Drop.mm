/*
 *  Drop.cpp
 *  Work
 *
 *  Created by Miguel on 30/04/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "Drop.h"

Drop::Drop( int x, int y, CCLayer* layer, int type, bool good , bool retina) :
good(good)
{
	state = GROWING;
	time = 0;
	
	NSString * imagen;
	switch (type) {
		default:
		case 1:
			if (good) imagen = @"Avatar_Agua-e.png";
			else	  imagen = @"Avatar_Agua-o.png";
			break;
		case 2:
			if (good) imagen = @"Avatar_Fuego-e.png";
			else	  imagen = @"Avatar_Fuego-o.png";
			break;
		case 3:
			if (good) imagen = @"Avatar_Tierra-e.png";
			else	  imagen = @"Avatar_Tierra-o.png";
			break;
		case 4:
			if (good) imagen = @"Avatar_Aire-e.png";
			else	  imagen = @"Avatar_Aire-o.png";
			break;
	}
	
	sprite = [CCSprite spriteWithFile:imagen];

	this->layer = layer;
	
	[this->layer addChild: sprite];
	
	CGPoint p;
	p.x = x;
	p.y = y;
	sprite.position = p;
	sprite.anchorPoint = ccp(0.5f, 1);
	
	if (!retina)
		scaleFactor = 0.45;
	else 
		scaleFactor = 1;
	speed = 100;
	
}

Drop::~Drop()
{
	[layer removeChild:sprite cleanup: FALSE];
}

void Drop::update( float t )
{
	
	switch (state) {
		case GROWING:
		{
			time += t;
			if ( time < (3.0f / (speed / 100.0f)) ) 
			{
				sprite.scale = time / (3.0f / (speed/100.0f)) * scaleFactor;
			}
			else 
			{
				sprite.scale = scaleFactor;
				state = FALLING;
			}

		}
			break;
		case FALLING:
		{
			CGPoint p = sprite.position;
			p.y -=  speed * t;
			if ( p.y /*- sprite.contentSize.height * scaleFactor */ <= 0 )
			{
				p.y = 0;
				state = DYING;
			}
			sprite.position = p;
		}
			break;
		case DYING:
		{
			
		}
			break;
			
		default:
			break;
	}
}

void Drop::die()
{
	state = DYING;
}