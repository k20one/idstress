/*
 *  Drop.cpp
 *  Work
 *
 *  Created by Miguel on 30/04/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#include "Score.h"
@implementation Score


- (id)initPuntuation:(int)points location:(CGPoint)location layer:(CCLayer *)l 
{
	if( (self=[super init])) 
	{
		NSString* t;
		if (points >= 0)
			t = [NSString stringWithFormat:@"+%d", points];
		else
			t = [NSString stringWithFormat:@"%d", points];
		text = [CCLabelTTF labelWithString:t fontName:@"Helvetica" fontSize:15];
		text.anchorPoint = ccp(0.5,0.5);
		text.position = location;
		
		text.color = points >= 0 ? ccc3(10, 150, 10) : ccc3(245, 0, 0);
		
		id seq = [CCSequence actions:[CCMoveBy actionWithDuration:1.0f position:ccp(0, 30)],[CCCallFunc actionWithTarget:self selector:@selector(end)], nil];
		[text runAction:seq];
		
		text.opacity = 0;
		id seq2 = [CCSequence actions:[CCFadeIn actionWithDuration:0.2f],[CCDelayTime actionWithDuration:0.5],[CCFadeOut actionWithDuration:0.2f], nil];
		[text runAction:seq2];
		
		layer = l;
		[layer addChild: text z: 1];
		
    }
    return self;
}

-(void) end
{
	[layer removeChild:text cleanup: true];
}

@end
