/*
 *  Drop.h
 *  Work
 *
 *  Created by Miguel on 30/04/12.
 *  Copyright 2012 __MyCompanyName__. All rights reserved.
 *
 */

#import "cocos2d.h"

class Drop
{
	public :
		Drop( int x, int y,  CCLayer* layer, int type, bool good, bool retina );
		~Drop();
	
		void update( float t );
	
		void die();
	
		float			speed;
		CCSprite*		sprite;
		CCLayer*		layer;
		float			time;
		int				type;
		bool			good;
		float			scaleFactor;
		
		enum DropState
		{
			GROWING,
			FALLING,
			DYING,
			FINISHED
		};
	
		DropState state;
};