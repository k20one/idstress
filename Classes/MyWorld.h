//
//  MyWorld.h
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"

@interface MyWorld : UIViewController <FBDialogDelegate>{
	IBOutlet UILabel *nivel;
	IBOutlet UILabel *nivel2;
	IBOutlet UIImageView *animacion;
	IBOutlet UIProgressView *progresoBarra;
}
@property (nonatomic, retain) IBOutlet UIProgressView *progresoBarra;
@property (nonatomic, retain) IBOutlet UIImageView *animacion;	
@property (nonatomic, retain) UILabel *nivel;
@property (nonatomic, retain) UILabel *nivel2;

- (IBAction) empezar: (id)sender;
- (IBAction) compartirFacebook: (id)sender;
- (IBAction) compartirTwitter: (id)sender;

@end
