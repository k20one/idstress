//
//  PostElementos.m
//  iDStress
//
//  Created by Pold on 07/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PostElementos.h"
#import "Appirater.h"
#import "Tratamientos.h"

@implementation PostElementos
@synthesize guia;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen = @"";
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
            self.title = NSLocalizedString(@"Water",nil);
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
            self.title = NSLocalizedString(@"Fire",nil);
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
            self.title = NSLocalizedString(@"Earth",nil);
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
            self.title = NSLocalizedString(@"Air",nil);
			break;
		default:
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
    [Appirater userDidSignificantEvent:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen = @"";
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
    self.navigationController.navigationBarHidden = NO;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[guia release];
    [super dealloc];
}


- (IBAction)tratamientosTouched:(id)sender {
    [self.tabBarController setSelectedIndex:1];
}
@end
