//
//  PrePanicButton.h
//  iD-Stress
//
//  Created by Pold on 18/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PrePanicButton : UIViewController {
	IBOutlet UIButton *empezar;
	IBOutlet UIImageView *guia;
	IBOutlet UIImageView *animacion;
}

@property (nonatomic, retain) IBOutlet UIImageView *animacion;
@property (nonatomic, retain) IBOutlet UIImageView *guia;
@property (nonatomic, retain) UIButton *empezar;
- (IBAction) empezar: (id)sender;

@end
