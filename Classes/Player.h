//
//  Player.h
//  iD-Stress
//
//  Created by Leopold Riola on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MPMoviePlayerViewController;
@interface Player : UIViewController {
    MPMoviePlayerViewController *playerView;
}
@property (nonatomic, retain)  MPMoviePlayerViewController *playerView;
@property (nonatomic, assign) UIViewController *parent;
- (IBAction) empezar: (id)sender;
@end