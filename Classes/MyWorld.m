//
//  MyWorld.m
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyWorld.h"
#import "Info.h"
#import "Appirater.h"
#import "Flurry.h"
#import "UserInfo.h"
#import <Twitter/Twitter.h>
#import "SessionM.h"

@implementation MyWorld
@synthesize nivel, nivel2, animacion, progresoBarra;
NSTimer *pauseTimer;
int numNivel;

- (void) postOnFacebook
{
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"134061643340779", @"app_id",
                                   @"iDStress", @"name",
                                  [NSString stringWithFormat:NSLocalizedString(@"My world in iD-Stress has grown! Level: %i",nil), numNivel], @"message",
                                   nil];
    
    [[UserInfo sharedInstance].facebook dialog:@"feed" andParams:params andDelegate:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"FBLogin" object:nil];
    [Flurry logEvent:@"Compartir" withParameters:@{@"Ventana": @"Evolucion", @"Plataforma":@"Facebook"}];
    SMAction(@"Facebook");
}

- (void)compartirFacebook:(id)sender{
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postOnFacebook) name:@"FBLogin" object:nil];
    [[UserInfo sharedInstance] startFbLogin];
}
- (void)compartirTwitter:(id)sender{
    TWTweetComposeViewController *tweetViewController = [[TWTweetComposeViewController alloc] init];
    
    // Set the initial tweet text. See the framework for additional properties that can be set.
    [tweetViewController setInitialText:[NSString stringWithFormat:NSLocalizedString(@"My world in iD-Stress has grown! Level: %i",nil), numNivel]];
    
    // Create the completion handler block.
    [tweetViewController setCompletionHandler:^(TWTweetComposeViewControllerResult result) {
        [self dismissModalViewControllerAnimated:YES];
        [tweetViewController release];
    }];
    
    // Present the tweet composition view controller modally.
    [self presentModalViewController:tweetViewController animated:YES];
    [Flurry logEvent:@"Compartir" withParameters:@{@"Ventana": @"Evolucion", @"Plataforma":@"Twitter"}];
    SMAction(@"Twitter");
}

-(IBAction) info: (id)sender {
	Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
    info.texto1 =  [[NSMutableString alloc] initWithString: NSLocalizedString(@"\n Aquí verás la evolución de tu mundo en iD-Stress.",nil)];
	info.texto2 = [[NSMutableString alloc] initWithString: NSLocalizedString(@"\n\n Para poder subir de nivel y evolucionar tus plantas, debes utilizar iD-Stress y sus tratamientos. Esto acumulará puntos de crecimiento que se utilizarán cuando pulses el botón 'Actualizar'.",nil)];
	[self.navigationController pushViewController:info animated:YES];
	[info.infoTexto1 setText:info.texto1];
	[info.infoTexto2 setText:info.texto2];
	[info release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	SET_NAVIGATION_BAR;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Home",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(showHome)];
    
	self.title = NSLocalizedString(@"Evolution",nil);

	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	int actual = [prefs integerForKey:@"nivelActual"];
	[animacion setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d.png", (actual%50)+1]]];
	[progresoBarra setProgress:(float)(actual%50)/50];
	numNivel = (int) (actual/50);
	nivel.text= [NSString stringWithFormat:@"%i", numNivel];
	nivel2.text = [NSString stringWithFormat:NSLocalizedString(@"Level: %i",nil), numNivel];
}

-(void)showHome{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) viewWillAppear: (BOOL)animated {
	[super viewWillAppear:animated];
    [Flurry logEvent:@"Evolucion"];
}


- (IBAction) empezar: (id)sender{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [Flurry logEvent:@"Evolucionar" withParameters:[NSDictionary dictionaryWithObject:[NSNumber numberWithInt:[prefs integerForKey:@"nivelFinal"]] forKey:@"nivel"]];
	pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(loop) userInfo:nil repeats:YES];
    [Appirater userDidSignificantEvent:YES];
    SMAction(@"Evolution");
}
- (void) loop {
	
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	int final = [prefs integerForKey:@"nivelFinal"];
	int actual = [prefs integerForKey:@"nivelActual"];
	if (final>=actual) {
		[animacion setImage: [UIImage imageNamed:[NSString stringWithFormat:@"%d.png", (actual%50)+1]]];
		[progresoBarra setProgress:(float)(actual%50)/50];
		numNivel = (int) (actual/50);
		nivel.text= [NSString stringWithFormat:@"%i", numNivel];
		nivel2.text = [NSString stringWithFormat:NSLocalizedString(@"Level: %i",nil), numNivel];
		actual++;
		[prefs setInteger:actual forKey:@"nivelActual"];
	}
	else {
		[pauseTimer invalidate];
		pauseTimer = nil;
	}
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
