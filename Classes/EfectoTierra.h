//
//  EfectoTierra.h
//  iDStress
//
//  Created by Sophy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "cocos2d.h"
#import "SpaceManager.h"
#import "cpCCSprite.h"
#import "cpShapeNode.h"
#import "cpMouse.h"
#import "drawSpace.h" 
#import "Bloque_verde.h"

#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#include <vector>
#include <map>

#define SHAKING_TIME 1.0f
#define IS_IPHONE_5 ([[UIScreen mainScreen ] bounds].size.height == 568.0f)

struct Grain 
{
	ccVertex2F direction;
	CCSprite*  sprite;
	Grain( int x, int y, CCSpriteBatchNode* batch, bool retina )
	{
		sprite = [CCSprite spriteWithFile:@"fire.png"];
		sprite.color = ccc3(255, 255, 200);
		sprite.opacity = 70;
		//sprite.blendFunc.src = GL_ONE;
		if (retina)
		{
			sprite.scale = 2;
		}
		[batch addChild:sprite];
		
		sprite.position = ccp(x,y);
		direction.x = direction.y = 0;
	};
};

// HelloWorld Layer
@interface EfectoTierra : CCLayer
{
	CCMenu				*backMenu;
	CCSprite			*background;
	CCSprite			*fondo;
    CCLabelTTF          *labelSacude;
	std::vector<Grain*>				granos;
	std::map<unsigned int, CGPoint>	hotSpots;
	
	CCSpriteBatchNode			*batchNode;
	CGSize						screenSize;
	CGSize						screenSize2;
	
	float						shakeTime;
	bool						shaking;
	 
}

// returns a Scene that contains the HelloWorld as the only child
+(id) scene;
-(void) startShake;

@end
