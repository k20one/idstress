//
//  StateTableCellView.h
//  States
//
//  Created by Julio Barros on 1/26/09.
//  Copyright 2009 E-String Technologies, Inc.. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomCell : UITableViewCell {
	IBOutlet UILabel *nombre;
	IBOutlet UILabel *descripcion;
	IBOutlet UILabel *duracion;
	IBOutlet UIImageView *icon;
}

@property (nonatomic,retain) IBOutlet UILabel *nombre;
@property (nonatomic,retain) IBOutlet UILabel *descripcion;
@property (nonatomic,retain) IBOutlet UILabel *duracion;
@property (nonatomic,retain) IBOutlet UIImageView *icon;

@end
