//
//  EfectoAire.m
//  iDStress
//
//  Created by Sophy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EfectoTierra.h"
#import "drawSpace.h"
#import "cpMouse.h"
#import "SessionM.h"

@implementation EfectoTierra

//#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

+(id) scene
{
	CCScene *scene = [CCScene node];
	EfectoTierra *layer = [EfectoTierra node];
	layer.isTouchEnabled=YES;
	[scene addChild: layer];
	return scene;
}



// inicializar la instancia
-(id) init
{		
	if( (self=[super init] )) 
	{
		// glClearColor(0.4, 0.4, 0.4, 1);
		
		self.isTouchEnabled = YES;
		
		CCDirector *director = [CCDirector sharedDirector];

		if ( [director enableRetinaDisplay:YES] || UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		{
			fondo = [[CCSprite spriteWithFile:@"arena_hd.jpeg"] retain];
			if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			{
                fondo.scaleX = 1.22;
                fondo.scaleY = 1.09;
				if ([director enableRetinaDisplay:YES]) {
                    fondo.scaleX = 2.40;
                    fondo.scaleY = 2.44;
                }
			}
            if(IS_IPHONE_5)
               fondo.scaleY = 1.18;
		}
		else
		{
			fondo = [[CCSprite spriteWithFile:@"arena.jpeg"] retain];
		}
		fondo.anchorPoint = CGPointZero;
		[self addChild:fondo];
		
		CCMenuItem *backMenuItem;
		CCMenuItem *restartMenuItem;
		bool retina = false;
		if ([director enableRetinaDisplay:YES])
		{
			retina = true;
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png" target:self selector:@selector(backButtonTapped:)];
			restartMenuItem = [CCMenuItemImage itemFromNormalImage:@"reiniciar_hd.png" selectedImage:@"reiniciar_hd.png" target:self selector:@selector(restartTapped:)];
			restartMenuItem.position = ccp(30, 450);
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
				restartMenuItem.position = ccp(30, 75);
            
            if(IS_IPHONE_5)
                restartMenuItem.position = ccp(30, 535);
		}
		else
		{
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png" target:self selector:@selector(backButtonTapped:)];
			restartMenuItem = [CCMenuItemImage itemFromNormalImage:@"reiniciar_regular.png" selectedImage:@"reiniciar_regular.png" target:self selector:@selector(restartTapped:)];
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
				restartMenuItem.position = ccp(30, 75);
            else 
				restartMenuItem.position = ccp(30, 450);			
		}
		
		backMenuItem.position = ccp(30, 30);
		backMenu = [CCMenu menuWithItems:backMenuItem, restartMenuItem, nil];
		backMenu.position = CGPointZero;
		
		
		
		batchNode = [CCSpriteBatchNode batchNodeWithFile:@"fire.png"];
		[self addChild:batchNode];
		
		screenSize = [CCDirector sharedDirector].winSize;
		for (int i = 0; i<screenSize.width; i+=15) 
		{
			for (int j = 0; j<screenSize.height; j+= 15) 
			{
				Grain* g = new Grain(i, j, batchNode, retina);
				granos.push_back( g );
				
			}
		}
		
		[self addChild:backMenu];
		[[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30)];
		[UIAccelerometer sharedAccelerometer].delegate = self;
		self.isAccelerometerEnabled = YES;
		[ self schedule:@selector( update: ) ];
		
		shaking = false;
        
        labelSacude = [CCLabelTTF labelWithString:NSLocalizedString(@"Shake to restart",nil) fontName:@"Helvetica" fontSize:20];
		labelSacude.color = ccWHITE;
        
        CGSize size = [[CCDirector sharedDirector] winSize];
		labelSacude.position = ccp(size.width/2,20);
		[self addChild:labelSacude z:0];
	}
	return self;
}

- (void)update:(ccTime)dt 
{
	
	if (shaking)
	{
		labelSacude.visible = NO;
		int index = 0;
		float lerp = 0.75;
		for (int i = 0; i<screenSize.width; i+=15) 
		{
			for (int j = 0; j<screenSize.height; j+= 15) 
			{
				float x = granos[ index ]->sprite.position.x + ((i - granos[ index ]->sprite.position.x ) * lerp);
				float y = granos[ index ]->sprite.position.y + ((j - granos[ index ]->sprite.position.y ) * lerp);
				granos[ index ]->sprite.position = ccp(x,y);
				index++;
			}
		}
		shakeTime -= dt;
		if (shakeTime <= 0) 
			shaking = false;
		return;
	}
	// modify inertia with fingers
	std::map<unsigned int, CGPoint>::iterator hotSpotsIt;
	for ( hotSpotsIt = hotSpots.begin(); hotSpotsIt != hotSpots.end(); hotSpotsIt++ ) 
	{
		CGPoint p = hotSpotsIt->second;
		std::vector<Grain*>::iterator it;
		for (it = granos.begin(); it != granos.end(); it++) 
		{
			Grain* g = *it;
			CGPoint newInertia;
			newInertia.x = g->sprite.position.x - p.x;
			newInertia.y = g->sprite.position.y - p.y;
			
			float l = cpvlengthsq(newInertia);
			if (l < 200)
			{
				if (l < 100) l = 100;
				g->direction.x += (newInertia.x / (l)) * 100;
				g->direction.y += (newInertia.y / (l)) * 100;
                g->sprite.opacity = 20;
			}
		}			
	}
	// hotSpots.clear();
	
	// Move grains with inertia
	std::vector<Grain*>::iterator it;
	for (it = granos.begin(); it != granos.end(); it++) 
	{
		Grain* g = *it;
		double x = 0;
		x = g->sprite.position.x + (g->direction.x );
		x = clampf(x, 0 , screenSize.width);
		g->direction.x = g->direction.x * dt * 10;
		
		double y = 0;
		y = g->sprite.position.y + (g->direction.y);
		y = clampf(y, 0 , screenSize.height);
		g->direction.y = g->direction.y * dt * 10;
		g->sprite.position = ccp(x,y);
		
		/*
		std::vector<Grain*>::iterator it2;
		for (it2 = it + 1; it2 != granos.end(); it2++) 
		{
			Grain* g2 = *it2;
			CGPoint newInertia;
			newInertia.x = g->sprite.position.x - g2->sprite.position.x;
			newInertia.y = g->sprite.position.y - g2->sprite.position.y;
			float l =  cpvlengthsq(newInertia);
			if ( l < 30 )
			{
				if (l < 0.1) l = 0.1;
				g->direction.x += 1.0f/(l * 2);
				g->direction.y += 1.0f/(l * 2);
			}
		}
		 */
	}
}



//genera el botón para regresar a la escena anterior o salir del juego
- (void)backButtonTapped:(id)sender 
{
	// Sets landscape mode
	CCDirector *director = [CCDirector sharedDirector];
	[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	glColor4ub(255,255,255,255);
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
    
    SMAction(@"Sand");
}

- (void)restartTapped:(id)sender 
{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[EfectoTierra scene]]];    
}



- (void)registerWithTouchDispatcher 
{
    [[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:YES];
}

- (BOOL)ccTouchBegan:(UITouch *)touch withEvent:(UIEvent *)event 
{
	
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
	{
		CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
		// touchLocation.x = touchLocation.x * ( screenSize.width /  screenSize2.width);
		// touchLocation.y = touchLocation.y * ( screenSize.height /  screenSize2.height);
		hotSpots[ [touch hash] ] = touchLocation;
	}
	
    return YES;
}

- (void)ccTouchMoved:(UITouch *)touch withEvent:(UIEvent *)event 
{
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
	{
		CGPoint touchLocation = [self convertTouchToNodeSpace:touch];
		// touchLocation.x = touchLocation.x * ( screenSize.width /  screenSize2.width);
		// touchLocation.y = touchLocation.y * ( screenSize.height /  screenSize2.height);
		hotSpots[ [touch hash] ] = touchLocation;
	}
}
- (void)ccTouchEnded:(UITouch *)touch withEvent:(UIEvent *)event 
{
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
	{
		hotSpots.erase( [touch hash] );
	}	
}

- (void)ccTouchCancelled:(UITouch *)touch withEvent:(UIEvent *)event 
{
	NSSet *allTouches = [event allTouches]; 
	for (UITouch *touch in allTouches)
	{
		hotSpots.erase( [touch hash] );
	}    
}

-(void) startShake
{
	shaking = true;
	shakeTime = SHAKING_TIME;
	int index = 0;
	for (int i = 0; i<screenSize.width; i+=15) 
	{
		for (int j = 0; j<screenSize.height; j+= 15) 
		{
			granos[ index ]->direction.x = 0;
			granos[ index ]->direction.y = 0;
            granos[ index ]->sprite.opacity = 65;
			index++;
		}
	}
}

- (void)accelerometer:(UIAccelerometer*)accelerometer didAccelerate:(UIAcceleration*)acceleration
{	
	static bool shake_once = false;
	float THRESHOLD = 2;
	
	if (acceleration.x > THRESHOLD || acceleration.x < -THRESHOLD || 
		acceleration.y > THRESHOLD || acceleration.y < -THRESHOLD ||
		acceleration.z > THRESHOLD || acceleration.z < -THRESHOLD) 
	{
		
		if (!shake_once) 
		{
			shake_once = true;
			[self startShake];
		}
	}
	else 
	{
		shake_once = false;
	}
	
}

//
// Recibiendo llamada, pausa la aplicación
-(void) applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

// llamada rechazada
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	[[CCDirector sharedDirector] startAnimation];
}

// aplicación será killed
- (void)applicationWillTerminate:(UIApplication *)application
{	
	CC_DIRECTOR_END();
}

// purgar memoria 
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}
- (void) dealloc
{
	[super dealloc];
}

@end



