//
//  Elementos.m
//  iD-Stress
//
//  Created by Pold on 15/03/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Elementos.h"
#import "Flurry.h"

@implementation Elementos
@synthesize botonAgua,botonFuego,botonTierra,botonAire;

- (IBAction) agua: (id)sender{
    [Flurry logEvent:@"Elementos" withParameters:[NSDictionary dictionaryWithObject:@"Agua" forKey:@"Elemento"]];
    //Guardar agua como elemento guia
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setInteger:1 forKey:@"elemento"];
    
	[[NSNotificationCenter defaultCenter] postNotificationName:@"empezarEfectoAgua" object:@""];
    PostElementos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos-iPad" bundle:nil];        
    } else {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos" bundle:nil];
    }
	[self.navigationController pushViewController:post animated:YES];
	[post release];
}

- (IBAction) fuego: (id)sender{
    [Flurry logEvent:@"Elementos" withParameters:[NSDictionary dictionaryWithObject:@"Fuego" forKey:@"Elemento"]];
    //Guardar fuego como elemento guia
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setInteger:2 forKey:@"elemento"];
    
	[[NSNotificationCenter defaultCenter] postNotificationName:@"empezarEfectoFuego" object:@""];
    PostElementos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos-iPad" bundle:nil];        
    } else {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos" bundle:nil];
    }
    [self.navigationController pushViewController:post animated:YES];
	[post release];
}
- (IBAction) tierra: (id)sender{
    [Flurry logEvent:@"Elementos" withParameters:[NSDictionary dictionaryWithObject:@"Tierra" forKey:@"Elemento"]];
    //Guardar tierra como elemento guia
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setInteger:3 forKey:@"elemento"];
    
	[[NSNotificationCenter defaultCenter] postNotificationName:@"empezarEfectoTierra" object:@""];
    PostElementos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos-iPad" bundle:nil];        
    } else {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos" bundle:nil];
    }
    [self.navigationController pushViewController:post animated:YES];
	[post release];
	
}
- (IBAction) aire: (id)sender{
    [Flurry logEvent:@"Elementos" withParameters:[NSDictionary dictionaryWithObject:@"Aire" forKey:@"Elemento"]];
    //Guardar aire como elemento guia
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	[prefs setInteger:4 forKey:@"elemento"];
    
	[[NSNotificationCenter defaultCenter] postNotificationName:@"empezarEfectoAire" object:@""];
    PostElementos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos-iPad" bundle:nil];        
    } else {
        post =[[PostElementos alloc] initWithNibName:@"PostElementos" bundle:nil];
    }
    [post viewWillAppear:YES];
    [self.navigationController pushViewController:post animated:YES];
	[post release];
	
}



- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Elements",nil);
	SET_NAVIGATION_BAR;
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
}

- (void)viewWillAppear:(BOOL)animated {
    if (self.tabBarController.selectedIndex!=0) {
        [Flurry logEvent:@"Tab bar" withParameters:[NSDictionary dictionaryWithObject:@"Elementos" forKey:@"Tab"]];
    }
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[botonAgua release];
	[botonFuego release];
	[botonTierra release];
	[botonAire release];
    [super dealloc];
}


@end
