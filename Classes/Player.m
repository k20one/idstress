//
//  Player.m
//  iD-Stress
//
//  Created by Leopold Riola on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Player.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Flurry.h"
#import "PostTratamientos.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "StoreManager.h"


@implementation Player
@synthesize playerView;
@synthesize parent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) showWarning {
    [self performSelector:@selector(empezar:) withObject:nil afterDelay:10];
}
- (IBAction) empezar: (id)sender{
    if (!playerView) {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        char inicial = [[prefs stringForKey:@"Tratamiento"] characterAtIndex:0];
        NSString *fileName;
        NSDictionary *dict = [[[NSDictionary alloc] initWithObjectsAndKeys:[prefs stringForKey:@"Tratamiento"],@"Tratamiento", nil] autorelease];
        [Flurry logEvent:@"Tratamiento Reproducido" withParameters:dict];
        switch (inicial) {
            case 'R':
                fileName = @"BasicRelax.mp3";
                break;
            case 'C':
                fileName =@"Concentration.mp3";
                break;
            case 'E':
                fileName = @"Pregnant.mp3";
                break;
            case 'I':
                fileName = @"Insomnia.mp3";
                break;
            case 'G':
                fileName = @"RelaxGesture.mp3";
                break;
            case 'P':
                fileName = @"PanicButton.mp3";
                break;
            default:
                fileName = @"BasicRelax.mp3";
                break;
        }
        NSString *path;
        NSURL *movieURL;
        
        if (inicial=='P') {
            path = [NSString stringWithFormat:NSLocalizedString(@"%@/en.lproj/%@",nil),
                              [[NSBundle mainBundle] resourcePath], fileName];
            movieURL = [NSURL fileURLWithPath:path];
        }
        else {
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
            NSString *directory = [paths objectAtIndex:0];
            directory = [directory stringByAppendingPathComponent:@"Downloads"];
            directory = [directory stringByAppendingPathComponent:[prefs stringForKey:@"Tratamiento"]];
            
            path = [NSString stringWithFormat:NSLocalizedString(@"%@/en.lproj/%@",nil),
                              directory, fileName];
            movieURL = [NSURL fileURLWithPath:path];
        }
        if ([movieURL checkResourceIsReachableAndReturnError:nil] == NO) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            switch (inicial) {
                case 'R':
                    [defaults setBool:NO forKey:iDStress1];
                    break;
                case 'C':
                    [defaults setBool:NO forKey:iDStress2];
                    break;
                case 'E':
                    [defaults setBool:NO forKey:iDStress4];
                    break;
                case 'I':
                    [defaults setBool:NO forKey:iDStress3];
                    break;
                case 'G':
                    [defaults setBool:NO forKey:iDStress5];
                    break;
                default:
                    break;
            }
            [self.navigationController popViewControllerAnimated:NO];
            return;
        }
        
        playerView = [[[MPMoviePlayerViewController alloc] initWithContentURL:movieURL] autorelease];
        [playerView.moviePlayer prepareToPlay];
        UIImageView *backgroundImage;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Portrait~ipad.png"]] autorelease];
        else backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default.png"]] autorelease];
        
        [playerView.moviePlayer.backgroundView addSubview:backgroundImage];
        [[AVAudioSession sharedInstance] setActive: YES error: nil];
        [playerView.moviePlayer play];
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            playerView.moviePlayer.scalingMode= MPMovieScalingModeAspectFill;
        }
        [self  presentMoviePlayerViewControllerAnimated:playerView];
        [self setWantsFullScreenLayout:YES]; 
    }
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    // Obtain the reason why the movie playback finished
    MPMoviePlayerController *moviePlayer = [aNotification object];
    [Flurry logEvent:@"Tratamiento finalizado" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:[[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey],@"Motivo",[NSNumber numberWithInt:(int)(moviePlayer.currentPlaybackTime/60)], @"Minuto" , nil]];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
    
    
    self.navigationController.navigationBarHidden = NO;
     self.tabBarController.tabBar.hidden = NO;
    PostTratamientos *post;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        post =[[PostTratamientos alloc] initWithNibName:@"PostTratamientos-iPad" bundle:nil];
    } else {
        post =[[PostTratamientos alloc] initWithNibName:@"PostTratamientos" bundle:nil];
    }
    [self.navigationController pushViewController:post animated:YES];
    [post release];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    NSError *setCategoryError = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (setCategoryError) { NSLog(@"Error setcategory"); }
    
    NSError *activationError = nil;
    [audioSession setActive:YES error:&activationError];
    if (activationError) { NSLog(@"Error activation"); }
    UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
    AudioSessionSetProperty (kAudioSessionProperty_AudioCategory, sizeof (sessionCategory), &sessionCategory);
    AudioSessionSetActive(true);
    
    self.navigationController.navigationBarHidden =YES;
    self.tabBarController.tabBar.hidden = YES;
    
    [self showWarning];
}
- (void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:playerView.moviePlayer];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
