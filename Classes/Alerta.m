//
//  Alerta.m
//  iDStress
//
//  Created by Pold on 18/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Alerta.h"
#import "Flurry.h"

@implementation Alerta
@synthesize infoView;
@synthesize eventsList, eventStore, defaultCalendar, detailViewController;


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
	[eventStore release];
	[eventsList release];
	[defaultCalendar release];
	[detailViewController release];
	
    [infoView release];
    [_notificationsView release];
	[super dealloc];
}


#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
	self.title = NSLocalizedString(@"Alerts",nil);
    SET_NAVIGATION_BAR;
	UIView *backgroundView = [[UIView alloc] initWithFrame: self.tableView.frame];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background-iPad.png"]];
    } else {
        backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]];
    }
    
    self.tableView.backgroundView = backgroundView;
    
	self.tableView.separatorColor = [UIColor whiteColor];
	[backgroundView release];
    
	
	
	// Initialize an event store object with the init method. Initilize the array for events.
	self.eventStore = [[EKEventStore alloc] init];
	
	self.eventsList = [[NSMutableArray alloc] initWithArray:0];
	
	// Get the default calendar from store.
	self.defaultCalendar = [self.eventStore defaultCalendarForNewEvents];
    
	//	Create an Add button 
	UIBarButtonItem *addButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
									  UIBarButtonSystemItemAdd target:self action:@selector(addEvent:)];
	self.navigationItem.rightBarButtonItem = addButtonItem;
	[addButtonItem release];
	
	
	self.navigationController.delegate = self;
}


- (void)viewDidUnload {
    [self setNotificationsView:nil];
    [self setInfoView:nil];
	self.eventsList = nil;
}


- (void)viewWillAppear:(BOOL)animated {
	[self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:NO];
    [self.eventsList removeAllObjects];
    if (self.defaultCalendar)    [self.eventsList addObjectsFromArray:[self fetchEventsForToday]];
    [self.tableView reloadData];
    [Flurry logEvent:@"View Alerta"];
}


// Support all orientations except for Portrait Upside-down.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark -
#pragma mark Table view data source

// Fetching events happening in the next 24 hours with a predicate, limiting to the default calendar 
- (IBAction)notificationsSwitched:(id)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:[((UISwitch *)sender) isOn] forKey:@"FraseAlarma"];
    
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[[UILocalNotification alloc]
                                         init] autorelease];
    if ([((UISwitch *)sender) isOn]) {
        [Flurry logEvent:@"Notificaciones" withParameters:@{@"Estado": @"On"}];
        NSDate *alertTime = [[NSDate date]
                             dateByAddingTimeInterval:24 * 60 * 60 - 600];
        
        NSString *fileText = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:NSLocalizedString(@"FrasesDelDia",nil) ofType:@"txt"] encoding:NSUTF8StringEncoding error:nil];
        NSMutableArray *frases = [[NSMutableArray alloc]initWithArray:[fileText componentsSeparatedByString:@"\n"]];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:alertTime];
        NSInteger dia = [components day];
        NSInteger mes = [components month];
        int index = ((dia + ((mes%3)*30))%frases.count);
        
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.alertAction = @"iDStress";
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        notifyAlarm.alertBody = [frases objectAtIndex:index];
        [app scheduleLocalNotification:notifyAlarm];
    }
    else {
        [Flurry logEvent:@"Notificaciones" withParameters:@{@"Estado": @"Off"}];
        [app cancelAllLocalNotifications];
    }
}

- (NSArray *)fetchEventsForToday {
	
	NSDate *startDate = [NSDate date];
	
	// endDate is 1 day = 60*60*24 seconds = 86400 seconds from startDate
	NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:86400*300];
	
	// Create the predicate. Pass it the default calendar.
	NSArray *calendarArray = [NSArray arrayWithObject:defaultCalendar];
	NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:startDate endDate:endDate 
																	calendars:calendarArray]; 
	
	// Fetch all events that match the predicate.
	NSArray *events = [self.eventStore eventsMatchingPredicate:predicate];
	
	return events;
}


#pragma mark -
#pragma mark Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    self.notificationsView.frame = CGRectMake(0, 285, self.notificationsView.frame.size.width, self.notificationsView.frame.size.height);
    if (eventsList.count == 0) {
        [self.view addSubview:infoView];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            infoView.frame = CGRectMake(220, 300, infoView.frame.size.width, infoView.frame.size.height);
            self.notificationsView.frame = CGRectMake(220, 300, self.notificationsView.frame.size.width, self.notificationsView.frame.size.height);
        }
    }
    else if (infoView.superview){
        [infoView removeFromSuperview];
    }
    [self.view addSubview:self.notificationsView];
    
	if (eventsList.count<7) return eventsList.count;
    else return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *CellIdentifier = @"Cell";
	
	// Add disclosure triangle to cell
	UITableViewCellAccessoryType editableCellAccessoryType =UITableViewCellAccessoryDisclosureIndicator;
	
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault 
									   reuseIdentifier:CellIdentifier] autorelease];
	}
	
	cell.accessoryType = editableCellAccessoryType;
	cell.selectionStyle =  UITableViewCellSelectionStyleGray;
	// Get the event at the row selected and display it's title
	cell.textLabel.text = [[self.eventsList objectAtIndex:indexPath.row] title];
	
	return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {	
	// Upon selecting an event, create an EKEventViewController to display the event.
	self.detailViewController = [[EKEventViewController alloc] initWithNibName:nil bundle:nil];			
	detailViewController.event = [self.eventsList objectAtIndex:indexPath.row];
	
	// Allow event editing.
	detailViewController.allowsEditing = YES;
	
	//	Push detailViewController onto the navigation controller stack
	//	If the underlying event gets deleted, detailViewController will remove itself from
	//	the stack and clear its event property.
	[self.navigationController pushViewController:detailViewController animated:YES];
	
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES ;
}
-(void)tableView:(UITableView *)tv commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete your data
        EKEvent *event = [self.eventsList objectAtIndex:indexPath.row];
        [self.eventStore removeEvent:event span:EKSpanThisEvent commit:YES error:nil];
        
        [self.eventsList removeAllObjects];
        if (self.defaultCalendar)    [self.eventsList addObjectsFromArray:[self fetchEventsForToday]];
        
        if (self.eventsList.count<6)
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        else [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark Navigation Controller delegate

- (void)navigationController:(UINavigationController *)navigationController 
	  willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
	// if we are navigating back to the rootViewController, and the detailViewController's event
	// has been deleted -  will title being NULL, then remove the events from the eventsList
	// and reload the table view. This takes care of reloading the table view after adding an event too.
	if (viewController == self && self.detailViewController.event.title == NULL) {
		[self.eventsList removeObject:self.detailViewController.event];
		[self.tableView reloadData];
	}
}


#pragma mark -
#pragma mark Add a new event

// If event is nil, a new event is created and added to the specified event store. New events are 
// added to the default calendar. An exception is raised if set to an event that is not in the 
// specified event store.
- (void)addEvent:(id)sender {
    [Flurry logEvent:@"Evento creado"];
	// When add button is pushed, create an EKEventEditViewController to display the event.
	EKEventEditViewController *addController = [[EKEventEditViewController alloc] initWithNibName:nil bundle:nil];
	EKEvent *event = [EKEvent eventWithEventStore:self.eventStore];
	event.location = @"iDStress";
	event.title = NSLocalizedString(@"iDStress event",nil);
	event.notes = NSLocalizedString(@"Do not forget to use iDStress.",nil);
	
	// set the addController's event store to the current event store.
	addController.eventStore = self.eventStore;
	addController.event = event;
	
	// present EventsAddViewController as a modal view controller
	[self presentModalViewController:addController animated:YES];
	
	addController.editViewDelegate = self;
	[addController release];
}


#pragma mark -
#pragma mark EKEventEditViewDelegate

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller 
		  didCompleteWithAction:(EKEventEditViewAction)action {
	
	NSError *error = nil;
	EKEvent *thisEvent = controller.event;
	
	switch (action) {
		case EKEventEditViewActionCanceled:
			// Edit action canceled, do nothing. 
			break;
			
		case EKEventEditViewActionSaved:
			// When user hit "Done" button, save the newly created event to the event store, 
			// and reload table view.
			// If the new event is being added to the default calendar, then update its 
			// eventsList.
			if (self.defaultCalendar ==  thisEvent.calendar) {
				[self.eventsList addObject:thisEvent];
			}
			[controller.eventStore saveEvent:controller.event span:EKSpanThisEvent error:&error];
			[self.tableView reloadData];
			break;
			
		case EKEventEditViewActionDeleted:
			// When deleting an event, remove the event from the event store, 
			// and reload table view.
			// If deleting an event from the currenly default calendar, then update its 
			// eventsList.
			if (self.defaultCalendar ==  thisEvent.calendar) {
				[self.eventsList removeObject:thisEvent];
			}
			[controller.eventStore removeEvent:thisEvent span:EKSpanThisEvent error:&error];
			[self.tableView reloadData];
			break;
			
		default:
			break;
	}
	// Dismiss the modal view controller
	[controller dismissModalViewControllerAnimated:YES];
	
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller {
	EKCalendar *calendarForEdit = self.defaultCalendar;
	return calendarForEdit;
}


@end