//
//  EfectoAire.m
//  iDStress
//
//  Created by Sophy on 06/04/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EfectoAire.h"
#import "SimpleAudioEngine.h"
#import "CDAudioManager.h"
#import "SessionM.h"

@implementation EfectoAire
@synthesize backMenu;


//#ifdef __IPHONE_OS_VERSION_MAX_ALLOWED
-(void) registerWithTouchDispatcher
{
	[[CCTouchDispatcher sharedDispatcher] addTargetedDelegate:self priority:0 swallowsTouches:NO];
}

-(BOOL) ccTouchBegan:(UITouch*)touch withEvent:(UIEvent*)event
{
	[self ccTouchEnded:touch withEvent:event];
	
	// claim the touch
	return YES;
}
- (void)ccTouchMoved:(UITouch*)touch withEvent:(UIEvent *)event
{
	[self ccTouchEnded:touch withEvent:event];
}

- (void)ccTouchEnded:(UITouch*)touch withEvent:(UIEvent *)event
{
	/*CGPoint location = [touch locationInView: [touch view]];
	CGPoint convertedLocation = [[CCDirector sharedDirector] convertToGL:location];
	
	CGPoint pos = CGPointZero;*/
	
}

- (void)restartTapped:(id)sender
{
    [recorder stop];
    AudioSessionSetActive(false);
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5 scene:[EfectoAire scene]]];
}
//#elif defined(__MAC_OS_X_VERSION_MAX_ALLOWED)

+(id) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	EfectoAire *layer = [EfectoAire node];
	layer.isTouchEnabled=YES;
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// inicializar la instancia
-(id) init
{		
	if( (self=[super init] )) 
	{
		back = [CCLayerGradient layerWithColor:ccc4(56, 140, 124, 255) fadingTo:ccc4(181, 243, 184, 255) ];
		[self addChild: back z: -10];
		
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		
		UInt32 sessionCategory = kAudioSessionCategory_PlayAndRecord;
		UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
		AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
		AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(audioRouteOverride), &audioRouteOverride);
		
		AudioSessionSetActive(true);

		CCDirector *director = [CCDirector sharedDirector];
		
        CCMenuItem *backMenuItem;
		CCMenuItem *restartMenuItem;
		if ([director enableRetinaDisplay:YES])
		{
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_hd.png" selectedImage:@"salir_hd.png" target:self selector:@selector(backButtonTapped:)];
			restartMenuItem = [CCMenuItemImage itemFromNormalImage:@"reiniciar_hd.png" selectedImage:@"reiniciar_hd.png" target:self selector:@selector(restartTapped:)];
			restartMenuItem.position = ccp(30, 450);
            if(IS_IPHONE_5)
                restartMenuItem.position = ccp(30, 535);
            
            CCSprite *fondo;
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                fondo = [CCSprite spriteWithFile:@"diente-de-leon-fondo-hd.png"];
                fondo.scaleX = 2.40;
                fondo.scaleY = 2.44;
                restartMenuItem.position = ccp(30, 75);
            }
            else fondo = [CCSprite spriteWithFile:@"diente-de-leon-fondo-hd.png"];
			fondo.anchorPoint = CGPointZero;
			[self addChild:fondo z:-1];
		}
		else
		{
			backMenuItem = [CCMenuItemImage itemFromNormalImage:@"salir_regular.png" selectedImage:@"salir_regular.png" target:self selector:@selector(backButtonTapped:)];
			restartMenuItem = [CCMenuItemImage itemFromNormalImage:@"reiniciar_regular.png" selectedImage:@"reiniciar_regular.png" target:self selector:@selector(restartTapped:)];
				
            CCSprite *fondo;
			if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                fondo = [CCSprite spriteWithFile:@"diente-de-leon-fondo-hd.png"];
                fondo.scaleX = 1.09;
                fondo.scaleY = 1.22;
                restartMenuItem.position = ccp(30, 75);
            }
            else {
                restartMenuItem.position = ccp(30, 450);
                fondo = [CCSprite spriteWithFile:@"diente-de-leon-fondo.png"];
            }
			fondo.anchorPoint = CGPointZero;
			[self addChild:fondo z:-1];
		}
		
		backMenuItem.position = ccp(30, 30);
		backMenu = [CCMenu menuWithItems:backMenuItem, restartMenuItem, nil];
		backMenu.position = CGPointZero;
		[self addChild:backMenu];
		
		backMenu.visible = YES;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
			[[CCDirector sharedDirector] setDeviceOrientation:CCDeviceOrientationPortraitUpsideDown];
		
		
        
		float altura = 0.2;
		// Load diente de león
        tallo = [[CCSprite spriteWithFile:@"tallo.png"] retain];
		tallo.anchorPoint = ccp(0.5 , 0.85 );
		[self addChild: tallo];
		tallo.position = ccp( size.width / 2, size.height *4/ 7);
        
        bool middle = false;
        bool forth = false;
        bool third = false;
        bool second = false;
        //Lateral ring
		for (int i = rand()&10; i<360; i += 20)
		{
			diente d;
			d.weight = 0.8 + ((rand()%35) / 100.0);
			d.sprite = [[CCSprite spriteWithFile:@"diente.png"] retain];
			d.sprite.anchorPoint = ccp(0.0, 0.0);
			d.sprite.rotation = -i+170;
			d.sprite.scale = 1;
			
			float radians = i * (M_PI / 180.0);
			d.angle = i;
			float x = cos(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&2)-1;
			float y = sin(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&2)-1;
			
			d.sprite.position = ccp( size.width/ 2 + x, size.height *4/ 7 + y);
			dientes.push_back( d );
			[self addChild: d.sprite];
            
            if (((int)i/10)>4&&!middle) {
                
                
            }
            if (((int)i/10)>9&&!forth) {
                forth = true;
                //Forth ring
                for (int i = rand()&10; i<360; i += 25)
                {
                    diente d;
                    d.weight = 0.9 + ((rand()%20) / 100.0);
                    d.sprite = [[CCSprite spriteWithFile:@"diente.png"] retain];
                    d.sprite.anchorPoint = ccp(0.05, 0.05);
                    d.sprite.rotation = -i + 180;
                    d.sprite.scale = 0.95;
                    
                    float radians = i * (M_PI / 180.0);
                    d.angle = i;
                    float x = cos(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&4)-2;
                    float y = sin(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&4)-2;
                    
                    d.sprite.position = ccp( size.width/ 2 + x, size.height *4/ 7 + y);
                    dientes.push_back( d );
                    [self addChild: d.sprite];
                    
                    if (((int)i/10)>9&&!third) {
                        third = true;
                        //Third ring
                        for (int i = rand()&20; i<360; i += 20)
                        {
                            diente d;
                            d.weight = 0.9 + ((rand()%30) / 100.0);
                            d.sprite = [[CCSprite spriteWithFile:@"diente.png"] retain];
                            d.sprite.anchorPoint = ccp(0.25, 0.25);
                            d.sprite.rotation = -i + 180;
                            d.sprite.scale = 0.85;
                            
                            float radians = i * (M_PI / 180.0);
                            d.angle = i;
                            float x = cos(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&6)-3;
                            float y = sin(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&6)-3;
                            
                            d.sprite.position = ccp( size.width/ 2 + x, size.height *4/ 7 + y);
                            dientes.push_back( d );
                            [self addChild: d.sprite];
                            
                            if (((int)i/10)>8&&!second) {
                                second = true;
                                //Second ring
                                for (int i = rand()&30; i<360; i += 40)
                                {
                                    diente d;
                                    d.weight = 2 + ((rand()%30) / 100.0);
                                    d.sprite = [[CCSprite spriteWithFile:@"diente-front.png"] retain];
                                    d.sprite.anchorPoint = ccp(0.1, 0.1);
                                    d.sprite.rotation = -i + 90;
                                    d.sprite.scale = 1.1;
                                    
                                    float radians = i * (M_PI / 180.0);
                                    d.angle = i;
                                    float x = cos(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&8)-4;
                                    float y = sin(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&8)-4;
                                    
                                    d.sprite.position = ccp( size.width/ 2 + x, size.height *4/ 7 + y);
                                    dientes.push_back( d );
                                    [self addChild: d.sprite];
                                    if (((int)i/10)>8&&!middle) {
                                        middle = true;
                                        
                                    }
                                }
                                
                                
                            }
                        }
                    }
                }
            }
		}

        //Middle
        for (int i = rand()&30; i<360; i += 40)
        {
            diente d;
            d.weight = 0.6 + ((rand()%35) / 100.0);
            d.sprite = [[CCSprite spriteWithFile:@"diente-front.png"] retain];
            d.sprite.anchorPoint = ccp(0.35, 0.35);
            d.sprite.rotation = -i + 90;
            d.sprite.scale = 0.8;
            
            d.angle = i;
            
            d.sprite.position = ccp( size.width/ 2 + (int)(rand()&20)-10, size.height *4/ 7 +  (int)(rand()&20)-10);
            dientes.push_back( d );
            [self addChild: d.sprite];
        }
        
        //First ring
        for (int i = rand()&40; i<360; i += 50)
        {
            diente d;
            d.weight = 0.85 + ((rand()%35) / 100.0);
            d.sprite = [[CCSprite spriteWithFile:@"diente-front.png"] retain];
            d.sprite.anchorPoint = ccp(0.5, altura);
            d.sprite.rotation = -i + 90;
            d.sprite.scale = 1;
            
            float radians = i * (M_PI / 180.0);
            d.angle = i;
            float x = cos(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&6)-3;
            float y = sin(radians) * d.sprite.contentSize.height * altura * d.sprite.scale + (int)(rand()&6)-3;
            
            d.sprite.position = ccp( size.width/ 2 + x , size.height *4/ 7 + y);
            dientes.push_back( d );
            [self addChild: d.sprite];
        }
        
        labelSopla = [CCLabelTTF labelWithString:NSLocalizedString(@"Blow into the mic",nil) fontName:@"Helvetica" fontSize:20];
		labelSopla.color = ccWHITE;
        
		labelSopla.position = ccp(size.width/2,20);
		[self addChild:labelSopla z:0];
	}
    
	return self;
	
}
//Creación de las partículas de aire

-(void) onEnter
{
	[super onEnter];

	NSError *activationError = nil;
	[[AVAudioSession sharedInstance] setActive: YES error: &activationError];
	
	NSURL *url = [NSURL fileURLWithPath:@"/dev/null"];
	
	NSDictionary *settings = [NSDictionary dictionaryWithObjectsAndKeys:
							  [NSNumber numberWithFloat: 44100.0],                 AVSampleRateKey,
							  [NSNumber numberWithInt: kAudioFormatAppleLossless], AVFormatIDKey,
							  [NSNumber numberWithInt: 1],                         AVNumberOfChannelsKey,
							  [NSNumber numberWithInt: AVAudioQualityMax],         AVEncoderAudioQualityKey,
							  nil];
	
	NSError *error;
	
	if (recorder) recorder = nil;
    recorder = [[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error];
	
	if (recorder) {
		[recorder prepareToRecord];
		recorder.meteringEnabled = YES;
		[recorder record];
		levelTimer = [NSTimer scheduledTimerWithTimeInterval: 0.03 target: self selector: @selector(levelTimerCallback:) userInfo: nil repeats: YES];
		
	} else	
	    NSLog(@"error en recorder");
	
	[ self schedule:@selector( update: ) ];
}

- (void)update:(ccTime)dt 
{
	for(unsigned int i = 0; i<dientes.size(); i++)
	{
		diente *d = &dientes[i];
		if( d->flying )
		{
			CGPoint p = d->sprite.position;
			d->sprite.position = ccp( p.x + d->speedX*dt , p.y + d->speedY * dt);
			
			d->time = dt + d->time;
			d->sprite.rotation = -cos(d->time * M_PI) * (d->angle - 90) * d->inertia;
			d->inertia = d->inertia - 0.2 * d->inertia * dt;
		}
	}
}

- (void)levelTimerCallback:(NSTimer *)timer {
	
	[recorder updateMeters];
	const double ALPHA = 0.05;
	double peakPowerForChannel = pow(10, (0.05 * [recorder peakPowerForChannel:0]));
	lowPassResults = ALPHA * peakPowerForChannel + (1.0 - ALPHA) * lowPassResults;	
	bool found = false;
	if (lowPassResults > 0.5)
	{
        labelSopla.visible = NO;
		
		std::vector<diente>::iterator it;
		
		for( it = dientes.begin() ; it != dientes.end(); it++)
		{
			if ( !it->flying && it->weight >= lowPassResults && !found)
			{
				it->flying = true;
				it->inertia = 1;
				it->speedX = (rand()&200) - 100;
				it->speedY = 20 + rand()%20;
				found = true;
			}
			else 
			{
				it->speedY += 1;
			}

		}

	}
}

//genera el botón para regresar a la escena anterior
- (void)backButtonTapped:(id)sender {
	[recorder stop];
    AudioSessionSetActive(false);
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) [[CCDirector sharedDirector] setDeviceOrientation:CCDeviceOrientationPortrait];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"inicio" object:@""];
    SMAction(@"Air");
}

- (void) dealloc
{
	[levelTimer release];
	[recorder release];
	//[AVAudioPlayer release];
	[super dealloc];
}

//
// Recibiendo llamada, pausa la aplicación
-(void) applicationWillResignActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] pause];
}

// llamada rechazada
-(void) applicationDidBecomeActive:(UIApplication *)application
{
	[[CCDirector sharedDirector] resume];
}

-(void) applicationDidEnterBackground:(UIApplication*)application
{
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application
{
	[[CCDirector sharedDirector] startAnimation];
}

// aplicación será killed
- (void)applicationWillTerminate:(UIApplication *)application
{	
	//CC_DIRECTOR_END();
	CCDirector *director = [CCDirector sharedDirector];
	// Sets landscape mode
	//[director setDeviceOrientation:kCCDeviceOrientationPortrait];
	
	[[director openGLView] removeFromSuperview];
	
	[director end];	
}
// purgar memoria 
- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
	[[CCDirector sharedDirector] purgeCachedData];
}

// next delta time will be zero
-(void) applicationSignificantTimeChange:(UIApplication *)application
{
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

@end



