//
//  PreRelajacionBaseBase.m
//  iDStress
//
//  Created by Leopold Riola on 07/06/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "Flurry.h"
#import "PreRelajacionBase.h"
#import "Player.h"
#import "Info.h"
#import "StoreManager.h"
@implementation PreRelajacionBase
@synthesize empezar, guia, animacion;

-(IBAction) info: (id)sender {
	Info *info;
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        info = [[Info alloc] initWithNibName:@"Info-iPad" bundle:nil];
    } else {
        info = [[Info alloc] initWithNibName:@"Info" bundle:nil];
    }
    info.texto1 =  [[[NSMutableString alloc] initWithString: NSLocalizedString(@"This exercise will help you to relax your body and mind so you can manage your feelings and stress.",nil)] autorelease];
	info.texto2 = [[[NSMutableString alloc] initWithString: NSLocalizedString(@"\n  Think of your favorite place or your favorite element of the nature. \n\n You can practice this exercise every day to prevent stress.",nil)] autorelease];
	[self.navigationController pushViewController:info animated:YES];
	[info release];
}

- (IBAction) empezar: (id)sender{
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"RelajacionBase" forKey:@"Tratamiento"];
    
    if ([prefs boolForKey:iDStress1]) {
        
        Player* player;
        player = [[Player alloc] initWithNibName:@"Player" bundle:nil];
        [self.navigationController pushViewController:player animated:NO];
        [player release];
    }
    else {
        [[StoreManager sharedStoreManager] purchaseItemWithIdentifier:iDStress1];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(purchased) name:@"TransactionSuccessful" object:nil];
    }
}

- (void) viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TransactionSuccessful" object:nil];
}

- (void) purchased {
    [self viewWillAppear:NO];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.title = NSLocalizedString(@"Basic Relax",nil);
	
    SET_INFO_BUTTON;
	
	int numberOfFrames = 76;
	NSMutableArray *imagesArray = [NSMutableArray arrayWithCapacity:numberOfFrames];
	
	animacion.image = [UIImage imageNamed:@"a1-1.png"];
    dispatch_queue_t backgroundQueue = dispatch_queue_create("com.Enlace.iDStress", 0);
    dispatch_async(backgroundQueue, ^{
        for (int i=1; i<=numberOfFrames-10; i++){
            [imagesArray addObject:[UIImage imageNamed:[NSString stringWithFormat:@"a1-%i.png", i]]];
        }
        for (int i=1; i<=10; i++){
            [imagesArray addObject:[UIImage imageNamed:@"a1-66.png"]];
        }
        animacion.animationImages = imagesArray;
        animacion.animationDuration = 8;
        dispatch_async(dispatch_get_main_queue(), ^{
            [animacion startAnimating];
        });
    });
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:iDStress1]) {
        [self.empezar setTitle:NSLocalizedString(@"Purchase", nil) forState:UIControlStateNormal];
    }
    else [self.empezar setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
    [Flurry logEvent:@"Tratamiento visto" withParameters:[NSDictionary dictionaryWithObject:@"Relajacion Base" forKey:@"Tratamiento"]];
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	NSString *imagen;
	switch ([prefs integerForKey:@"elemento"]) {
		case 1:
			imagen = @"Avatar_Agua-e.png";
			break;
		case 2:
			imagen = @"Avatar_Fuego-e.png";
			break;
		case 3:
			imagen = @"Avatar_Tierra-e.png";
			break;
		case 4:
			imagen = @"Avatar_Aire-e.png";
			break;
		default:
			imagen = @"Avatar_Fuego-e.png";
			break;
	}
	guia.image = [UIImage imageNamed:imagen];
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[animacion release];
	[guia release];
	[empezar release];
	[super dealloc];
}


@end
